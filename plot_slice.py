import matplotlib as mpl
mpl.use('Agg')

import ytathena as ya
import yt
import glob
import argparse
import os
import cPickle as pickle

import matplotlib.colorbar as colorbar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm,SymLogNorm,NoNorm,Normalize
from pyathena import read_starvtk
from slices.scatter_sp import scatter_sp,sp_legend
import numpy as np
import string

aux=ya.set_aux('solar')

fields=['cell_volume','cell_mass']


def plot_slice(slcfname,vtkfname,fields_to_draw,zoom=1.,writefile=True,pngfname=''):
    global aux
    plt.rc('font',size=14)
    plt.rc('xtick',labelsize=14)
    plt.rc('ytick',labelsize=14)

    slc_data=pickle.load(open(slcfname,'rb'))
    Lx=slc_data['yextent'][1]-slc_data['yextent'][0]
    Lz=slc_data['yextent'][3]-slc_data['yextent'][2]
    Nx,Nz=slc_data['y']['nH'].shape
    Lz=Lz/zoom
    ix=2
    iz=ix*Lz/Lx
    nf=len(fields_to_draw)
    fig=plt.figure(1,figsize=(ix*nf+ix,iz+ix*2))
    gs = gridspec.GridSpec(2,nf,height_ratios=[iz,ix])
    gs.update(left=0.10,right=0.90,wspace=0,hspace=0)

    sp=read_starvtk(vtkfname[:-3]+'starpar.vtk')
    images=[]
    for i,axis in enumerate(['y','z']):
        for j,f in enumerate(fields_to_draw):
            data=slc_data[axis][f]
            ax=plt.subplot(gs[i,j])
            im=ax.imshow(data,origin='lower',interpolation='nearest')
            if aux[f]['log']: im.set_norm(LogNorm()) 
            extent=slc_data[axis+'extent']
            im.set_extent(extent)
            im.set_cmap(aux[f]['cmap'])
            im.set_clim(aux[f]['clim'])
            images.append(im)
            if j == 0: 
		scatter_sp(sp,ax,axis=axis,runaway=False,norm_factor=4.)
            elif j == 1: 
		scatter_sp(sp,ax,axis=axis,norm_factor=4.)
            ax.set_xlim(extent[0],extent[1])
            ax.set_ylim(extent[2],extent[3])

    gs2 = gridspec.GridSpec(nf+1,1)
    gs2.update(left=0.91,right=0.93,hspace=0.05)
    for j,(im,f) in enumerate(zip(images,fields_to_draw)):
        cax=plt.subplot(gs2[j+1])
        cbar = fig.colorbar(im,cax=cax,orientation='vertical')
        cbar.set_label(aux[f]['label'])
        #cax.xaxis.tick_top()
        #cax.xaxis.set_label_position('top')
        if aux[f].has_key('cticks'): cbar.set_ticks(aux[f]['cticks'])

    cax=plt.subplot(gs2[0])
    cbar = colorbar.ColorbarBase(cax, ticks=[0,20,40],
           cmap=plt.cm.cool_r, norm=Normalize(vmin=0,vmax=40), 
           orientation='vertical')
    cbar.set_label(r'${\rm age [Myr]}$')

    axes=fig.axes[:2*nf]
    legend=sp_legend(axes[0],top=True,norm_factor=4.)

    plt.setp([ax.get_xticklabels() for ax in axes[:2*nf]],visible=False)
    plt.setp([ax.get_yticklabels() for ax in axes[:2*nf]],visible=False)
    plt.setp(axes[:nf],'ylim',(slc_data['yextent'][2]/zoom,slc_data['yextent'][3]/zoom))

    plt.setp(axes[nf:2*nf],'xlabel','x [kpc]')
    plt.setp(axes[0],'ylabel','z [kpc]')
    plt.setp(axes[nf],'ylabel','y [kpc]')
    plt.setp([ax.get_xticklabels() for ax in axes[nf:]], visible=True)
    plt.setp([ax.get_yticklabels() for ax in axes[:2*nf:nf]], visible=True)
    plt.setp([ax.xaxis.get_majorticklabels() for ax in axes[nf:2*nf]], rotation=45 )

    if pngfname == '':
        pngfname=slcfname[:-1]+'png'
    #canvas = mpl.backends.backend_agg.FigureCanvasAgg(fig)
    #canvas.print_figure(pngfname,num=1,dpi=150,bbox_inches='tight')
    if writefile:
        plt.savefig(pngfname,bbox_inches='tight',num=0,dpi=150)
        plt.close()



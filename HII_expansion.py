#!/bin/python
from init import *
from ath_data import *

class anal():
	def __init__(self):
		self.setup()

	def solve(self,type):
		if type==1: 
			self.eq=self.eq1
		elif type==2:
			self.eq=self.eq2
		elif type==3:
			self.eq=self.eq3

		y0=[self.xi[0],self.xidot[0]]
		t0=self.t[0]
		while (t0 < 10.) and (y0[0] > 0):
			y1=self.RK4(y0,t0,self.dt)
			self.xi.append(y1[0])
			self.xidot.append(y1[1])
			self.t.append(t0+self.dt)
			y0=y1
			t0=t0+self.dt
		self.xi=np.array(self.xi)
		self.xidot=np.array(self.xidot)
		self.t=np.array(self.t)


	def RK4(self,y0,t0,dt):
		dy1=dt*self.eq(y0,t0)
		dy2=dt*self.eq(y0+dy1/2.0,t0+dt/2.0)
		dy3=dt*self.eq(y0+dy2/2.0,t0+dt/2.0)
		dy4=dt*self.eq(y0+dy3,t0+dt)

		return y0+(1.0/6.0)*(dy1+2.0*dy2+2.0*dy3+dy4)

	def setup(self,x=1.0,c0=0.0):
		self.t=[0.]
		self.dt=1.e-3
		self.x=x
		self.c0=c0
		self.xidot=[np.sqrt(self.x-self.c0**2)]
		self.xi=[1.0+self.xidot[0]*self.dt]

	def eq1(self,y,x):
		dydt1 = y[1]
		dydt2 = 3.0*(self.x*y[0]**(-2.5) - (y[1]**2+self.c0**2)/y[0])

		return np.array([dydt1,dydt2])
	def eq2(self,y,x):
		dydt1 = y[1]
		dydt2 = 3.0*(self.x*np.sqrt(y[0])-y[0]**2*(y[1]**2+self.c0**2))/(y[0]**3.0-1.0)

		return np.array([dydt1,dydt2])

	def eq3(self,y,x):
		dydt1 = y[1]
		dydt2 = (3.0*(self.x*np.sqrt(y[0])-y[0]**2*self.c0**2)-(y[0]**2+2.0/y[0])*y[1]**2)/(y[0]**3.0-1.0)

		return np.array([dydt1,dydt2])

	def plot(self,ax=None):
		xi_HI=(1+7./4.*np.sqrt(4./3.*self.x)*self.t)**(4./7.)
		if ax == None:
			fig=plt.gcf()
			fig.clf()
			ax=fig.add_subplot(111)
		ax.plot(self.t,self.xi,'o')
		ax.plot(self.t,self.xidot,'o')
		ax.plot(self.t,xi_HI)
		plt.draw()

		return ax


class rprof():
	def __init__(self,filename):
		self.setup(filename)

	def setup(self,filename):
		data = ath_data(filename)
		data.readall()

		starfile = filename.split('.')
		starfile.insert(2,'starpar')
		starfile = string.join(starfile,'.')
		nstar, id, mass, pos, vel=readvtk_star(starfile)
		self.msp = mass*1.4*mp*pc*pc*pc/msun
		if nstar == 0: pos=(0,0,0)

		r3d,x3d,y3d,z3d = data.rprof(x0=pos[0],y0=pos[1],z0=pos[2])
		self.r3d=r3d.flatten()
		self.d = data.data[0].flatten()
		self.vr = data.data[1]*x3d/r3d+data.data[2]*y3d/r3d+data.data[3]*z3d/r3d
		self.vr = self.vr.flatten()
		self.P = data.data[4].flatten()
		self.Punit = 1.4*mp*1.e10/kbol
		self.cs = np.sqrt(self.P/self.d)
		self.pr = self.d*self.vr
		self.t = data.time
		dVol = data.grid['dx1']*data.grid['dx2']*data.grid['dx3']
		self.Mr = self.d*self.vr*dVol
	

	def set_Spitzer(self, n0):
		self.Rs = 3.13 * (3.981*self.msp/1.e3)**(1/3.) * (n0/100.)**(-2/3.)
		eta= 4./7.
		ts = self.Rs/11.4
		self.time = np.linspace(0,5,100)*1.e6*yr/pc*1.e5
		self.Ri = self.Rs*(1+self.time/eta/ts)**eta
		self.Vi = self.Ri/ts/(1+self.time/eta/ts)

def plot(id,x=1.0,n1=100.):
	base_dir='/scr1/cgkim/Research/athena_StarParticle/bin/N64_test/id0/'
	files=glob.glob(base_dir+id+'.????.vtk')
	files.sort()

	sol=anal()
	sol.setup(x=x)
	sol.solve(2)

	fig1=plt.figure(1,figsize=(6,8))
	fig2=plt.figure(2,figsize=(6,8))
	fig1.clf()
	fig2.clf()
	ax1=fig1.add_subplot(411)
	ax2=fig1.add_subplot(412)
	ax3=fig1.add_subplot(413)
	ax4=fig1.add_subplot(414)
	ax_rprof1=fig2.add_subplot(311)
	ax_rprof2=fig2.add_subplot(312)
	ax_rprof3=fig2.add_subplot(313)

	profile=rprof(files[0])
	print profile.msp
	profile.set_Spitzer(n1)
	cII=6.89
	ts=profile.Rs/cII
	time=sol.t*ts
	Ri=profile.Rs*sol.xi
	Vi=cII*sol.xidot
	n0=n1/x
	line1,=ax1.plot(time,Ri)
	ax1.plot(time,profile.Rs*(1+np.sqrt(4.*x/3.)*7./4.*sol.t)**(4./7.))
	line2,=ax2.plot(time,Vi)
	line3,=ax3.plot(time,4*np.pi*(Ri**3-profile.Rs**3)*n0*Vi/3.)
	line4,=ax4.plot(time,n1/sol.xi**1.5)

	dt=1.0
	dline,=ax_rprof1.plot(profile.r3d,profile.d,'.')
	vline,=ax_rprof2.plot(profile.r3d,profile.vr,'.')
	Pline,=ax_rprof3.plot(profile.r3d,profile.cs,'.')
	tnext=profile.t+dt
	for file in files[1:]:
		profile=rprof(file)
		ind=profile.Mr.argmax()
		ax1.plot(profile.t,profile.r3d[ind],'o',color=line1.get_color())
		ax2.plot(profile.t,profile.vr[ind],'o',color=line2.get_color())
		ind=np.where(profile.cs > 5.)
		ax3.plot(profile.t,profile.Mr.sum()*8,'o',color=line3.get_color(),alpha=0.5)
		ax3.plot(profile.t,profile.Mr[ind].sum()*8,'s',color=line3.get_color(),alpha=0.5)
		ax4.plot(profile.t,profile.d[ind].mean(),'o',color=line4.get_color(),alpha=0.5)
		#ax3.plot(profile.r3d,profile.vr,'.')
		if profile.t>tnext:
			ax_rprof1.plot(profile.r3d,profile.d,'.',alpha=0.5)
			ax_rprof2.plot(profile.r3d,profile.vr,'.',alpha=0.5)
			ax_rprof3.plot(profile.r3d,profile.cs,'.',alpha=0.5)
			tnext=tnext+dt

	ax3.set_xlabel('t')
	ax1.set_ylabel(r'$R_i$')
	ax2.set_ylabel(r'$V_i$')
	ax3.set_ylabel(r'$P_{tot}$')
	ax3.set_yscale('log')

	ax_rprof3.set_xlabel('r')
	ax_rprof1.set_ylabel(r'$\rho$')
	ax_rprof2.set_ylabel(r'$v_r$')
	ax_rprof3.set_ylabel(r'$c_s$')
	ax_rprof1.set_yscale('log')
	plt.draw()
	fig1.tight_layout()
	fig2.tight_layout()
	fig1.savefig('png/HII_n1_tprof.png')
	fig2.savefig('png/HII_n1_rprof.png')

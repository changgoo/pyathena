#!/usr/bin/python

from init import *
import ath_hst
import glob
import os
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-b','--base-directory', dest='base_directory',
                  help='base directory for data',
                  default='/scr1/cgkim/Research/athena_StarParticle/bin/')
parser.add_option('-d','--directory',dest='directory',
                  help='working directory',default='')
parser.add_option('-i','--id',dest='id',
                  help='id of dataset')
parser.add_option('-r','--xrange',dest='xr',
                  help='xrange')
parser.add_option('-o','--output',dest='out_fname',
                  help='output file name')
(options, args) = parser.parse_args()

files=[]
style = ['b-','r-','g-']

fig=plt.figure(figsize=(8,10))
ax1=fig.add_subplot(311)
ax2=fig.add_subplot(312)
ax3=fig.add_subplot(313)

t0=pc/1.e5/1.e6/yr # in units of Myr
munit=1.4*mp*pc**3/msun # in units of M_sun/Myr
mdot0=munit/t0
Omega=28.e-3
Lz=64.

ids = options.id.split(',')
  

print len(ids)

for id in ids:

	fsearch = options.base_directory 
	fsearch = fsearch+options.directory
	fsearch = fsearch+id
	hstfile = fsearch+'.hst'
	starfile = glob.glob(fsearch+'.*.star')

	hst = ath_hst.read(hstfile)
	time = hst['time']*Omega/2./np.pi
	gmass = hst['mass']
	print hst['mass'][0],gmass[0]*Lz*munit
	gmass = hst['mass']/(hst['mass'][0])#*hst['vol']*munit/1.e5
	sig1 = np.sqrt(2.0*hst['x1KE']/hst['mass'])
	sig2 = np.sqrt(2.0*hst['x2dke']/hst['mass'])
	sig3 = np.sqrt(2.0*hst['x3KE']/hst['mass'])
	sig= np.sqrt(sig1**2+sig2**2+sig3**2)
	msp = hst['msp']/hst['mass'][0]#*hst['vol']*munit/1.e5
	mghost = hst['mghost']/hst['mass'][0]#*hst['vol']*munit/1.e5
	H = np.sqrt(hst['H2']/hst['mass'])

	line,=ax1.plot(time,(gmass+msp-mghost))
        lcolor=line.get_color()
	ax1.plot(time,gmass,color=lcolor,ls='--')
	ax1.plot(time,msp,color=lcolor,ls=':')
	ax1.plot(time,mghost+1,color=lcolor,ls='-.')

	ax2.plot(time,sig,color=lcolor)
	ax2.plot(time,sig1,color=lcolor,ls='--')
	ax2.plot(time,sig2,color=lcolor,ls='-.')
	ax2.plot(time,sig3,color=lcolor,ls=':')

	ax3.plot(time,H,color=lcolor,label=id)



#time=np.linspace(0,1,1000)
#ax1.plot(time,1/(1+np.exp(-2.0*(time*2*np.pi-np.pi))),color='black',ls=':')
#ax1.plot(time,1/(1+np.exp(-4.0*(time*2*np.pi-np.pi))),color='black',ls=':')
ax1.axhline(1.0,color='black',ls=':')
ax3.set_xlabel(r'$t/t_{\rm orb}$')
ax1.set_ylabel(r'M$_{\rm gas}$, M$_{\rm *}$, M_${\rm tot}$]')
ax2.set_ylabel(r'$\sigma$ [km/s]')
ax3.set_ylabel(r'H [pc]')
ax2.set_yscale('log')
ax3.legend(loc=1)
if options.xr != None:
	xr = options.xr.split(',')
	axes = fig.get_axes()
	plt.setp(axes,'xlim',(eval(xr[0]),eval(xr[1])))
ax1.set_ylim(0.9,1.1)
mystyle()
plt.tight_layout()

if options.out_fname == None: out_fname='png/'+ids[0]+'_hst.png'
else: out_fname=options.out_fname
fig.savefig(out_fname,dpi=100)

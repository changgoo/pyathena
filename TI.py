#!python
from init import *
from ath_data import *
from cooling import *
import ath_hst

class TIdata:
	def __init__(self,filename):
		self.setup(filename)

	def setup(self,filename):
		self.filename=filename
		data = ath_data(filename)
		data.set_grid()
		self.set_header(data.grid)
		self.set_unit()
		self.reload(data)

	def reset_time(self,step):
		data = ath_data(self.filename)
		data.set_step(step)
		self.filename=data.filename
		self.reload(data)

	def read_hst(self,hstfilename):
		self.hst=ath_hst.read(hstfilename)

	def set_unit(self):
		self.lunit=pc
		self.dunit=1.4*mp
		self.vunit=1.e5
		self.tunit=self.lunit/self.vunit/yr/1.e6
		self.Punit=self.dunit*self.vunit**2

	def set_header(self,grid):
		self.x1=grid['x1']
		self.x2=grid['x2']
		self.x3=grid['x3']
		self.grid=grid
		self.nx1 = grid['nx1']
		self.nx2 = grid['nx2']
		self.nx3 = grid['nx3']
		self.dx1 = grid['dx1']
		self.dx2 = grid['dx2']
		self.dx3 = grid['dx3']

	def reload(self,data):
		data.readall(grid=self.grid)
		print len(data.data)

		self.time = data.time
		self.tMyr = self.time*self.tunit
		
		self.n = data.data[0]
		self.v1 = data.data[1]
		self.v2 = data.data[2]
		self.v3 = data.data[3]
		if(len(data.data) > 4):
			self.P = data.data[4]*self.Punit/kbol
			self.T = self.P/self.n/1.1
		if(len(data.data) > 5): self.Phi = data.data[5]
		self.dir = data.dir
		self.id = data.id
		self.step = data.step
		self.ext = data.ext

		#self.core_property(ncut=5.0)

	def convert_log(self):
		if self.T.max() > 10:
			self.n = np.log10(self.n)
			self.P = np.log10(self.P)
			self.T = np.log10(self.T)
		else:
			print "it is already in log scales"

	def convert_lin(self):
		if self.T.max() < 10:
			self.n = np.power(10,self.n)
			self.P = np.power(10,self.P)
			self.T = np.power(10,self.T)
		else:
			print "it is already linear scales"

	def rprof(self,force=False,interp=False,Npoints=1):
		x3d = np.tile(self.x1.reshape(1,1,self.nx1),(self.nx3,self.nx2,1))
		y3d = np.tile(self.x2.reshape(1,self.nx2,1),(self.nx3,1,self.nx1))
		z3d = np.tile(self.x3.reshape(self.nx3,1,1),(1,self.nx2,self.nx1))

		self.r3d = np.sqrt(x3d**2+y3d**2+z3d**2)
		self.vr3d = (self.v1*x3d+self.v2*y3d+self.v3*z3d)/self.r3d

		self.r1d = np.arange(self.r3d.min(),self.r3d.max(),self.dx1)

		if(force):
			fP = np.gradient(self.P*kbol/self.Punit,self.dx3,self.dx2,self.dx1)
			fG = np.gradient(self.Phi,self.dx3,self.dx2,self.dx1)
			self.fPr = (fP[2]*x3d+fP[1]*y3d+fP[0]*z3d)/self.r3d
			self.fGr = self.n*(fG[2]*x3d+fG[1]*y3d+fG[0]*z3d)/self.r3d

		if interp:
			ind = np.argsort(self.r3d,axis=None)
			r3dtmp = (self.r3d.flatten())[ind]
			vr3dtmp = (self.vr3d.flatten())[ind]
			n3dtmp = (self.n.flatten())[ind]
			T3dtmp = (self.T.flatten())[ind]
			P3dtmp = (self.P.flatten())[ind]

			self.vr1d = np.interp(self.r1d,r3dtmp,vr3dtmp)
			self.n1d = np.interp(self.r1d,r3dtmp,n3dtmp)
			self.T1d = np.interp(self.r1d,r3dtmp,T3dtmp)
			self.P1d = np.interp(self.r1d,r3dtmp,P3dtmp)

			if(force):
				fGrtmp = (self.fGr.flatten())[ind]
				fPrtmp = (self.fPr.flatten())[ind]
				self.fGr1d = np.interp(self.r1d,r3dtmp,fGrtmp)
				self.fPr1d = np.interp(self.r1d,r3dtmp,fPrtmp)

		else:
			self.vr1d = np.zeros(len(self.r1d))
			self.n1d = np.zeros(len(self.r1d))
			self.T1d = np.zeros(len(self.r1d))
			self.P1d = np.zeros(len(self.r1d))
			self.fGr1d = np.zeros(len(self.r1d))
			self.fPr1d = np.zeros(len(self.r1d))
			for i in range(len(self.r1d)):
				ind = ((self.r3d >= (self.r1d[i]-0.5*self.dx1)) & (self.r3d <= (self.r1d[i]+0.5*self.dx1)))
				self.vr1d[i] = np.mean(self.vr3d[ind])
				self.n1d[i] = np.mean(self.n[ind])
				self.T1d[i] = np.mean(self.T[ind])
				self.P1d[i] = np.mean(self.P[ind])
				if(force):
					self.fGr1d[i] = np.mean(self.fGr[ind])
					self.fPr1d[i] = np.mean(self.fPr[ind])

		if Npoints > len(self.r1d):
			r1d = np.linspace(self.r3d.min(),self.r3d.max(),Npoints)
			self.vr1d = np.interp(r1d,self.r1d,self.vr1d)
			self.n1d = np.interp(r1d,self.r1d,self.n1d)
			self.P1d = np.interp(r1d,self.r1d,self.P1d)
			self.T1d = np.interp(r1d,self.r1d,self.T1d)
			if(force):
				self.fGr1d = np.interp(r1d,self.r1d,self.fGr1d)
				self.fPr1d = np.interp(r1d,self.r1d,self.fPr1d)
			self.r1d = r1d
	def core_property(self,ncut=0.0):
		ind = (self.n > ncut)
		den = self.n[ind]*1.4*mp
		self.Pc = self.P[ind].max()/self.P[ind].min()
		dVol = self.dx1*self.dx2*self.dx3*pc**3.
		dM = den*dVol/msun
		self.Mtot = dM.sum()

	
class rprof:
	def __init__(self,TIdata):
		self.setup(TIdata)

	def setup(self,TIdata):
		self.data=TIdata
		self.r1d=[]

	def readall(self,start=-1,stop=-1,force=False):

		import glob
		import os

		file = glob.glob(os.path.join(self.data.dir,self.data.id)+'.????.'+self.data.ext)
		nf = len(file)
		if len(self.r1d)==0:
			self.vr1d=[]
			self.n1d=[]
			self.P1d=[]
			self.T1d=[]
			self.fPr1d=[]
			self.fGr1d=[]
			self.Mtot=[]

		if start == -1: start = len(self.r1d)
		if stop == -1: stop = nf
		
		for i in range(start,stop):
			self.data.reset_time(i)
			self.data.rprof(force=force)
			self.r1d.append(self.data.r1d)	
			self.vr1d.append(self.data.vr1d)	
			self.P1d.append(self.data.P1d)	
			self.n1d.append(self.data.n1d)	
			self.T1d.append(self.data.T1d)	
			if force:
				self.fPr1d.append(self.data.fPr1d)
				self.fGr1d.append(self.data.fGr1d)
			self.Mtot.append(self.data.Mtot)
	def mean_profile(self,start,stop,log=True):

		r1d=np.array(self.r1d)
		n1d=np.array(self.n1d)
		P1d=np.array(self.P1d)
		T1d=np.array(self.T1d)
		vr1d=np.array(self.vr1d)

		if log:
			n1d=np.log(self.n1d)
			P1d=np.log(self.P1d)
			T1d=np.log(self.T1d)

		self.r1dmean=r1d[start:stop,:].mean(axis=0)
		self.n1dmean=n1d[start:stop,:].mean(axis=0)
		self.P1dmean=P1d[start:stop,:].mean(axis=0)
		self.T1dmean=T1d[start:stop,:].mean(axis=0)
		self.vr1dmean=vr1d[start:stop,:].mean(axis=0)

		if log:
			self.n1dmean = np.exp(self.n1dmean)
			self.P1dmean = np.exp(self.P1dmean)
			self.T1dmean = np.exp(self.T1dmean)


def plot_rprof(rprof,step,new=False,noadd=True,mean=False):
	if new:
		fig, axes, = plt.subplots(nrows=2,ncols=2,sharex=False,figsize=(10,10))
		axes = axes.reshape(4)
	else:
		fig = plt.gcf()
		axes = fig.get_axes()
		if noadd:
			for ax in axes: ax.clear()

	r1d=rprof.r1d[step]
	vr1d=rprof.vr1d[step]
	T1d=rprof.T1d[step]
	n1d=rprof.n1d[step]
	P1d=rprof.P1d[step]
#	fPr1d=rprof.fPr1d[step]
#	fGr1d=rprof.fGr1d[step]
	lines = []

	for ax, y in zip(axes[:-1], (n1d,vr1d,T1d)):
		lines.append(ax.plot(r1d,y))
	#axes[2].plot(r1d,-fPr1d)
	if mean:
		axes[0].plot(r1d,rprof.n1dmean)
		axes[1].plot(r1d,rprof.vr1dmean)
		axes[2].plot(r1d,rprof.P1dmean)

	axes[0].set_ylabel('$n$ [cm$^-3$]')
	axes[1].set_ylabel('$v_r$ [km/s], $v_r/c_s$')
	axes[2].set_ylabel('$P/k$ [K]')

	axes[0].set_ylim(0.1,1.e6)
	axes[1].set_ylim(-4,0.1)
	axes[2].set_ylim(1.e1,1.e5)
	

	plt.setp(axes,'xlim',(1.e-2,6))
	plt.setp(axes,'xscale','log')
	plt.setp(axes,'xlabel','$r$ [pc]')

	axes[0].set_yscale('log')
	axes[2].set_yscale('log')

	if noadd: axes[3].plot(neq,Peq,label='TE curve',linewidth=2,color='y')
        axes[3].plot(n1d,P1d,marker='o',alpha=0.5)
	axes[3].set_xlabel('$n$ [cm$^-3$]')
	axes[3].set_ylabel('$P/k$ [cm$^-3$ K]')
	axes[3].set_xscale('log')
	axes[3].set_yscale('log')
	axes[3].set_xlim(1.e-1,1.e5)
	axes[3].set_ylim(1.e3,1.e6)

	Tmean=np.sum(T1d*n1d)/np.sum(n1d)

        nHLP=8.86*1.1*kbol*T1d.min()/(1.4*mp)**2/Gconst/r1d**2/pc**2/np.pi/4.0
        cs=np.sqrt(1.1*kbol*T1d/(1.4*mp))/1.e5
	print T1d.min(),  n1d.max(), T1d.max(), n1d.min(), P1d.min(), P1d[-1]
	axes[0].plot(r1d,nHLP)
	axes[1].plot(r1d,vr1d/cs)
        nHLP=8.86*1.1*kbol*Tmean/(1.4*mp)**2/Gconst/r1d**2/pc**2/np.pi/4.0
        #nHLP=8.86*1.1*kbol*T1d/(1.4*mp)**2/Gconst/r1d**2/pc**2/np.pi/4.0
	#nHLP=3.1e3*(T1d/100.)/(rprof.data.grid['dx1']/2.)**2
	nLP, TLPeq = TLP(rprof.data.grid['dx1']/2.)
        print TLPeq,nLP

	axes[0].plot(r1d,nHLP)
	axes[0].axhline(nLP,ls=':')
	if new: 
		fig.suptitle(rprof.data.id)
		plt.tight_layout()
	plt.draw()

	

def ani_phase(TIdata):
	import glob
	import ath_util
	import os

	file = glob.glob(os.path.join(TIdata.dir,TIdata.id)+'.????.'+TIdata.ext)
	nf = len(file)
	
	fig = plt.figure(figure_size=(10,10))
	TIdata.reset_time(0)
	logplot_phase(fig,TIdata.n,TIdata.P)
	
	for i in range(nf):
		TIdata.reset_time(i)
		logplot_phase(fig,TIdata.n,TIdata.P)
		fig.clear()
	
def cal_hist2d(n1d,P1d,binsize=0.02):
		
	xrange=(n1d.min(),n1d.max())
	yrange=(P1d.min(),P1d.max())
	xnbin=int(max((xrange[1]-xrange[0])/binsize,1)+1)
	ynbin=int(max((yrange[1]-yrange[0])/binsize,1)+1)
	xbins=np.linspace(xrange[0],xrange[1],xnbin)
	ybins=np.linspace(yrange[0],yrange[1],ynbin)

	hist, xedge, yedge = np.histogram2d(n1d,P1d,
	                                    bins=[xbins,ybins],
	                                    weights=np.power(10,n1d))
	htot=hist.sum()
	hist = np.log10(hist.T)
	hist -= np.log10(htot)

	return hist, xedge, yedge

def update_phase(fig,n,P):
	ax = fig.get_axes()
	if len(ax) == 0:
		print "there is no axes"
		return

	axPhase = ax[0]
	axHistn = ax[1]
	axHistP = ax[2]

	n1d=np.log10(n.reshape(n.size))
	P1d=np.log10(P.reshape(P.size))

	xrange=(n1d.min(),n1d.max())
	yrange=(P1d.min(),P1d.max())

	hist, xedge, yedge = cal_hist2d(n1d,P1d)

	img = axPhase.get_images()
	if len(img) == 0:
		print "there is no images"
	img[0].set_data(hist)
	img[0].set_extent((xedge[0],xedge[-1],yedge[0],yedge[-1]))

	update_PDF(axHistn,axHistP,n1d,P1d)
	axPhase.set_xlim(-1,2)
	axPhase.set_ylim(2,5)

	plt.draw()

def update_PDF(axHistn,axHistP,n1d,P1d):

	axHistn.clear()
	axHistP.clear()

	xrange=(n1d.min(),n1d.max())
	yrange=(P1d.min(),P1d.max())

	xbinsize=0.1
	ybinsize=0.1
	xnbin=max(int(max((xrange[1]-xrange[0])/xbinsize,1)+1),5)
	ynbin=max(int(max((yrange[1]-yrange[0])/ybinsize,1)+1),5)
	xbins=np.linspace(xrange[0],xrange[1],xnbin)
	ybins=np.linspace(yrange[0],yrange[1],ynbin)

	vhist, xedge=np.histogram(n1d, bins=xbins, density=True)
	mhist, xedge=np.histogram(n1d, bins=xbins, density=True, weights=np.power(10,n1d))
	dx=np.diff(xedge)
	axHistn.bar(xedge[:-1],vhist*dx,width=dx,bottom=1.e-4,
	            label='V-pdf', alpha=0.5, color='b')
	axHistn.bar(xedge[:-1]+0.1*dx,mhist*dx,width=dx*0.8,bottom=1.e-4, 
	            label='M-pdf', alpha=0.5, color='g')
	axHistn.set_ylim(1.e-4,5)
	axHistn.set_yscale("log")
	axHistn.set_ylabel("PDF")
	axHistn.set_title("Phase Diagram")

	vhist, xedge=np.histogram(P1d, bins=ybins, density=True)
	mhist, xedge=np.histogram(P1d, bins=ybins, density=True, weights=np.power(10,n1d))
	dx=np.diff(xedge)
	axHistP.barh(xedge[:-1],vhist*dx,height=dx,left=1.e-4,
	             label='V-pdf', alpha=0.5, color='b')
	axHistP.barh(xedge[:-1]+0.1*dx,mhist*dx,height=dx*0.8,left=1.e-4, 
	             label='M-pdf', alpha=0.5, color='g')
	axHistP.set_xlim(1.e-4,5)
	axHistP.set_xscale("log")
	axHistP.set_xlabel("PDF")
	plt.setp(axHistn.get_xticklabels() + axHistP.get_yticklabels(), visible=False)


def logplot_phase(fig,n,P):
	fig.clear()

	axes = fig.get_axes()
	if len(axes) == 0:
		axPhase = fig.add_subplot(111,label='Phase')
		axPhase.set_ylabel("log P/k [cm$^{-3}$K]")
		axPhase.set_xlabel("log n [cm$^{-3}$]")
	else:
		axPhase = axes[0]
	
	ndata=n.size
	nx=float(n.shape[0])
	binsize=0.02

	n1d=np.log10(n.reshape(n.size))
	P1d=np.log10(P.reshape(P.size))

        if ndata > 1.e5:
                hist, xedge, yedge = cal_hist2d(n1d,P1d)

                axPhase.imshow(hist,
                               interpolation='nearest',
                               extent=(xedge[0],xedge[-1],yedge[0],yedge[-1]),
                               cmap=cm.RdPu,origin='lower',vmin=-4,vmax=-1)

        else:
                axPhase.scatter(n1d,P1d,alpha=1/nx,marker='.')

	lines = axPhase.get_lines()
	if len(lines) == 0:
		axPhase.plot(np.log10(neq),np.log10(Peq),label='TE curve',linewidth=2,color='y')
	axPhase.legend(frameon=False)

	axes = fig.get_axes()
	if len(axes) == 1:
		divider = make_axes_locatable(axPhase)
		axHistn = divider.append_axes("top",
	                              size=1.2, pad=0.2,
	                              sharex=axPhase, label='n-PDF')
		axHistP = divider.append_axes("right",
	                              size=1.2, pad=0.2,
	                               sharey=axPhase, label='P-PDF')
		axHistn.set_ylabel("PDF")
		axHistn.set_title("Phase Diagram")
		axHistP.set_xlabel("PDF")
		plt.setp(axHistn.get_xticklabels() + axHistP.get_yticklabels(), visible=False)
	else:
		axHistn = axes[1]
		axHistP = axes[2]

	update_PDF(axHistn,axHistP,n1d,P1d)
	axHistn.legend(frameon=False)

	axPhase.set_xlim(-1,2)
	axPhase.set_ylim(2,5)

	plt.draw()

def func(T0,dx,c):
	return c*T0/10./dx**2-heat/cool_lowT(T0)

def TLP(dx):
	TLPeq=newton(func,5.,args=(dx,nLP_pc_10,))
	nLP=heat/cool_lowT(TLPeq)
	return nLP,TLPeq

def TTr(dx):
	TTreq=newton(func,5.,args=(dx,nTr_pc_10,))
	nTr=heat/cool_lowT(TTreq)
	return nTr,TTreq


heat=2.e-26
Teq=np.logspace(1,5,1000.)
neq=heat/cool(Teq)
Peq=1.1*neq*Teq

nLP_pc_10 = 8.86/(4.*np.pi*Gconst)*(1.1*kbol)/(1.4*mp)**2/(pc)**2*10.
nTr_pc_10 = np.pi/(16.*Gconst)*(1.1*kbol)/(1.4*mp)**2/(pc)**2*10.

# In[3]:
import numpy as np
import rst_handler as rh


# In[4]:

f_lowres='/tigress/changgoo/MHD_2pc_rst/id0/MHD_2pc_rst.0171.rst'


# In[5]:

par=rh.parse_par(f_lowres)


# In[6]:

dm=par['domain1']
Nx=np.array([dm['Nx1'],dm['Nx2'],dm['Nx3']])
Ng=np.array([dm['NGrid_x1'],dm['NGrid_x2'],dm['NGrid_x3']])
Nb=Nx/Ng

print 'Number of zones',Nx
print 'Number of grids',Ng
print 'Number of zones in each grid',Nb


# In[7]:

grids,NG=rh.calculate_grid(Nx,Nb)


# In[7]:

rstdata=rh.read(f_lowres,grids,NG,verbose=True)


#cropping
rstdata_low_crop={}
Nz,Ny,Nx,=rstdata['DENSITY'].shape
hNz =Nz/2
kwidth=224
xf,xc=rh.set_xpos_with_dm(dm)
print xc['z'][hNz-kwidth],xc['z'][hNz+kwidth]


for f in rstdata:
    if f == '3-FIELD':
        rstdata_low_crop[f]=rstdata[f][hNz-kwidth:hNz+kwidth+1,:,:].copy()
    else:
        rstdata_low_crop[f]=rstdata[f][hNz-kwidth:hNz+kwidth,:,:].copy()
    
    if not f.startswith('SCALAR'): print f,rstdata_low_crop[f].shape,rstdata[f].shape,rstdata_low_crop[f].min(),rstdata[f].min()

rstdata_high=rh.refine(rstdata_low_crop)


# In[41]:

#dB=rh.divB(rstdata_high)
#print np.abs(dB).max(),dB.std()
#im=plt.imshow(dB[:,0,:],interpolation='nearest')
#im.set_clim(-1.e-12,1.e-12)


# In[42]:

Nx3,Nx2,Nx1,=rstdata_high['DENSITY'].shape


# In[8]:

pardata_low=rh.parse_misc_info(f_lowres)


# In[9]:

par=pardata_low['par']
par_dm=par[par.rfind('<domain1'):par.rfind('<problem')]
print par_dm


# In[45]:

new_Nx=np.array([Nx1,Nx2,Nx3])
new_NB=np.array([64,64,64])
new_grids,new_NG=rh.calculate_grid(new_Nx,new_NB)
print new_Nx,new_NG


# In[46]:

par=par.replace('Nx1           = 512','Nx1           = %d' % Nx1)
par=par.replace('Nx2           = 512','Nx2           = %d' % Nx2)
par=par.replace('Nx3           = 896','Nx3           = %d' % Nx3)
par=par.replace('x3min         = -896','x3min         = -%d' % (Nx3/2))
par=par.replace('x3max         = 896','x3max         = %d' % (Nx3/2))
par=par.replace('NGrid_x1      = 8','NGrid_x1      = %d' % new_NG[0])
par=par.replace('NGrid_x2      = 8','NGrid_x2      = %d' % new_NG[1])
par=par.replace('NGrid_x3      = 14','NGrid_x3      = %d' % new_NG[2])
pardata_low['par']=par


# In[47]:

par=pardata_low['par']
par_dm=par[par.rfind('<domain1'):par.rfind('<problem')]
print par_dm


# In[48]:

rh.write_allfile(pardata_low,rstdata_high,new_grids,
                 dname='/tigress/changgoo/rst/',id='MHD_1pc_rst',verbose=False,scalar=0)

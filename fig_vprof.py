"""
  Copyright (c) Chang-Goo Kim
  
"""
import glob
import os
import argparse
import cPickle as pickle
import pandas as pd

# matplotlib
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.patheffects import withStroke

# numpy
import numpy as np

# astropy
import astropy.constants as c
import astropy.units as u

# history
import history

myeffect = withStroke(foreground="w", linewidth=3)

panels=['(a) ','(b) ','(c) ','(d) ','(e) ','(f) ','(g) ','(h) ']
flabels={'sfr':r'$\Sigma_{\rm SFR}$',\
         'number_density':r'$n_{\rm H}$',\
         'scaleH':r'$H$',\
         'zeta':r'$\langle{|z|}\rangle$',\
         'chi':r'$\chi$',\
         'H':r'$H$',\
         'Hmag':r'$H_{\rm mag}$',\
         'turbulent_velocity3':r'$v_{\rm z}$',\
         'sz':r'$\sigma_{\rm z}$',\
         'turbulent_alfven_velocity':r'$\delta v_A$',\
         'sound_speed':r'$c_s$',\
         'rhomid':r'$n_{\rm H,0}$',\
         'alpha':r'$\alpha$',\
#=[P_{\rm turb}+P_{\rm th}]/P_{\rm th}$',\
         'beta':r'$P_{\rm mag}/P_{\rm th}$',\
         'R':r'$\mathcal{R}$',\
#=P_{\rm mag}/[P_{\rm turb}+P_{\rm th}]$',\
         'deltaR':r'$\delta\mathcal{R}=P_{\rm mag,t}/[P_{\rm turb}+P_{\rm th}]$',\
         'zetad':r'$\zeta_d$',\
         'gamma':r'$P_{\rm mag,t}/P_{\rm mag,o}$',\
         'dmass':r'$\Delta M/M_0$',\
         'turbulent_pressure':r'$P_{\rm turb}$',\
         'pressure':r'$P_{\rm th}$',\
         'magnetic_pressure':r'$P_{\rm mag}$',\
         'magnetic_energy3':r'$P_{\rm mag,z}$',\
         'magnetic_field2':r'$B_{\rm y}$',\
         'magnetic_support':r'$P_{\rm mag}$',\
         'total_pressure':r'$P_{\rm tot}$',\
         'ord_magnetic_support':r'$P_{\rm mag,o}$',\
         'turb_magnetic_support':r'$P_{\rm mag,t}$',\
         'Pratio':r'$P_{\rm tot}/P_{\rm tot,DE}$',\
         'C':r'$\mathcal{C}$',\
        }
ulabels={'sfr':r' [$M_\odot$ kpc$^{-2}$ yr$^{-1}]$',\
         'number_density':r' [cm$^{-3}$]',\
         'scaleH':r' [pc]',\
         'zeta':r' [pc]',\
         'H':r' [pc]',\
         'Hmag':r' [pc]',\
         'turbulent_velocity3':r' [km/s]',\
         'sound_speed':r' [km/s]',\
         'turbulent_alfven_velocity':r' [km/s]',\
         'sz':r' [km/s]',\
         'rhomid':r' [cm$^{-3}$]',\
         'alpha':r'',\
         'beta':r'',\
         'R':r'',\
         'deltaR':r'',\
         'zetad':r'',\
         'gamma':r'',\
         'dmass':r'',\
         'pressure':r'$/k_B$ [cm$^{-3}$ K]',\
         'magnetic_pressure':r'$/k_B$ [cm$^{-3}$ K]',\
         'magnetic_energy3':r'$/k_B$ [cm$^{-3}$ K]',\
         'magnetic_field2':r' [$\mu$G]',\
         'turbulent_pressure':r'$/k_B$ [cm$^{-3}$ K]',\
         'total_pressure':r'$/k_B$ [cm$^{-3}$ K]',\
         'magnetic_support':r'$/k_B$ [cm$^{-3}$ K]',\
         'ord_magnetic_support':r'$/k_B$ [cm$^{-3}$ K]',\
         'turb_magnetic_support':r'$/k_B$ [cm$^{-3}$ K]',\
         'Pratio':r'',
        } 

def get_mean_std(data,nsigma=3.0):
  ind = range(len(data))
  for i in range(10):
    n0=len(ind)
    tmean = data[ind].mean()
    t2mean = (data[ind]**2).mean()
    tstd = np.sqrt(t2mean-tmean**2)
    ind = np.where((data < tmean+nsigma*tstd) & (data > tmean-nsigma*tstd))[0]
    n1=len(ind)
    if n0 == n1: break

  return i,tmean,tstd

def get_logerror(q,dq):
  reldq=dq/q/np.log(10)
  upper=q*(10**reldq-1)
  lower=q*(1-10**(-reldq))
  return [[lower],[upper]]

def get_self_gravity(z,phi):
  zcoord=(z[1:]+z[:-1])*0.5
  gz_sg=np.diff(phi)/np.diff(z*c.pc.cgs.value)
  return zcoord,gz_sg


def get_external_gravity(z,rhosd):
  zcoord=(z[1:]+z[:-1])*0.5
  gz_ext=(4*np.pi*c.G*rhosd*c.pc).cgs.value*zcoord
  return zcoord,gz_ext


def get_gravity(z,phi):
  rhosd = 0.05*c.M_sun/u.pc**3
  zcoord,gz_ext=get_external_gravity(z,rhosd)
  zcoord,gz_sg=get_self_gravity(z,phi)
  gz = gz_ext+gz_sg
  return zcoord,gz

def get_zetad(z,den):
  rho=np.array(den)
  dz=z[1]-z[0]
  surf=np.sum(rho*dz)
  Nz=len(den)
  rhomid=np.interp(0,z,rho)
  rhoz=np.sum(rho*np.abs(z)*dz)
  zetad=rhomid*rhoz/surf**2
  return zetad

def get_scaleH(z,den,moment=2):
  rhoz2=np.sum(den*np.abs(z)**moment)
  rho=np.sum(den)
  H=(rhoz2/rho)**(1./moment)
  return H

def legend_decorate(legend):
  light_grey = np.array([float(248)/float(255)]*3)
  rect = legend.get_frame()
  rect.set_linewidth(0.0)
  rect.set_facecolor(light_grey)

class Vprof(object):
  """ 
  Object for vertical profile
  """
  def __init__(self,base='/scr1/cgkim/SFR_MHD/pickles/',ids=[],labels=[]):
    if len(ids) == 0:
      ids.append('QA10')
      ids.append('QA10_binf_Turb_SN_2pc')
      ids.append('QA10_b10_Turb_SN_2pc_p4r2')
      ids.append('QA10_b10_Turb_SN_2pc_p3')
      ids.append('QA10_b1_Turb_SN_2pc_p4r2')
      ids.append('QA10_b1_Turb_SN_2pc_p3')

    if len(labels) == 0:
      labels.append('HS')
      labels.append('HL')
      labels.append('MA10')
      labels.append('MB10')
      labels.append('MA1')
      labels.append('MB1')

    labels_mhd=[]
    for label in labels:
      if 'M' in label: labels_mhd.append(label)

    self.base=base
    self.ids=ids
    self.labels=labels
    self.labels_mhd=labels_mhd
    self.tlim={}
    self.zlim={}

    self.setup()

    self._load_data()
    self.hst=self._load_hst()
    self._calculate_averages()


  def setup(self):
    pass

  def _load_hst(self):
    hst=history.hstdata()
    hst.hstfiles=[]
    hst.labels=[]
    for id, label in zip(self.ids,self.labels):
      hst.hstfiles.append(self.base+'hst/'+id+'.hst')
      hst.labels.append(label)
    hst.read()
  
    return hst


  def _load_data(self):
    self.vprof={} 
    self.ds={} 
    self.units={} 
    self.axis={}
    self.time={}
    for id, label in zip(self.ids,self.labels):
      fname=self.base+id+'/'+id+'.vprof.p'
      print fname
      if os.path.isfile(fname): self.vprof[label]=pickle.load(open(fname,'rb'))
      else: print "cannot find Vertical Profile data for ", label
      self.axis[label]=pd.Series(self.vprof[label].pop('axis'),name='z')
      self.time[label]=pd.Series(self.vprof[label].pop('time'),name='time')

      dsfname=self.base+id+'/'+id+'.0000.ds.p'
      if os.path.isfile(dsfname): 
        self.ds[label]=pickle.load(open(dsfname,'rb'))
        self.units=self.ds[label].units
      else: print "cannot find DataSet file for ", label

      self._add_fields(label)

    slcfname=self.base+id+'/'+id+'.0000.surf.1.p'
    if os.path.isfile(slcfname): 
      slc=pickle.load(open(slcfname,'rb'))
      for field in slc:
        self.units[field]=slc[field].units
    else: print "cannot find Slice file for ", label
    
    self._add_units()
    
# unit setting
  def _add_units(self):
    for label in self.labels:
      vprof=self.vprof[label]
      for field in vprof:
        if not self.units.has_key(field):
          if 'turbulent_pressure' is field: self.units[field]=self.units['pressure']
          elif 'total_pressure' is field: self.units[field]=self.units['pressure']
          elif 'magnetic_pressure' in field: self.units[field]=self.units['magnetic_pressure']
          elif 'magnetic_energy' in field: self.units[field]=self.units['magnetic_pressure']
          elif 'magnetic_support' in field: self.units[field]=self.units['magnetic_pressure']
          elif 'magnetic_field' in field: self.units[field]=self.units['cell_centered_B']
          elif 'velocity' in field: self.units[field]=self.units['velocity']
          elif 'speed' in field: self.units[field]=self.units['speed']
          elif 'beta' in field: self.units[field]=u.cm/u.cm
          elif 'zeta' in field: self.units[field]=self.units['length']
          else: print field+' does not have units'

  def _calculate_averages(self):
    self.tavg={}
    self.zavg_mw={}
    self.zavg_vw={}
    self.zavg_mid={}
    for label in self.labels:
      vprof=self.vprof[label]
      t=self.time[label]*self.units['time'].to('Myr').value
      z=self.axis[label]*self.units['length'].to('pc').value
      if self.tlim.has_key(label): t1,t2=self.tlim[label]
      else: t1,t2 = 0.8*max(t), max(t)
      if self.zlim.has_key(label): z1,z2=self.zlim[label]
      else: z1,z2 = min(z),max(z)
      tind =t.index[(t >= t1) * (t <= t2)]
      zind =z.index[(z >= z1) * (z <= z2)]
      t1 = tind.min()
      t2 = tind.max()+1
      z1 = zind.min()
      z2 = zind.max()+1
      Nt=len(t)
      Nz=len(z)
      tavg={}
      zavg_vw={}
      zavg_mw={}
      zavg_mid={}
      print t1,t2,z1,z2
      for field in vprof:
        if field != 'axis' and field != 'time':
          data=vprof[field]
          tavg[field]=data[t1:t2,:].mean(axis=0)
          zavg_vw[field]=data[:,z1:z2].mean(axis=1)
          den=vprof['density']
          zavg_mw[field]=(den*data)[:,z1:z2].mean(axis=1)/den[:,z1:z2].mean(axis=1)
          zavg_mid[field]=data[:,Nz/2-1:Nz/2+1].mean(axis=1)
      self.tavg[label]=pd.DataFrame(tavg,index=z)
      self.zavg_vw[label]=pd.DataFrame(zavg_vw,index=t)
      self.zavg_mw[label]=pd.DataFrame(zavg_mw,index=t)
      self.zavg_mid[label]=pd.DataFrame(zavg_mid,index=t)
      
  def _add_fields(self,label):
    vprof=self.vprof[label]
    if 'kinetic_energy3' in vprof:
      vprof['turbulent_pressure'] = 2.0*vprof['kinetic_energy3']
      vprof['turbulent_velocity1'] = np.sqrt(2.0*vprof['kinetic_energy1']/vprof['density'])
      vprof['turbulent_velocity2'] = np.sqrt(2.0*vprof['kinetic_energy2']/vprof['density'])
      vprof['turbulent_velocity3'] = np.sqrt(2.0*vprof['kinetic_energy3']/vprof['density'])
      vprof['total_pressure'] = vprof['turbulent_pressure']+vprof['pressure']
    if 'pressure' in vprof:
      vprof['sound_speed'] = np.sqrt(vprof['pressure']/vprof['density'])
    if 'magnetic_pressure' in vprof:
      PB1=vprof['magnetic_energy1']
      PB2=vprof['magnetic_energy2']
      PB3=vprof['magnetic_energy3']
      if 'cell_centered_B1' in vprof:
        vprof['magnetic_field1']=vprof['cell_centered_B1']
        vprof['magnetic_field2']=vprof['cell_centered_B2']
        vprof['magnetic_field3']=vprof['cell_centered_B3']
      B1=vprof['magnetic_field1']
      B2=vprof['magnetic_field2']
      B3=vprof['magnetic_field3']
      oPB1=B1**2/(8*np.pi)
      oPB2=B2**2/(8*np.pi)
      oPB3=B3**2/(8*np.pi)
      tPB1=vprof['turb_magnetic_energy1']
      tPB2=vprof['turb_magnetic_energy2']
      tPB3=vprof['turb_magnetic_energy3']
      Pth=vprof['pressure']
      vprof['magnetic_support']=PB1+PB2-PB3
      vprof['ord_magnetic_support']=oPB1+oPB2-oPB3
      vprof['turb_magnetic_support']=tPB1+tPB2-tPB3
      vprof['ord_magnetic_energy1']=oPB1
      vprof['ord_magnetic_energy2']=oPB2
      vprof['ord_magnetic_energy3']=oPB3
      vprof['ord_magnetic_pressure']=oPB1+oPB2+oPB3
      vprof['beta']=Pth/vprof['magnetic_pressure']/4/np.pi
      vprof['ord_beta']=Pth/(oPB1+oPB2+oPB3)/4/np.pi
      vprof['turb_beta']=Pth/(tPB1+tPB2+tPB3)/4/np.pi
      vprof['total_pressure'] += vprof['magnetic_pressure']*4*np.pi

    z=self.axis[label]
    t=self.time[label]
    Nz=z.count()
    Nt=t.count()
    den=vprof['density']
    vprof['zeta']=np.abs(np.tile(z.reshape(1,Nz),(Nt,1)))
    self.vprof[label]=vprof

class figure_vprof(object):
  """
  """
  def __init__(self,vpdata,base=None):
    self.vpdata=vpdata
    self.labels=vpdata.labels
    self.labels_mhd=vpdata.labels_mhd
    self.ids=vpdata.ids
    if base is None: self.base=vpdata.base
    else: self.base=base
    self.setup()

  def setup(self):
    colors = {}
    colors['HS']='k'
    colors['HL']='g'
    colors['MA10']='r'
    colors['MB10']='b'
    colors['MA1']='m'
    colors['MB1']='c'

    self.colors=colors

    plt.rc('font',family='serif')
    plt.rc('lines',lw=2)
    plt.rc('font',size=16)
    plt.rc('xtick',labelsize=14)
    plt.rc('ytick',labelsize=14)

    fig = plt.gcf()
    fig.clf()
    self.fig=fig

  def draw_all(self):
    plt.clf()
    self.plot_pressure()
    self.save(10,12,'P_profiles.pdf')
    plt.clf()
    self.plot_pressure(model_first=True)
    self.save(10,12,'P_profiles2.pdf')
    plt.clf()
    self.plot_pressure(mode='tevol')
    self.save(10,12,'P_evolution.pdf')
    plt.clf()
    self.plot_pressure(mode='tevol',model_first=True)
    self.save(10,12,'P_evolution2.pdf')
    plt.clf()
    self.plot_init()
    self.save(10,8,'initial_vprof.pdf')
    plt.clf()
    self.plot_balance()
    self.save(10,12,'force_balance.pdf')
    plt.clf()
    self.plot_density()
    self.save(10,8,'density.pdf')

  def plot_zetad(self,vpdata):
    ax=self.fig.add_subplot(111)
    rhosd = 0.05*c.M_sun/u.pc**3
    units=vpdata.units
    for label in self.labels:
      vprof=vpdata.vprof[label]
      den=vprof['density']*units['density']
      z=vprof['axis']*units['length'].to('pc')
      t=vprof['time']*units['time'].to('Myr')
      dz=z[1]-z[0]
      Nz=len(z)
      zetad=[]
      rhomid=[]
      H=[]
      for i in range(len(t)):
        zetad.append(get_zetad(z,den[i,:]).value)
        rhomid.append(np.interp(0,z,den[i,:].cgs/1.4/c.m_p.cgs))
        H.append(get_scaleH(z,den[i,:],moment=1).value)
      H=np.array(H)*u.pc
      surf = (den*dz).sum(axis=1).to('M_sun/pc**2')
      l,=ax.plot(t,rhomid,label=label,lw=1,color=self.colors[label])
      ax.plot(t,(surf/2.0/H/1.4/c.m_p).cgs,ls=':',color=l.get_color())
    legend_decorate(ax.legend())
    ax.set_ylim(0,5)
    
  def plot_evolution1(self,vpdata):
    ncol=2
    fields=['sfr','sz','rhomid','zeta','alpha','R']#,'deltaR']
    nrow=len(fields)/ncol
    i=0
    axes={}
    mean={}
    std={}
    for field in fields:
      ax = self.fig.add_subplot(nrow,ncol,i+1)
      ax.grid(True)
      axes[field]=ax
      mtable={}
      stable={}
      for label in self.labels:
        draw=True
        vw=vpdata.zavg_vw[label]
        mw=vpdata.zavg_mw[label]
        mid=vpdata.zavg_mid[label]
        if field is 'sfr':
          x=vpdata.hst.snwu[label]['sfr_time']
          y=vpdata.hst.snwu[label]['sfr']
          #x=vpdata.hst.hstwu[label].time
          #y=vpdata.hst.hstwu[label].sfr
          y.index=x
        elif field is 'sz':
          y=np.sqrt((2.0*vw['kinetic_energy3']+vw['pressure'])/vw['density'])
          y=np.sqrt((2.0*mid['kinetic_energy3']+mid['pressure'])/mid['density'])
        elif field is 'alpha': 
          y=2.0*mid['kinetic_energy3']/mid['pressure']+1
        elif field is 'R' and label in self.labels_mhd: 
          y=4*np.pi*mid['magnetic_support']/(2.0*mid['kinetic_energy3']+mid['pressure'])
        elif field is 'deltaR' and label in self.labels_mhd: 
          y=4*np.pi*mid['turb_magnetic_support']/(2.0*mid['kinetic_energy3']+mid['pressure'])
        elif field is 'rhomid':
          y=mid['density']
        elif field is 'zeta':
          y=mw['zeta']
        else:
          draw = False
        if draw: 
          x=y.index
          ax.plot(x,y,label=label,color=self.colors[label],lw=1)
          mtable[label] = y.loc[x[x>(x.max()*0.8)]].mean()
          stable[label] = y.loc[x[x>(x.max()*0.8)]].std()

      if field in flabels: ax.set_ylabel(flabels[field]+ulabels[field])
      i=i+1
      mean[field]=mtable
      std[field]=stable

    legend_decorate(axes['zeta'].legend(loc=0,fontsize='x-small'))

    axes['sfr'].set_yscale('log')
    axes['sfr'].set_ylim(1.e-4,1.e-2)
    if axes.has_key('R'):
      axes['R'].set_yscale('log')
      axes['R'].set_ylim(0.1,10)
    if axes.has_key('deltaR'):
      axes['deltaR'].set_yscale('log')
      axes['deltaR'].set_ylim(0.01,1)
    if axes.has_key('rhomid'):
      axes['rhomid'].set_ylim(0,5)
    if axes.has_key('alpha'):
      axes['alpha'].set_ylim(0,10)
    if axes.has_key('sz'):
      axes['sz'].set_ylim(0,10)
    if axes.has_key('zeta'):
      axes['zeta'].set_ylim(0,120)
    axes=self.fig.axes
    plt.setp(axes,'xlim',(0,800))
    plt.setp([ax.get_xticklabels() for ax in axes[:ncol*(nrow-1)]],visible=False)
    plt.setp([ax.get_xticklabels() for ax in axes[ncol*(nrow-1):]], rotation=45)
    plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$t $ [Myr]')
    plt.draw()
    plt.tight_layout()


    mean=pd.DataFrame(mean).fillna(0)
    std=pd.DataFrame(std).fillna(0)
    mean=mean[fields] 
    std=std[fields] 
    table1(mean,std)
    return mean,std

  def plot_evolution(self,vpdata):
    ncol=2
    fields=['sfr','scaleH','sz','alpha','R','zetad','beta','gamma']
    nrow=len(fields)/ncol
    i=0
    axes={}
    for field in fields:
      ax = self.fig.add_subplot(nrow,ncol,i+1)
      axes[field]=ax
      for label in self.labels:
        h=vpdata.hst.hstwu[label]
        dom = vpdata.ds[label].domain 
        Nz=dom['Nx'][2]

        vprof=vpdata.vprof[label]
        units=vpdata.units
        t=vprof['time']*units['time'].to('Myr')
        z=vprof['axis']*units['length'].to('pc')
        dz=z[1]-z[0]

        Pth = vprof['pressure']*units['pressure'].cgs
        Pturb = vprof['turbulent_pressure']*units['turbulent_pressure'].cgs
        Pturb1 = 2.0*vprof['kinetic_energy1']*units['turbulent_pressure'].cgs
        Pturb2 = 2.0*vprof['kinetic_energy2']*units['turbulent_pressure'].cgs
        Pturb3 = 2.0*vprof['kinetic_energy3']*units['turbulent_pressure'].cgs
        den = vprof['density']*units['density'].cgs
        rhomid = den[:,Nz/2-1:Nz/2].mean(axis=1)

        scaleH=[]
        for j in range(len(t)):
          scaleH.append(get_scaleH(z,den[j,:],moment=1).value)

        ind=np.where(t.value > 400.)
        Hmean = np.mean(np.array(scaleH)[ind])

        nwidth=int(5.0*Hmean/dz.value)
        k1=np.max([Nz/2-1-nwidth,0])
        k2=np.min([Nz/2+nwidth,Nz])
        k1=0
        k2=Nz

        #print Hmean,nwidth,k1,k2

        Pth = Pth[:,k1:k2]
        Pturb = Pturb[:,k1:k2]
        Pturb1 = Pturb1[:,k1:k2]
        Pturb2 = Pturb2[:,k1:k2]
        Pturb3 = Pturb3[:,k1:k2]
        den = den[:,k1:k2]

        meanrho = den.mean(axis=1)
        vz = np.sqrt(Pturb.mean(axis=1)/meanrho).to('km/s')
        v1 = np.sqrt(Pturb1.mean(axis=1)/meanrho).to('km/s')
        v2 = np.sqrt(Pturb2.mean(axis=1)/meanrho).to('km/s')
        v3 = np.sqrt(Pturb3.mean(axis=1)/meanrho).to('km/s')
        cs = np.sqrt(Pth.mean(axis=1)/meanrho).to('km/s')
        sz = np.sqrt(cs**2+vz**2)
        Ptot=Pturb+Pth
        alpha = ((Pturb+Pth)/Pth)
        #alpha = alpha[:,k1:k2].mean(axis=1)
        alpha = (alpha*den).mean(axis=1)/meanrho

        rhosd = 0.05*c.M_sun/u.pc**3
        surf = 10.*c.M_sun/u.pc**2
        surf = (den*dz).sum(axis=1).to('M_sun/pc**2')

        zetad=[]
        for j in range(len(t)):
          zetad.append(get_zetad(z[k1:k2],den[j,:]).value)

        if 'magnetic_pressure' in vprof:
          Pmag = vprof['magnetic_support']*units['magnetic_support'].cgs
          oPmag = vprof['ord_magnetic_support']*units['magnetic_support'].cgs
          tPmag = vprof['turb_magnetic_support']*units['magnetic_support'].cgs
          tPmag1 = vprof['turb_magnetic_energy1']*units['magnetic_support'].cgs
          tPmag2 = vprof['turb_magnetic_energy2']*units['magnetic_support'].cgs
          tPmag3 = vprof['turb_magnetic_energy3']*units['magnetic_support'].cgs
          Pmag = Pmag[:,k1:k2]
          oPmag = oPmag[:,k1:k2]
          tPmag = tPmag[:,k1:k2]
          tPmag1 = tPmag1[:,k1:k2]
          tPmag2 = tPmag2[:,k1:k2]
          tPmag3 = tPmag3[:,k1:k2]
          beta = (den*Pmag/Pth).mean(axis=1)/meanrho
          R = (den*Pmag/(Ptot)).mean(axis=1)/meanrho
          gamma = (den*tPmag/oPmag).mean(axis=1)/meanrho
          gamma1 = (den*tPmag1/oPmag).mean(axis=1)/meanrho
          gamma2 = (den*tPmag2/oPmag).mean(axis=1)/meanrho
          gamma3 = (den*tPmag3/oPmag).mean(axis=1)/meanrho
          Ptot=Ptot+Pmag
        else:
          beta=np.zeros(len(t))
          R=np.zeros(len(t))
          gamma=np.zeros(len(t))
          gamma1=np.zeros(len(t))
          gamma2=np.zeros(len(t))
          gamma3=np.zeros(len(t))

        if field is 'sfr':
          t=h['time']*u.Myr
          y=h['sfr']
        elif field is 'scaleH':
          y=np.array(scaleH)
        elif field is 'alpha':
          y=alpha
        elif field is 'beta':
          y=beta
        elif field is 'R':
          y=R
        elif field is 'gamma':
          y=gamma
        elif field is 'vturb':
          y=vz
        elif field is 'sz':
          y=sz
        elif field is 'zetad':
          y=np.array(zetad)
        elif field is 'Pratio':
          C=4*0.5*rhosd*(5.0*u.km/u.s)**2/np.pi/c.G/surf**2
          Ptotde=np.pi*c.G*surf**2/4.0*(1+np.sqrt(1+4*C.cgs))
          y=Ptot[:,Nz/2-1:Nz/2].mean(axis=1)
          y=y/Ptotde.cgs
        else:
          t,y = self._get_t_field(label,field,zrange=[k1,k2])
        ax.plot(t,y,label=label,lw=1)
        ax.text(0.05,0.90,panels[i],transform=ax.transAxes,fontsize=20,path_effects=[myeffect])
        ind=np.where(t.value > 300.)
        print label, field, y[ind].mean()

        if field is 'gamma':
          for lw,y in enumerate([gamma1,gamma2,gamma3]):
            ax.plot(t,y,label=label,color=ax.lines[-1].get_color(),ls=':',lw=lw)
            print label, lw, y[ind].mean()
        elif field is 'sz':
          for lw,y in enumerate([v1,v2,v3]):
            print label, lw, y[ind].mean()
        elif field is 'scaleH':
          if 'M' in label:
            P = vpdata.vprof[label]['magnetic_field2']
            H=[]
            for j in range(len(t)):
              H.append(get_scaleH(z,P[j,:],moment=1).value)
            y=np.array(H)
            ax.plot(t,y,label=label,color=ax.lines[-1].get_color(),ls='--')
          y=(surf/rhomid/2.0).to('pc')
          ax.plot(t,y,label=label,color=ax.lines[-1].get_color(),ls=':')
          print label, y[ind].mean()
        elif field is 'Pmag':
          t,y = self._get_t_field(label,'turb_magnetic_pressure',zrange=[0,Nz])
         # ax.plot(t,y,label=label,color=ax.lines[-1].get_color(),ls='--')

      ax.set_ylabel(flabels[field]+ulabels[field])
      i=i+1

    legend_decorate(axes['sfr'].legend(loc=3,fontsize='x-small'))

    plt.setp(axes.values(),'yscale','log')
    axes['scaleH'].set_yscale('linear')
    #axes['Pratio'].set_yscale('linear')
    if axes.has_key('alpha'): axes['alpha'].set_yscale('linear')
    if axes.has_key('Pratio'): axes['Pratio'].set_ylim(0.25,4)
    axes['sfr'].set_ylim(1.e-4,1.e-2)
    axes['scaleH'].set_ylim(0,200)
    if axes.has_key('sz'): axes['sz'].set_yscale('linear')
    if axes.has_key('vturb'): axes['vturb'].set_yscale('linear')
    if axes.has_key('zetad'): 
      axes['zetad'].set_ylim(0.1,1)
      axes['zetad'].axhline(0.5,ls=':',color='k')
      axes['zetad'].axhline(1/np.pi,ls=':',color='k')
    if axes.has_key('gamma'): axes['gamma'].set_ylim(0.05,10)
    if axes.has_key('beta'): axes['beta'].set_ylim(1.e-1,10)
    if axes.has_key('R'): axes['R'].set_ylim(1.e-1,10)
    axes=self.fig.axes
    plt.setp(axes,'xlim',(0,700))
    plt.setp([ax.get_xticklabels() for ax in axes[:ncol*(nrow-1)]],visible=False)
    plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$t $ [Myr]')

    plt.draw()
    plt.tight_layout()


  def plot_density(self,vpdata):
    nrow=2
    ncol=2
    fields=['number_density','alpha','R','gamma']
    i=0
    axes={}
    for field in fields:
      ax = self.fig.add_subplot(nrow,ncol,i+1)
      axes[field]=ax
      for label in self.labels:
        tavg=vpdata.tavg[label]
        Pturb = tavg['turbulent_pressure']
        Pth = tavg['pressure']
        Pmag = 0.
        if field is 'number_density':
          y=tavg[field]
        elif field is 'alpha':
          y=(Pturb+Pth)/Pth
        elif field is 'R':
          if 'magnetic_support' in tavg: Pmag = tavg['magnetic_support']
          y=Pmag/(Pth+Pturb)
        elif field is 'gamma':
          if 'magnetic_support' in tavg:
            oPmag =tavg['ord_magnetic_support']
            tPmag =tavg['turb_magnetic_support']
            y=tPmag/oPmag
          else:
            y=pd.Series(np.zeros(len(tavg.index)),index=tavg.index)
        ax.plot(y.index,y,label=label)
        ax.text(0.05,0.90,panels[i],transform=ax.transAxes,fontsize=20,path_effects=[myeffect])

      ax.set_ylabel(flabels[field]+ulabels[field])
      i=i+1

    legend_decorate(axes['alpha'].legend(loc=0,fontsize='x-small'))
    axes['alpha'].set_ylim(0,5)
    axes['R'].set_ylim(1.e-3,1.e0)
    axes['gamma'].set_ylim(1.e-2,1.e1)
    axes=self.fig.axes
    plt.setp(axes,'yscale','log')
    plt.setp(axes[1],'yscale','linear')
    plt.setp(axes,'xlim',(-512,512))
    plt.setp([ax.get_xticklabels() for ax in axes[:ncol*(nrow-1)]],visible=False)
    plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$z$ [pc]')
    plt.draw()
    plt.tight_layout()

  def plot_balance(self,vpdata):
    nrow=3
    ncol=2
    i=0
    for label in self.labels:
      tavg=vpdata.tavg[label]
      ax = self.fig.add_subplot(nrow,ncol,i+1)
      Pturb = tavg.turbulent_pressure*vpdata.units['pressure'].cgs.value
      Pth = tavg.pressure*vpdata.units['pressure'].cgs.value
      if label in self.labels_mhd:
        Pmag = tavg.magnetic_support*vpdata.units['magnetic_pressure'].cgs.value
        oPmag = tavg.ord_magnetic_support*vpdata.units['magnetic_pressure'].cgs.value
        tPmag = tavg.turb_magnetic_support*vpdata.units['magnetic_pressure'].cgs.value
      else:
        Pmag = 0.
        oPmag = 0.
        tPmag = 0.
      Ptot = np.array(Pturb + Pth + Pmag)/c.k_B.cgs.value
      oPtot = np.array(Pturb + Pth + oPmag)/c.k_B.cgs.value
      tPtot = np.array(Pturb + Pth + tPmag)/c.k_B.cgs.value

      den = tavg.density*vpdata.units['density'].cgs.value
      phi = tavg.gravitational_potential*(1.0*vpdata.units['gravitational_potential']).cgs.value
      z=np.array(den.index)
      den = np.array(den)
      phi = np.array(phi)

      zcoord,gz = get_gravity(z,phi)

      denmid=0.5*(den[1:]+den[:-1])
      dz=np.diff(z)*c.pc.cgs.value

      #weight=np.cumsum(-denmid*gz*dz)/c.k_B.cgs.value
      ax.plot(z,Ptot-Ptot[-1])
      #ax.plot(zcoord,weight)

      weight=np.cumsum(denmid[::-1]*gz[::-1]*dz)/c.k_B.cgs.value
      ax.plot(zcoord,weight[::-1])

      #ax.plot(z.to('pc'),oPtot.cgs)
      #ax.plot(z.to('pc'),tPtot.cgs)
      ax.text(0.05,0.90,panels[i]+label,transform=ax.transAxes,fontsize=20,path_effects=[myeffect])
      i=i+1


    lines=ax.lines
    lines[0].set_label(r'$\Delta P_{\rm tot}(z)$')
    lines[1].set_label(r'$\mathcal{W}(z)$')
    legend_decorate(ax.legend(loc=0))

    axes=self.fig.axes
    plt.setp(axes[0::ncol],'ylabel',r'$P/k_B$ [cm$^{-3}$ K]')
    plt.setp([ax.get_yticklabels() for ax in axes],visible=False)
    plt.setp([ax.get_yticklabels() for ax in axes[0::ncol]],visible=True)
    plt.setp([ax.get_xticklabels() for ax in axes[:ncol*(nrow-1)]],visible=False)
    plt.setp(axes,'xlim',(-512,512))
    plt.setp(axes,'ylim',(0,1.e4))
    plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$z$ [pc]')
    plt.draw()
    plt.tight_layout()

  def plot_init(self):
    nrow=2
    ncol=2
    fields=['number_density','turbulent_pressure','pressure','magnetic_support']
    i=0
    for field in fields:
      ax = self.fig.add_subplot(nrow,ncol,i+1)
      for label in self.labels:
        x,y = self._get_z_field(label,field,trange=[0,1])
        ax.plot(x,y,label=label)
        ax.text(0.05,0.90,panels[i],transform=ax.transAxes,fontsize=20,path_effects=[myeffect])
      ax.set_ylabel(flabels[field]+ulabels[field])
      i=i+1

    axes=self.fig.axes

    legend=axes[1].legend(loc=0,fontsize='x-small')
    legend_decorate(legend)

    plt.setp([ax.get_xticklabels() for ax in axes[:ncol*(nrow-1)]],visible=False)

    plt.setp(axes,'yscale','log')

    plt.setp(axes,'ylim',(10,1.e4))
    plt.setp(axes[0],'ylim',(1.e-3,10))
    plt.setp(axes,'xlim',(-512,512))
    plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$z$ [pc]')
    plt.draw()
    plt.tight_layout()

    for label in self.labels:
      z,den=self._get_z_field(label,'density',trange=[0,1])
      Nz=len(z)
      dz=z[1]-z[0]
      surf=np.sum(den*dz).to('Msun/pc**2')
      rho0=np.interp(0,z,den)*den.unit
      rhomid=0.5*(den[Nz/2-1]+den[Nz/2])
      H=get_scaleH(z,den,moment=1)
      H2=get_scaleH(z,den,moment=2)
      print label, surf, rho0, rhomid, H, H2, get_zetad(z,den)

  def plot_pressure(self,mode='zprof',model_first=False):
    ncol=2
    fields=['total_pressure','turbulent_pressure','pressure','magnetic_support','ord_magnetic_support','turb_magnetic_support']
    fields=['turbulent_pressure','pressure','ord_magnetic_support','turb_magnetic_support']
    nrow=len(fields)/ncol
    i=0

    units=self.vpdata.units
    if model_first:
      for label in self.labels:
        ax = self.fig.add_subplot(nrow,ncol,i+1)
        tavg=self.vpdata.tavg[label]
        mid = self.vpdata.zavg_mid[label]
        for field in fields:
          if mode is 'zprof': 
            if field in tavg:
              y = tavg[field]*units[field].cgs.value/c.k_B.cgs.value
            else:
              y = pd.Series(np.zeros(len(tavg.index)),index=tavg.index)
          elif mode is 'tevol': 
            ax.grid(True)
            if field in mid:
              y = mid[field]*units[field].cgs.value/c.k_B.cgs.value
            else:
              y = pd.Series(np.zeros(len(mid.index)),index=mid.index)
          ax.plot(y.index,y,label=flabels[field],color=self.colors[label])
        ax.text(0.05,0.90,panels[i]+label,transform=ax.transAxes,fontsize=20,path_effects=[myeffect])
        i=i+1
    else:
      for field in fields: 
        ax = self.fig.add_subplot(nrow,ncol,i+1)
        for label in self.labels:
          tavg=self.vpdata.tavg[label]
          mid = self.vpdata.zavg_mid[label]
          if mode is 'zprof': 
            if field in tavg:
              y = tavg[field]*units[field].cgs.value/c.k_B.cgs.value
            else:
              y = pd.Series(np.zeros(len(tavg.index)),index=tavg.index)
          elif mode is 'tevol': 
            ax.grid(True)
            if field in mid:
              y = mid[field]*units[field].cgs.value/c.k_B.cgs.value
            else:
              y = pd.Series(np.zeros(len(mid.index)),index=mid.index)
          ax.plot(y.index,y,label=label,color=self.colors[label])
        ax.text(0.05,0.90,panels[i]+flabels[field],transform=ax.transAxes,fontsize=20,path_effects=[myeffect])
        i=i+1

    axes=self.fig.axes

    legend=axes[-1].legend(loc=0,fontsize='x-small')
    legend_decorate(legend)

    plt.setp([ax.get_xticklabels() for ax in axes[:ncol*(nrow-1)]],visible=False)
    plt.setp([ax.get_yticklabels() for ax in axes],visible=False)
    plt.setp([ax.get_yticklabels() for ax in axes[0::ncol]],visible=True)

    plt.setp(axes,'yscale','log')
    plt.setp(axes[0::ncol],'ylabel',r'$P/k_B$ [cm$^{-3}$ K]')

    if mode is 'zprof':
      plt.setp(axes,'ylim',(10,2.e4))
      plt.setp(axes,'xlim',(-512,512))
      plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$z$ [pc]')

    if mode is 'tevol':
      plt.setp(axes,'ylim',(1.e2,5.e4))
      plt.setp(axes,'xlim',(0,800))
      plt.setp([ax.get_xticklabels() for ax in axes[ncol*(nrow-1):]], rotation=45)
      plt.setp([ax.lines for ax in self.fig.axes],'linewidth',1)
      plt.setp(axes[ncol*(nrow-1):],'xlabel',r'$t $ [Myr]')
    plt.draw()
    plt.tight_layout()

  def plot_table(self):
    Ptot = ((mean['turbulent_pressure']+mean['pressure']+mean['magnetic_pressure']-2.0*mean['magnetic_energy3'])/c.k_B).cgs.value
    Ptot_err = (np.sqrt(std['turbulent_pressure']**2+std['pressure']**2+std['magnetic_pressure']**2+2.0*std['magnetic_energy3']**2)/c.k_B).cgs.value
    Ptot_err = get_logerror(Ptot,Ptot_err)
    sfr = mean['sfr'].value
    sfr_err = std['sfr'].value
    sfr_err = get_logerror(sfr,sfr_err)
    ax.errorbar(Ptot,sfr,xerr=Ptot_err,yerr=sfr_err,marker='s',ms=10,alpha=0.5)
    Ptot = ((mean['turbulent_pressure']+mean['pressure']+mean['turb_magnetic_support'])/c.k_B).cgs.value
    Ptot_err = (np.sqrt(std['turbulent_pressure']**2+std['pressure']**2+std['turb_magnetic_support']**2)/c.k_B).cgs.value
    Ptot_err = get_logerror(Ptot,Ptot_err)
    ax.errorbar(Ptot,sfr,xerr=Ptot_err,yerr=sfr_err,marker='o',color=ax.lines[-1].get_color(),ms=10,label=label)

    legend_decorate(ax.legend(loc=0))
    x=np.logspace(3,5,100)
    y=2.1e-3*(x/1.e4)**(1.18)
    ax.plot(x,y,color='k')
    #y=1.8e-3*(x/1.e4)**(1.13)
    ax.plot(x,y,color='k',ls=':')

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylim(1.e-4,5.e-3)
    ax.set_xlim(1.e3,5.e4)
    ax.set_xlabel(flabels['total_pressure']+ulabels['total_pressure'])
    ax.set_ylabel(flabels['sfr']+ulabels['sfr'])

    plt.draw()
    plt.tight_layout()

    self.save(8,8,'sfr.pdf')

 
  def save(self,width,height,file):
    fig=self.fig
    fig.set_figwidth(width)
    fig.set_figheight(height)
    fig.savefig(self.base+file,bbox_inches='tight')

  def unit_conversion(self,y,field):
    if self.units.has_key(field):
      units = self.units[field]
      y = y*units.cgs
      if 'pressure' in field: y = y/c.k_B.cgs
      if 'energy' in field: y = y/c.k_B.cgs
      if 'support' in field: y = y/c.k_B.cgs

    return y

  def _get_t_field_weighted(self,label,field,wfield='density',zrange=[0,512]):
    vprof=self.vprof[label]
    x=vprof['time']
    if field in vprof: 
      w = vprof[wfield]
      y = vprof[field]*w
      y = y[:,zrange[0]:zrange[1]].mean(axis=1)/w[:,zrange[0]:zrange[1]].mean(axis=1)
    else: y=np.zeros(len(x))
    y = self.unit_conversion(y,field)
    x = x*self.ds[label].units['time'].to('Myr')
    return x,y

  def _get_t_field(self,label,field,zrange=[0,512]):
    vprof=self.vprof[label]
    x=vprof['time']
    if field in vprof: 
      y = vprof[field]
      y = y[:,zrange[0]:zrange[1]].mean(axis=1)
    else: y=np.zeros(len(x))
    y = self.unit_conversion(y,field)
    x = x*self.ds[label].units['time'].to('Myr')
    return x,y

  def _get_z_field(self,label,field,trange=[0,1]):
    vprof=self.vprof[label]
    x=vprof['axis']
    if field in vprof: 
      y = vprof[field]
      y = y[trange[0]:trange[1],:].mean(axis=0)
    else: y=np.zeros(len(x))
    y = self.unit_conversion(y,field)
    x = x*self.ds[label].units['length'].to('pc')
    return x,y


  def plot_tevol(self,ax,label,field,nwidth=0):
    dom = self.ds[label].domain 
    Nz=dom['Nx'][2]
    k1=Nz/2-1-nwidth
    k2=Nz/2+nwidth
    if field is 'total_pressure':
      x,Pturb = self._get_t_field(label,'turbulent_pressure',zrange=[k1,k2])
      x,Pth = self._get_t_field(label,'pressure',zrange=[k1,k2])
      x,Pmag = self._get_t_field(label,'magnetic_support',zrange=[k1,k2])
      y = Pturb + Pth + Pmag
    else: 
      x,y = self._get_t_field(label,field,zrange=[k1,k2])
    ax.plot(x,y,lw=1)

  def plot_zprof(self,ax,label,field):
    tavg=self.vpdata.tavg[label]
    units=self.vpdata.units
    if field in tavg:
      y = tavg[field]*units[field].cgs.value/c.k_B.cgs.value
      ax.plot(y.index,y)
    else:
      ax._get_lines.color_cycle.next()

def table1(mean,std):
    rhosd = 0.05*c.M_sun/u.pc**3
    surf = 10.*c.M_sun/u.pc**2
    rhounit = 1.4*c.m_p/u.cm**3
    vunit = u.km/u.s
    H=(surf/2.0/rhounit).to('pc').value/mean['rhomid']
    Hstd=np.sqrt(H**2*(std['rhomid']/mean['rhomid'])**2)
    zetad=mean['zeta']/H/2.0
    zetadstd=np.sqrt(zetad**2*((std['zeta']/mean['zeta'])**2+(Hstd/H)**2))
    chi=2.0*zetad*(rhosd/rhounit).cgs.value/mean['rhomid']
    chistd=np.sqrt(chi**2*((zetadstd/zetad)**2+(std['rhomid']/mean['rhomid'])**2))
    C=(4.0*rhosd*vunit**2/np.pi/c.G/surf**2).cgs.value*mean['sz']**2
    Cstd=np.sqrt(C**2*(std['sz']/mean['sz'])**2) 
    mean.insert(mean.columns.get_loc('R')+1,'H',H)
    mean['zetad']=zetad
    mean['chi']=chi
    #mean['C']=C

    std.insert(std.columns.get_loc('R')+1,'H',Hstd)
    std['zetad']=zetadstd
    std['chi']=chistd
    #std['C']=Cstd

def latex_table(mean,std,file='/scr1/cgkim/Dropbox/Papers/SFR_MHD/table.tex'):

    fp = open(file,'w')
    fp.write('\\begin{deluxetable}{l'+'c'*len(mean.columns)+'}\n')
    fp.write('\\tabletypesize{\\footnotesize}\n')

    fp.write('\\tablehead{\n')
    fp.write('\\colhead{Model}')
    for field in mean.columns:
      fp.write(' &\n\\colhead{'+flabels[field]+'}')
    fp.write('} \n')
    fp.write('\\startdata\n')

    format=' &\t $%5.2f \pm %5.2f$'
    for model in mean.index:
      fp.write(model)
      for field in mean.columns:
        if field is 'sfr': factor=1.e-3
        else: factor=1
        fp.write(format % (mean[field][model]/factor,std[field][model]/factor))
      fp.write(' \\\\ \n')

    fp.write('\\enddata\n')
    fp.write('\\end{deluxetable}\n')
    fp.close()




class table(object):
  def __init__(self,vpdata):
    self.vpdata=vpdata
    self.setup()

  def wirte_table(self,dt=100):

    dt=dt*u.Myr
    vfields_mid=['turbulent_pressure','pressure','magnetic_pressure','magnetic_energy3','magnetic_field2','turb_magnetic_support','magnetic_support']
    vfields_mw=['turbulent_velocity3','sound_speed','turbulent_alfven_velocity',]
    hfields=['sfr',]
    mean={}
    std={}
    ax=self.fig.add_subplot(111)
    fields_table=['sfr','turbulent_velocity3','sound_speed','turbulent_alfven_velocity','magnetic_field2','turbulent_pressure','pressure','magnetic_pressure','H','Hmag']

    fp = open('/scr1/cgkim/Dropbox/Papers/SFR_MHD/table.tex','w')

    fp.write('\\tablehead{\n')
    fp.write('\\colhead{Model}')
    for field in fields_table:
      fp.write(' &\n\\colhead{'+flabels[field]+'}')
    fp.write('} \n')
    fp.write('\\startdata\n')

    for label in self.labels:
      h = hst.hstwu[label]
      htime = h['time']
      vprof = self.vprof[label]
      units = self.units
      time = vprof['time']*units['time'].to('Myr')
      z = vprof['axis']*units['length'].to('pc')
      Nz = len(z)
      Nt = len(time)
      ind, = np.where(time > (max(time)-dt))
      hind, = np.where(htime > (max(htime)-dt))

      den = vprof['density']*units['density'].cgs
      if 'magnetic_pressure' in vprof:
        Pmag = vprof['magnetic_field2']
      else:
        Pmag = np.zeros(den.shape)
      zcoord2 = np.tile((vprof['axis']**2).reshape(1,Nz),(Nt,1))*units['length']**2

      for field,wfield in zip(['H','Hmag'],[den,Pmag]):
        if wfield[0][0] == 0.0: mean[field]='nodata'
        else:
          data = zcoord2*wfield
          data = np.sqrt(data.mean(axis=1)/wfield.mean(axis=1))
          iclip,mean[field],std[field]=get_mean_std(data[ind],nsigma=3)
          if iclip>1: print field,iclip

      width = np.min([int(4.0*(mean['H'].to('pc')/(z[1]-z[0])).value), Nz/2-1])

      for field in vfields_mid: 
        if field in vprof:
          data = vprof[field]*units[field].cgs
          data_min = 0.5*(data[:,Nz/2-1-width] + data[:,Nz/2+width])
          data = data[:,Nz/2-1:Nz/2].mean(axis=1)
          data = data-data_min
          iclip,mean[field],std[field]=get_mean_std(data[ind],nsigma=3)
          if iclip>1: print field,iclip
        else:
          mean[field]='nodata'

      for field in vfields_mw:
        if field is 'turbulent_velocity3':
          data = 2.0*vprof['kinetic_energy3']*units['kinetic_energy3'].cgs
        elif field is 'sound_speed':
          data = vprof['pressure']*units['pressure'].cgs
        elif field is 'turbulent_alfven_velocity':
          if 'magnetic_pressure' in vprof: 
            data = 2.0*vprof['turb_magnetic_pressure']*units['turb_magnetic_pressure'].cgs
          else:
            mean[field]='nodata'
            break
        den = vprof['density']*units['density'].cgs
        data = np.sqrt(data.mean(axis=1)/den.mean(axis=1))
        iclip,mean[field],std[field]=get_mean_std(data[ind],nsigma=3)
        if iclip>1: print field,iclip
      
      for field in hfields:
        data = h[field]
        tmean = data[hind].mean()
        t2mean = (data[hind]**2).mean()
        tstd = np.sqrt(t2mean-tmean**2)
        iclip,mean[field],std[field]=get_mean_std(data[hind],nsigma=3)
        if iclip>1: print field,iclip


      fp.write(label)
      for field in fields_table:
        if mean[field] is 'nodata': fp.write(' &\t \\nodata')
        else:
          format=' &\t $%5.2f \pm %5.2f$'
          if 'pressure' in field or 'energy' in field or 'support' in field:
            outstr=format % ((mean[field]/c.k_B/1.e3).cgs.value,(std[field]/c.k_B/1.e3).cgs.value)
          elif 'velocity' in field or 'speed' in field:
            outstr=format % (mean[field].to('km/s').value,std[field].to('km/s').value)
          elif 'sfr' in field:
            outstr=format % (mean[field].value/1.e-3,std[field].value/1.e-3)
          elif 'magnetic_field' in field:
            outstr=format % (mean[field].cgs.value/1.e-6,std[field].cgs.value/1.e-6)
          elif 'H' in field:
            outstr=' &\t $%3.0f \pm %3.0f$' % (mean[field].to('pc').value,std[field].to('pc').value)
          else:
            print field+': no rule for this field'
          fp.write(outstr)
      fp.write(' \\\\ \n')

       

    fp.write('\\enddata')
    fp.close()



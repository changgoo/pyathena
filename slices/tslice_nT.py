import matplotlib as mpl
mpl.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid, make_axes_locatable
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.gridspec as gridspec
import pyathena as pa
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
import pandas as pd
from scatter_sp import scatter_sp
import astropy.constants as c
import astropy.units as u
from matplotlib.colors import LogNorm,SymLogNorm,NoNorm,Normalize
import glob
import os

plt.rc('font',size=14)
plt.rc('xtick',labelsize=14)
plt.rc('ytick',labelsize=14)

aux={}
aux['number_density']={'title':r'$n_{\rm H} [{\rm cm}^{-3}]$',
  'cmap':'RdYlBu_r','cmin':1.e-5,'cmax':1.e2,
  'factor':1.0,'norm':LogNorm()}
aux['temperature']={'title':r'$T [{\rm K}]$',
  'cmap':'Spectral_r','cmin':10,'cmax':1.e7,
  'factor':1.0,'norm':LogNorm()}
aux['surface_density']={'title':r'$\Sigma [{\rm M}_{\odot} {\rm pc}^{-2}]$',
  'cmap':'pink_r','cmin':0.5,'cmax':100,
  'factor':1.0,'norm':LogNorm()}
aux['velocity3']={'title':r'$v_z [{\rm km/s}]$',
  'cmap':'RdBu_r','cmin':-100,'cmax':100,
  'factor':1.0,'norm':Normalize()}
aux['velocity']={'title':r'$v [{\rm km/s}]$',
  'cmap':'jet','cmin':1,'cmax':1000,
  'factor':1.0,'norm':LogNorm()}
aux['Bmag']={'title':r'$|B| [\mu{\rm G}]$',
  'cmap':'pink_r','cmin':0.1,'cmax':10,
  'factor':1.0,'norm':LogNorm()}
def texteffect():
    try:
        from matplotlib.patheffects import withStroke
        myeffect = withStroke(foreground="w", linewidth=3)
        kwargs = dict(path_effects=[myeffect], fontsize=12)
    except ImportError:
        kwargs = dict(fontsize=12)
    return kwargs

def draw_slice(slice,ax):
    im=ax.imshow(slice.data,origin='lower')
    im.set_extent(slice.bound)
    ax.set_xlabel(slice.axis_labels[0])
    ax.set_ylabel(slice.axis_labels[1])

def axtext(ax,x,y,text):
    ax.text(x,y,text,size=20, horizontalalignment='center',
              transform = ax.transAxes,**(texteffect()))
   
def tslice_nT(interval=1,ncols=0,imin=0,legend=True,cbar=True,labels=True,dpi=150,**kwargs):
    
    unit=pa.set_units(muH=1.4271)
    cf=pa.coolftn()

    dir = kwargs['base_directory']+kwargs['directory']
    dsfname=dir+'pickles/%s.0000.ds.p' % (kwargs['id'])
    if not os.path.isdir(dir+'png/slices'): os.mkdir(dir+'png/slices')

    domain=pickle.load(open(dsfname,'rb')).domain
    Lx=domain['Lx'][0]
    Lz=domain['Lx'][2]
    Nx=domain['Nx'][0]
    Nz=domain['Nx'][2]
    ix=Lx/512.
    iz=Lz/512.
    izt=2*ix+iz+ix

    fig=plt.figure(0,figsize=(ix*2,izt))
    surffname=glob.glob(kwargs['base_directory']+kwargs['directory']+'pickles/%s.????.surf.0.p' % (kwargs['id']))
    if ncols == 0: ncols=len(surffname)

    for i in range(ncols):
        itime=i*interval+imin
        stime='%4.4d' % (itime)
        surffname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.surf.0.p' % (kwargs['id'],stime)
        surf=pickle.load(open(surffname,'rb'))
        slc1fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.0.p' % (kwargs['id'],stime)
        slc1=pickle.load(open(slc1fname,'rb'))
        slc2fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.1.p' % (kwargs['id'],stime)
        slc2=pickle.load(open(slc2fname,'rb'))

                
        vtkfname=kwargs['base_directory']+kwargs['directory']+'id0/%s.%s.vtk' % (kwargs['id'],stime)
    
        ds=pa.AthenaDataSet(vtkfname,setgrid=False)
        sp = pd.DataFrame(ds.read_starvtk())
        gs = gridspec.GridSpec(3,2,height_ratios=[ix*2,iz,ix],hspace=0.01,wspace=0.01)

        ax0 = plt.subplot(gs[0, :])
        ax=ax0
        f='surface_density'
        norm=aux[f]['norm']

        scatter_sp(sp,ax,axis=0,type='surf',runaway=False)
        imdata=surf['density'].data*unit['density'].cgs*aux[f]['factor']*domain['Lx'][2]*c.pc
        imdata=imdata.to('Msun/pc^2')

        im=ax.imshow(imdata.value,extent=np.array(surf['density'].bound)/1.e3,norm=norm,origin='lower')
        im.set_cmap(aux[f]['cmap'])
        im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
        ax.text(0.5, 1.2,r't=%4.2f$\,t_{\rm orb}$' % (itime/224.), horizontalalignment='center',transform = ax.transAxes)
        
        pngname=kwargs['base_directory']+kwargs['directory']+'png/%s.%s.surf.png' % (kwargs['id'],stime)

        slc=[slc1,slc2]
        
        for  j,f in enumerate(['number_density','temperature']):
            ax1 = plt.subplot(gs[1,j])
            ax2 = plt.subplot(gs[2,j])
            axes=[ax1,ax2]
            for ax,iax in zip(axes,[1,0]):
                if f is 'temperature' and slc[iax].has_key('T1'):
                    scatter_sp(sp,ax,axis=iax,thickness=10,type='slice',runaway=True)
                    slc[iax][f].data = cf.get_temp(slc[iax]['T1'].data)
                else:
                    scatter_sp(sp,ax,axis=iax,thickness=10,type='slice',runaway=False)
                imdata=slc[iax][f].data*unit[f].cgs*aux[f]['factor']
            
                im=ax.imshow(imdata.value,extent=np.array(slc[iax][f].bound)/1.e3,norm=aux[f]['norm'],origin='lower')
                im.set_cmap(aux[f]['cmap'])
                im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
                


            
# labels
        axes=fig.axes
        plt.setp([ax.get_xticklabels() for ax in axes],visible=False)
        plt.setp([ax.get_yticklabels() for ax in axes],visible=False)

        if labels==True:
            axtext(ax0,0.5, 0.9,r'$\Sigma$')
            ax0.set_xlabel('x [kpc]')
            ax0.set_ylabel('y [kpc]')
            ax0.xaxis.set_label_position('top')
            ax0.xaxis.tick_top()
            ax0.xaxis.set_ticks_position('both')
            
            
            plt.setp([ax.get_xticklabels() for ax in [axes[0],axes[2],axes[4]]], visible=True)
            plt.setp([ax.get_yticklabels() for ax in [axes[0],axes[1],axes[2]]], visible=True)
            plt.setp([ax.xaxis.get_majorticklabels() for ax in [axes[2],axes[4]]], rotation=45 )

            axes[1].text(0.5, 0.95,r'$n_H$', size=20, horizontalalignment='center',transform = axes[1].transAxes,**(texteffect()))
            axes[3].text(0.5, 0.95,r'$T$', size=20,horizontalalignment='center',transform = axes[3].transAxes,**(texteffect()))
            axes[1].set_ylabel('z [kpc]')
            axes[2].set_ylabel('y [kpc]')
            axes[2].set_xlabel('x [kpc]')
            axes[4].set_xlabel('x [kpc]')
# legend
        if legend==True:
            ext=ax0.images[0].get_extent()
   
            s1=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e3)/4.,color='k',alpha=.5,label=r'$10^3 M_\odot$')
            s2=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e4)/4.,color='k',alpha=.5,label=r'$10^4 M_\odot$')
            s3=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e5)/4.,color='k',alpha=.5,label=r'$10^5 M_\odot$')
            ax0.set_xlim(ext[0],ext[1])
            ax0.set_ylim(ext[2],ext[3])
            ax0.legend((s1,s2,s3),(r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), \
                       scatterpoints=1, bbox_to_anchor=(1.01, 1),loc=2,fontsize=12, frameon=False)
# colorbar
            cax3 = fig.add_axes([0.93, 0.1, 0.05, 0.15])
            cax2 = fig.add_axes([0.93, 0.3, 0.05, 0.15])
            cax1 = fig.add_axes([0.93, 0.5, 0.05, 0.15])
            cax4 = fig.add_axes([0.93, 0.7, 0.05, 0.1])

            for f,cax in zip(['surface_density','number_density','temperature'],[cax1,cax2,cax3]):
                cmap = plt.get_cmap(aux[f]['cmap'])
                norm = LogNorm(vmin=aux[f]['cmin'], vmax=aux[f]['cmax'])
                cb = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm, orientation='vertical')
                cb.set_label(aux[f]['title'])
            cb = mpl.colorbar.ColorbarBase(cax4, ticks=[0,20,40],cmap=plt.cm.cool_r, norm=plt.Normalize(vmin=0,vmax=40), orientation='vertical')
            cb.set_label(r'${\rm age [Myr]}$')
        
        pngname=kwargs['base_directory']+kwargs['directory']+'png/slices/%s.%s.png' % (kwargs['id'],stime)
                       
        canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
        canvas.print_figure(pngname,bbox_inches='tight',num=0,dpi=dpi)
        fig.clf()
        print pngname
    return fig

def tslice_fields(interval=1,ncols=0,imin=0,legend=True,cbar=True,labels=True,dpi=150,**kwargs):
    
        
    cf=pa.coolftn(fname='../coolftn.p')

    dir = kwargs['base_directory']+kwargs['directory']
    dsfname=dir+'pickles/%s.0000.ds.p' % (kwargs['id'])
    if not os.path.isdir(dir+'png/4slices'): os.mkdir(dir+'png/4slices')

    domain=pickle.load(open(dsfname,'rb')).domain
    Lx=domain['Lx'][0]
    Lz=domain['Lx'][2]
    Nx=domain['Nx'][0]
    Nz=domain['Nx'][2]
    le=domain['left_edge']
    re=domain['right_edge']
    boundxy=np.array([le[0],re[0],le[1],re[1]])
    boundxz=np.array([le[0],re[0],le[2],re[2]])
    boundyz=np.array([le[1],re[1],le[2],re[2]])
    bound=[boundxy,boundxz,boundyz]
    dx=domain['dx'][0]
    fields=['number_density','temperature','velocity3','Bmag']
    nf=len(fields)
    ix=Lx/512.
    iz=Lz/512.
    Surf=False
    if Surf: izt=2*ix+iz+ix
    else: izt=0.1*ix+iz+ix
    if legend: fig=plt.figure(0,figsize=(ix*nf*1.1,izt))
    else: fig=plt.figure(0,figsize=(ix*nf,izt))
    surffname=glob.glob(kwargs['base_directory']+kwargs['directory']+'pickles/%s.????.surf.0.p' % (kwargs['id']))
    if ncols == 0: ncols=len(surffname)

    for i in range(ncols):
        itime=i*interval+imin
        stime='%4.4d' % (itime)
        surffname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.surf.0.p' % (kwargs['id'],stime)
        surf=pickle.load(open(surffname,'rb'))
        slc1fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.0.p' % (kwargs['id'],stime)
        slc1=pickle.load(open(slc1fname,'rb'))
        slc2fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.1.p' % (kwargs['id'],stime)
        slc2=pickle.load(open(slc2fname,'rb'))

                
        vtkfname=kwargs['base_directory']+kwargs['directory']+'id0/%s.%s.vtk' % (kwargs['id'],stime)
    
        ds=pa.AthenaDataSet(vtkfname,setgrid=False)
        unit=pa.set_units(muH=1.4271)
        sp = pd.DataFrame(ds.read_starvtk())
        if Surf:
          gs = gridspec.GridSpec(3,nf,height_ratios=[ix*2,iz,ix],hspace=0.01,wspace=0.01)
        else:
          gs = gridspec.GridSpec(3,nf,height_ratios=[ix*0.1,iz,ix],hspace=0.01,wspace=0.01)

        ax0 = plt.subplot(gs[0, :2])
        if Surf:
          ax=ax0
          f='surface_density'
          norm=aux[f]['norm']

          scatter_sp(sp,ax,axis=0,type='surf',runaway=False)
          imdata=surf['density'].data*(unit['density'].cgs*aux[f]['factor']*Lz*c.pc).to('Msun/pc^2')

          im=ax.imshow(imdata.value,extent=bound[0]/1.e3,norm=norm,origin='lower')
          im.set_cmap(aux[f]['cmap'])
          im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
        else:
          plt.axis('off')
        
        slc=[slc1,slc2]
        
        for  j,f in enumerate(fields):
            ax1 = plt.subplot(gs[1,j])
            ax2 = plt.subplot(gs[2,j])
            axes=[ax1,ax2]
            norm=aux[f]['norm']
            for ax,iax in zip(axes,[1,0]):
                if f is 'temperature' and slc[iax].has_key('T1'):
                    scatter_sp(sp,ax,axis=iax,thickness=10,type='slice',runaway=True)
                    slc[iax][f].data = cf.get_temp(slc[iax]['T1'].data)
                    imdata=slc[iax][f].data*unit['temperature'].cgs*aux[f]['factor']
                elif f is 'Bmag':
                    B1=slc[iax]['magnetic_field1'].data
                    B2=slc[iax]['magnetic_field2'].data
                    B3=slc[iax]['magnetic_field3'].data
                    Bmag=np.sqrt(B1**2+B2**2+B3**2)
                    imdata=Bmag*unit['magnetic_field'].to('microGauss')
                elif f is 'velocity3':
                    #scatter_sp(sp,ax,axis=iax,thickness=10,type='slice',runaway=False)
                    imdata=slc[iax][f].data*unit['velocity'].to('km/s')
                elif f is 'velocity':
                    v1=slc[iax]['velocity1'].data
                    v2=slc[iax]['velocity2'].data
                    v3=slc[iax]['velocity3'].data
                    vmag=np.sqrt(v1**2+v2**2+v3**2)
                    imdata=vmag*unit['velocity'].to('km/s')
                elif f is 'number_density':
                    scatter_sp(sp,ax,axis=iax,thickness=10,type='slice',runaway=False)
                    imdata=slc[iax][f].data*u.cm**(-3)*aux[f]['factor']
            
                if f is 'velocity3':
                  im=ax.imshow(imdata.value,extent=bound[iax]/1.e3,
                  origin='lower')
                else:
                  im=ax.imshow(imdata.value,extent=bound[iax]/1.e3,
                  norm=norm,origin='lower')
                im.set_cmap(aux[f]['cmap'])
                im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
                if f is 'velocity':
                  b=bound[iax]
                  x,y=np.meshgrid(np.arange(b[0],b[1],dx),np.arange(b[2],b[3],dx))
                  if iax == 0:
                    vx=slc[iax]['velocity1'].data
                    vy=slc[iax]['velocity2'].data
                  elif iax == 1:
                    vx=slc[iax]['velocity1'].data
                    vy=slc[iax]['velocity3'].data
                  vmax=200.
                  nskip=8
                  vx[vx>vmax]=vmax
                  vy[vy>vmax]=vmax
                  vx[vx<-vmax]=-vmax
                  vy[vy<-vmax]=-vmax
                  vmag=np.sqrt(vx**2+vy**2)
                  vec=ax.quiver(x[::nskip,::nskip]/1.e3,y[::nskip,::nskip]/1.e3,
                    vx[::nskip,::nskip],vy[::nskip,::nskip],color='r',width=0.005,
                    units='width',scale=1000,headwidth=5,headlength=10,headaxislength=10)
                  if iax == 1: qk=ax.quiverkey(vec,0.5,1.02,100,r'100 km/s',labelpos='E')
                  ax.set_xlim(b[0]/1.e3,b[1]/1.e3)
                  ax.set_ylim(b[2]/1.e3,b[3]/1.e3)
                  im.set_alpha(0.5)
                
# labels
        axes=fig.axes
        axtext(axes[3],1.0,1.06,r'$t=%4.2f\,t_{\rm orb}$' % (itime/224.))
        plt.setp([ax.get_xticklabels() for ax in axes],visible=False)
        plt.setp([ax.get_yticklabels() for ax in axes],visible=False)

        if labels==True:
            if Surf:
              axtext(ax0,0.5, 0.9,r'$\Sigma$')
              ax0.set_xlabel('x [kpc]')
              ax0.set_ylabel('y [kpc]')
              ax0.xaxis.set_label_position('top')
              ax0.xaxis.tick_top()
              ax0.xaxis.set_ticks_position('both')
            
            plt.setp([ax.get_xticklabels() for ax in axes[0::2]], visible=True)
            plt.setp([ax.get_yticklabels() for ax in axes[:3]], visible=True)
            plt.setp([ax.xaxis.get_majorticklabels() for ax in axes[2::2]], rotation=45 )

            axtext(axes[1],0.5,1.02,r'$n_H$')
            axtext(axes[3],0.5,1.02,r'$T$')
            axtext(axes[5],0.5,1.02,r'$v_z$')
            #axtext(axes[5],0.5,0.95,r'$v$')
            axtext(axes[7],0.5,1.02,r'$|B|$')
            axes[1].set_ylabel('z [kpc]')
            axes[2].set_ylabel('y [kpc]')
            plt.setp(axes[2::2],'xlabel','x [kpc]')
# legend
        if legend==True:
            ax=axes[7]
            ext=ax.images[0].get_extent()
   
            s1=ax.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e3)/4.,color='k',alpha=.5,label=r'$10^3 M_\odot$')
            s2=ax.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e4)/4.,color='k',alpha=.5,label=r'$10^4 M_\odot$')
            s3=ax.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e5)/4.,color='k',alpha=.5,label=r'$10^5 M_\odot$')
            ax.set_xlim(ext[0],ext[1])
            ax.set_ylim(ext[2],ext[3])
            ax.legend((s1,s2,s3),(r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), \
                       scatterpoints=1, bbox_to_anchor=(1.05, 1.08),loc=2,fontsize=12, frameon=False)
# colorbar
            cax5 = fig.add_axes([0.92, 0.10 , 0.03, 0.13])
            cax4 = fig.add_axes([0.92, 0.25, 0.03, 0.13])
            cax3 = fig.add_axes([0.92, 0.40, 0.03, 0.13])
            cax2 = fig.add_axes([0.92, 0.55, 0.03, 0.13])
#            cax1 = fig.add_axes([0.53, 0.69, 0.03, 0.1])
            cax0 = fig.add_axes([0.92, 0.70, 0.03, 0.13])

            for f,cax in zip(fields,[cax2,cax3,cax4,cax5]):
                cmap = plt.get_cmap(aux[f]['cmap'])
                norm = aux[f]['norm']
                norm.vmin=aux[f]['cmin']
                norm.vmax=aux[f]['cmax']
                cb = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm, orientation='vertical')
                if f.startswith('velocity3'): cb.set_ticks([-100,-50,0,50,100])
                cb.set_label(aux[f]['title'])
            cb = mpl.colorbar.ColorbarBase(cax0, ticks=[0,20,40],cmap=plt.cm.cool_r, norm=plt.Normalize(vmin=0,vmax=40), orientation='vertical')
            cb.set_label(r'${\rm age [Myr]}$')
        
        pngname=kwargs['base_directory']+kwargs['directory']+'png/4slices/%s.%s.png' % (kwargs['id'],stime)
                       
        canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
        canvas.print_figure(pngname,bbox_inches='tight',num=0,dpi=dpi)
        fig.clf()
        print pngname
    return fig

def surf(interval=1,ncols=0,imin=0,legend=True,cbar=True,labels=True,dpi=150,**kwargs):
    
    unit=pa.set_units(muH=1.4271)
    cf=pa.coolftn(fname='../coolftn.p')

    dir = kwargs['base_directory']+kwargs['directory']
    dsfname=dir+'pickles/%s.0000.ds.p' % (kwargs['id'])
    if not os.path.isdir(dir+'png/surfs'): os.mkdir(dir+'png/surfs')

    domain=pickle.load(open(dsfname,'rb')).domain
    Lx=domain['Lx'][0]
    Lz=domain['Lx'][2]
    Nx=domain['Nx'][0]
    Nz=domain['Nx'][2]
    ix=Lx/512.
    iz=Lz/512.
    izt=2*ix+iz+ix

    fig=plt.figure(0,figsize=(10,10))
    surffname=glob.glob(kwargs['base_directory']+kwargs['directory']+'pickles/%s.????.surf.0.p' % (kwargs['id']))
    if ncols == 0: ncols=len(surffname)

    for i in range(ncols):
        itime=i*interval+imin
        stime='%4.4d' % (itime)
        surffname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.surf.0.p' % (kwargs['id'],stime)
        surf=pickle.load(open(surffname,'rb'))
        slc1fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.0.p' % (kwargs['id'],stime)
        slc1=pickle.load(open(slc1fname,'rb'))
        slc2fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.1.p' % (kwargs['id'],stime)
        slc2=pickle.load(open(slc2fname,'rb'))

                
        vtkfname=kwargs['base_directory']+kwargs['directory']+'id0/%s.%s.vtk' % (kwargs['id'],stime)
    
        ds=pa.AthenaDataSet(vtkfname,setgrid=False)
        sp = pd.DataFrame(ds.read_starvtk())

        ax0 = plt.subplot(111)
        ax=ax0
        f='surface_density'
        norm=aux[f]['norm']

        scatter_sp(sp,ax,axis=0,type='surf',runaway=False)
        imdata=surf['density'].data*unit['density'].cgs*aux[f]['factor']*domain['Lx'][2]*c.pc
        imdata=imdata.to('Msun/pc^2')

        im=ax.imshow(imdata.value,extent=np.array(surf['density'].bound)/1.e3,norm=norm,origin='lower')
        im.set_cmap(aux[f]['cmap'])
        im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
        ax.text(0.5, 1.05,r't=%4.2f$\,t_{\rm orb}$' % (itime/224.), horizontalalignment='center',transform = ax.transAxes)
        
        pngname=kwargs['base_directory']+kwargs['directory']+'png/surfs/%s.%s.surf.png' % (kwargs['id'],stime)

        if labels==True:
            axtext(ax0,0.5, 0.9,r'$\Sigma$')
            ax0.set_xlabel('x [kpc]')
            ax0.set_ylabel('y [kpc]')
# legend
        if legend==True:
            ext=ax0.images[0].get_extent()
   
            s1=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e3)/4.,color='k',alpha=.5,label=r'$10^3 M_\odot$')
            s2=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e4)/4.,color='k',alpha=.5,label=r'$10^4 M_\odot$')
            s3=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e5)/4.,color='k',alpha=.5,label=r'$10^5 M_\odot$')
            ax0.set_xlim(ext[0],ext[1])
            ax0.set_ylim(ext[2],ext[3])
            ax0.legend((s1,s2,s3),(r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), \
                       scatterpoints=1, bbox_to_anchor=(1.01, 1.05),loc=2,fontsize=12, frameon=False)
# colorbar
            cax = fig.add_axes([0.93, 0.1, 0.05, 0.35])
            cax0 = fig.add_axes([0.93, 0.50, 0.05, 0.20])

            f='surface_density'
            cmap = plt.get_cmap(aux[f]['cmap'])
            norm = LogNorm(vmin=aux[f]['cmin'], vmax=aux[f]['cmax'])
            cb = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm, orientation='vertical')
            cb.set_label(aux[f]['title'])

            cb = mpl.colorbar.ColorbarBase(cax0, ticks=[0,20,40],cmap=plt.cm.cool_r, norm=plt.Normalize(vmin=0,vmax=40), orientation='vertical')
            cb.set_label(r'${\rm age [Myr]}$')
        
        canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
        canvas.print_figure(pngname,num=0,bbox_inches='tight',dpi=dpi)
        fig.clf()
        print pngname
    return fig

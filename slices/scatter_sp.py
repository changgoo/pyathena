from pyathena import set_units
from matplotlib import cm
import numpy as np

def projection(sp,axis):
    if axis == 0 or axis == 'z':
        spx=sp['x1']
        spy=sp['x2']
        spz=sp['x3']
    elif axis == 1 or axis == 'y':
        spx=sp['x1']
        spy=sp['x3']
        spz=sp['x2']
    elif axis == 2 or axis == 'x':
        spx=sp['x2']
        spy=sp['x3']
        spz=sp['x1']
    return spx,spy,spz

def projection_v(sp,axis):
    if axis == 0 or axis == 'z':
        spx=sp['v1']
        spy=sp['v2']
        spz=sp['v3']
    elif axis == 1 or axis == 'y':
        spx=sp['v1']
        spy=sp['v3']
        spz=sp['v2']
    elif axis == 2 or axis == 'x':
        spx=sp['v2']
        spy=sp['v3']
        spz=sp['v1']
    return spx,spy,spz

def scatter_sp(sp,ax,axis=0,thickness=10,norm_factor=4., \
  type='slice',kpc=True,runaway=True):
    unit=set_units(muH=1.4271)
    Msun=unit['mass'].to('Msun').value
    Myr=unit['time'].to('Myr').value
    #print len(sp)
    if len(sp) >0:
      runaways=(sp['mass'] == 0.0)
      sp_runaway=sp[runaways]
      sp_normal=sp[-runaways]
      #print len(sp_runaway)
      if len(sp_normal) > 0: 
        spx,spy,spz=projection(sp_normal,axis)
        if kpc:
            spx = spx/1.e3
            spy = spy/1.e3
        if type == 'slice': xbool=abs(spz) < thickness
        spm=np.sqrt(sp_normal['mass']*Msun)/norm_factor
        spa=sp_normal['age']*Myr
        iyoung=np.where(spa < 40.)
        #print len(iyoung[0])
        if type == 'slice':
            islab=np.where(xbool*(spa<40))
            ax.scatter(spx.iloc[islab],spy.iloc[islab],marker='o',\
                s=spm.iloc[islab],c=spa.iloc[islab],\
                vmax=40,vmin=0,cmap=cm.cool_r,alpha=1.0)
        ax.scatter(spx.iloc[iyoung],spy.iloc[iyoung],marker='o',\
            s=spm.iloc[iyoung],c=spa.iloc[iyoung],\
            vmax=40,vmin=0,cmap=cm.cool_r,alpha=0.8)
      if len(sp_runaway) > 0 and runaway:
        spx,spy,spz=projection(sp_runaway,axis)
        spvx,spvy,spvz=projection_v(sp_runaway,axis)
        if kpc:
            spx = spx/1.e3
            spy = spy/1.e3
        if type == 'slice': 
            islab=np.where(abs(spz) < thickness)
            #ax.scatter(spx.iloc[islab],spy.iloc[islab],marker='.',c='k',alpha=1.0)
            #ax.quiver(spx.iloc[islab],spy.iloc[islab],
            #      spvx.iloc[islab],spvy.iloc[islab],color='k',alpha=1.0)
        ax.scatter(spx,spy,marker='o',c='w',alpha=0.5,s=10.0/norm_factor)
        #ax.quiver(spx,spy,spvx,spvy,color='w',alpha=0.5)
 
def sp_legend(ax0,top=False,norm_factor=4.):
    ext=ax0.images[0].get_extent()

    s1=ax0.scatter(ext[1]*2,ext[3]*2,
      s=np.sqrt(1.e3)/norm_factor,color='k',
      alpha=.5,label=r'$10^3 M_\odot$')
    s2=ax0.scatter(ext[1]*2,ext[3]*2,
      s=np.sqrt(1.e4)/norm_factor,color='k',
      alpha=.5,label=r'$10^4 M_\odot$')
    s3=ax0.scatter(ext[1]*2,ext[3]*2,
      s=np.sqrt(1.e5)/norm_factor,
      color='k',alpha=.5,label=r'$10^5 M_\odot$')
    ax0.set_xlim(ext[0],ext[1])
    ax0.set_ylim(ext[2],ext[3])
    if top:
      legend=ax0.legend((s1,s2,s3),
        (r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), 
        scatterpoints=1,loc=2,ncol=3,fontsize='medium',
        bbox_to_anchor=(0.0, 1.1), frameon=False)
    else:
      legend=ax0.legend((s1,s2,s3),
        (r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), 
        scatterpoints=1, bbox_to_anchor=(1.01, 1),loc=2,fontsize='medium',
        frameon=False)

    return legend

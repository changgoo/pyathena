
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_agg
from matplotlib.colors import LogNorm
import cPickle as p
import numpy as np

import argparse
import glob
import os

def phase_plot(fig,fname,cmap=plt.cm.bone_r):
  hist=p.load(open(fname,'rb'))

  keys=['nP_volume','nT_volume','vT_volume','nP_mass','nT_mass','vT_mass'] 
  for i,k in enumerate(keys):
    ax = plt.subplot(2,3,i+1)
    h=hist[k]
    X, Y = np.meshgrid(h[1], h[2])
    im=ax.pcolormesh(X,Y,(h[0]/h[0].sum()).T,norm=LogNorm())
    im.set_clim(1.e-6,1.e-2)
    im.set_cmap(cmap)
    sp = k.split('_')
    if i%3 ==2: plt.colorbar(im,label=sp[1]+' fraction')
    if sp[0][1] == 'T': ax.set_ylabel(r'$T [{\rm K}]$')
    if sp[0][1] == 'P': ax.set_ylabel(r'$P/k_B [{\rm K}{\rm cm}^{-3}]$')
    if sp[0][0] == 'n': ax.set_xlabel(r'$n_H [{\rm cm}^{-3}$]')
    if sp[0][0] == 'v': ax.set_xlabel(r'$v [{\rm km/s}]$')
  return fig

def main(**kwargs):
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  file = glob.glob(dir+"pickles/"+id+".????.phase.p")
  file.sort()
  if not os.path.isdir(dir+'png/phase'): os.mkdir(dir+'png/phase')

  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1
  file=file[start:end:fskip]

  fig=plt.figure(0,figsize=(12,8))
  for f in file:
    print f
    phase_plot(fig,f,cmap=plt.cm.CMRmap_r)
    pngfname=dir+'png/phase/'+id+f[-13:]+'ng'
    canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
    canvas.print_figure(pngfname,bbox_inches='tight',num=0,dpi=150)
    fig.clf()

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))



#!python
"""
  handling star particle data files from Athena
"""

import re
def read(filename):
	file=open(filename,'r')
	time=[]
	mass=[]
	x1=[]
	x2=[]
	x3=[]
	v1=[]
	v2=[]
	v3=[]
	age=[]
	mdot=[]
	mhist=[]
	while 1:
		line=file.readline()
		if line == '':
			break
		else:
			split=re.split('\s',line)
			if split[0] == '#': 
				head=line
			else:
				time.append(eval(split[0]))
				mass.append(eval(split[1]))
				x1.append(eval(split[2]))
				x2.append(eval(split[3]))
				x3.append(eval(split[4]))
				v1.append(eval(split[5]))
				v2.append(eval(split[6]))
				v3.append(eval(split[7]))
				age.append(eval(split[8]))
				mdot.append(eval(split[9]))
				mhist.append(eval(split[10]))

	return time, mass, x1, x2, x3, v1, v2, v3, age, mdot, mhist

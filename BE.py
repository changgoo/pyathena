# BE sphere profile
from scipy.integrate import odeint
from pylab import *

def deriv(y,xi):
	return array([y[1]/(xi**2),-xi**2*exp(y[0])])

def deriv2(y,xi):
	return array([y[1],-2.0*y[1]/xi+exp(-y[0])])

def fig(xi,psi):
	fig = figure()
	ax1 = fig.add_subplot(2,1,1)
	ax2 = fig.add_subplot(2,1,2)

	line1 = ax1.plot(xi,exp(psi[:,0]),xi,-psi[:,0])
	ax1.set_xlabel(r'${\xi}$')
	ax1.set_ylabel(r'${\rho/\rho_0}$')

	line2 = ax2.plot(xi,-psi[:,1]*exp(psi[:,0]/2))
	ax2.plot(xicrit,mcrit,'ro')
	ax2.set_xlabel(r'${\xi_e}$')
	ax2.set_ylabel(r'${M(\xi_e)}$')

	show()

dxi=1.e-3

y0=array([-dxi**2.0/6.0,-dxi**3.0/3.0])
xi=linspace(dxi,10.0,100000)
psi=odeint(deriv,y0,xi)
psi[:,0]=-psi[:,0]
psi[:,1]=-psi[:,1]/xi**2.0

y0=array([dxi*dxi/6.,dxi/3.0])
psi2=odeint(deriv2,y0,xi)

Me=xi**2.0*psi2[:,1]*exp(-psi2[:,0]/2)
mcrit=Me.max()
xicrit=xi[Me.argmax()]
psicrit=-psi2[Me.argmax(),0]
rhocrit=exp(-psi2[Me.argmax(),0])

rho=exp(-psi[:,0])

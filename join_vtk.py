import glob
import os
import string
import subprocess
import argparse

join_vtk_bin='/u/ckim14/join_vtk/join_vtk'
join_vtk_sh='/u/ckim14/join_vtk/join_vtk.sh'

def main(**kwargs):
  base=kwargs['base_directory']
  dir=kwargs['directory']
  id=kwargs['id']
  joined_id=kwargs['id']+'_joined'
  nend = kwargs['nend']

  current_dir=os.getcwd()
  os.chdir(base+dir)

  for i in range(nend):
    tail='.%4.4d.vtk' % i
    vtkfile=base+dir+'id0/'+id+tail
    joined_vtk=base+dir+joined_id+tail
    if os.path.isfile(vtkfile):
      mtime_p= os.path.getmtime(vtkfile)
      print 'parallel vtk exists'
      if os.path.isfile(joined_vtk):
        mtime_j= os.path.getmtime(joined_vtk)
      else: 
        mtime_j= 0.
      if mtime_j < mtime_p:
        print 'joining vtk file'
        join_command = [join_vtk_sh,'-i',id,'-o',joined_id,'-c','%d:1:%d' % (i,i)]
        print string.join(join_command,' ')
        subprocess.check_call(join_command)
      else:
        print 'joined vtk exists'
        if kwargs['clean']:
          print 'Cleaning ...'
          clean_command = ['find',base+dir+'id*','-name','"%s*%s"' % (id,tail),'-delete']
          print string.join(clean_command,' ')
          #subprocess.check_call(clean_command)
    else:
      print '%s does not exist' % vtkfile

  os.chdir(current_dir)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/nobackup/ckim14/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-n','--nend',type=int,default=25,
                      help='number of files')
  parser.add_argument('-c','--clean',action='store_true',help='clean up')
  args = parser.parse_args()
  main(**vars(args))

from pyathena import *
import ath_hst
import glob
import os
import argparse
import cPickle as pickle

# matplotlib
import matplotlib
matplotlib.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc

# astropy
import astropy.constants as c
import astropy.units as u

import color_maps as cm

def main(**kwargs):
  unit=set_units(muH=1.4271)
  Myr = unit['time'].to('Myr').value
  files=[]
  files += ['/nobackup/ckim14/MHD_4pc/4pc_rst/id0/MHD_4pc.hst']
  files += ['/nobackup/ckim14/MHD_2pc/id0/MHD_2pc.hst']


  fig = matplotlib.figure.Figure(figsize=(10,10))

  hst={}
  labels=['MHD_4pc','MHD_2pc']
  for i,f in enumerate(files):
    hst[labels[i]]=ath_hst.read_w_pandas(f)
    vol=ath_hst.get_volume(f)

  ax=fig.add_subplot(211)
  for lab in labels:
    h=hst[lab]
    ax.plot(h['time'],h['dt'],label=lab,lw=1,alpha=0.9)
  ax.legend(loc=0)
  ax.set_ylabel(r'$\Delta t$')
  ax=fig.add_subplot(212)
  for lab in labels:
    h=hst[lab]
    ax.plot(h['time'],h['sfr40'],label=lab,lw=1,alpha=0.9)
  ax.set_ylabel(r'$\Sigma_{\rm SFR,40}$')
  plt.setp(fig.axes[0],'yscale','log')
  plt.setp(fig.axes,'xlabel',r'$t$')
  canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
  canvas.print_figure('history_plots.png',bbox_inches='tight')

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/nobackup/ckim14/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  args = parser.parse_args()
  main(**vars(args))

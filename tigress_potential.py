import numpy as np
import astropy.units as u
import astropy.constants as c

solar_param={'surf_s':42*u.M_sun/u.pc**2,
            'rho_dm':0.0064*u.Msun/u.pc**3,
            'z0': 0.245*u.kpc,
            'R0': 8*u.kpc,
            'surf_g':13*u.M_sun/u.pc**2,}
sb_param={'surf_s':300*u.M_sun/u.pc**2,
            'rho_dm':0.1*u.Msun/u.pc**3,
            'z0': 0.05*u.kpc,
            'R0': 1*u.kpc,
         'surf_g':200*u.M_sun/u.pc**2,}
inter_param={'surf_s':120*u.M_sun/u.pc**2,
            'rho_dm':0.03*u.Msun/u.pc**3,
            'z0': 0.12*u.kpc,
            'R0': 3*u.kpc,
         'surf_g':50*u.M_sun/u.pc**2,}

class extpot(object):
    def __init__(self,param=solar_param):
        self.surf_s=param['surf_s']
        self.rho_dm=param['rho_dm']
        self.z0=param['z0']
        self.R0=param['R0']
        self.surf_g=param['surf_g']
        self.sig1=10*u.km/u.s
        self.sig2=10*self.sig1
        
    def gext(self,z):

        a1=2*np.pi*c.G*self.surf_s
        a2=4*np.pi*c.G*self.rho_dm
        g1=-a1*z/np.sqrt(z**2+self.z0**2)
        g2=-a2*z/(z**2/self.R0**2+1)
        g_new=g1+g2
    
        return g_new

    def phiext(self,z):
        phi=2*np.pi*c.G*(self.surf_s*(np.sqrt(z**2+self.z0**2)-self.z0)
                         +self.rho_dm*self.R0**2*np.log(1+z**2/self.R0**2))
        return phi
    
    def vesc(self,z):
        return np.sqrt(2*(self.phiext(z.max()).to('km^2/s^2')-self.phiext(z).to('km^2/s^2')))
    
    def phisg(self,z,zg):
        #phi=2*np.pi*c.G*self.surf_g*np.abs(z)#(np.sqrt(z**2+zg**2)-zg)
        #gsg=-2*np.pi*c.G*self.surf_g*np.tanh((z/z0*u.pc).cgs.value)
        phi=np.log(np.cosh((z/zg).cgs.value))*2*np.pi*c.G*self.surf_g*zg
        return phi
    
    def set_n0(self,z):
    
        n0=1.0/u.cm**3
        
        for i in range(5):
            H=self.surf_g/2.0/(1.4271*c.m_p*n0)
            phitot=self.phiext(z)+self.phisg(z,H.to('pc'))
            phitot=phitot.to('km^2/s^2')

            rho10=n0*1.4*c.m_p
            rho20=0.00001*rho10
            rho1=rho10*np.exp(-(phitot-phitot.min())/self.sig1**2)
            rho2=rho20*np.exp(-(phitot-phitot.min())/self.sig2**2)
            rho=rho1+rho2
            nden=rho/1.4271/c.m_p
            
            n0=n0*(self.surf_g/(2.0*(rho.sum()*z[1]).to('Msun/pc^2'))).cgs
            print n0,H.to('pc')
            #print n0,2.0*(rho1.sum()*z[1]).to('Msun/pc^2'),2.0*(rho.sum()*z[1]).to('Msun/pc^2')
        self.n0=n0
        self.H=H.to('pc')
        
    def den(self,z):
        phitot=self.phiext(z)+self.phisg(z,self.H)
        phitot=phitot.to('km^2/s^2')
        rho10=self.n0*1.4*c.m_p
        rho20=0.00001*rho10
        rho1=rho10*np.exp(-(phitot-phitot.min())/self.sig1**2)
        rho2=rho20*np.exp(-(phitot-phitot.min())/self.sig2**2)
        rho=rho1+rho2
        nden=rho/1.4271/c.m_p

        return nden,rho1,rho2
    
    def press(self,z):
        n,rho1,rho2=self.den(z)
        P1=rho1*self.sig1**2
        P2=rho2*self.sig2**2
        pok=(P1+P2).cgs/c.k_B.cgs
        
        return pok,P1,P2
    
    def pmag(self,z,beta=10):
        pok,P1,P2=self.press(z)
        Pmag=(P1+P2)/beta

        return Pmag
    
    def Bmag(self,z,beta=10):
        Pmag=self.pmag(z,beta=beta)
        return np.sqrt(Pmag.cgs*8*np.pi)


def parse_line(line, grid):
	sp = line.strip().split()

	if "vtk" in sp:
		grid['vtk_version'] = sp[-1]
	elif "time=" in sp:
		time_index = sp.index("time=")
        	grid['time'] = float(sp[time_index+1].rstrip(','))
        	if 'level' in sp: grid['level'] = int(sp[time_index+3].rstrip(','))
        	if 'domain' in sp: grid['domain'] = int(sp[time_index+5].rstrip(','))  
		if sp[0] == "PRIMITIVE": 
			grid['prim_var_type']=True
	elif "DIMENSIONS" in sp:
		grid['Nx'] = np.array(sp[-3:]).astype('int')
	elif "ORIGIN" in sp:
		grid['left_edge'] = np.array(sp[-3:]).astype('float64')
	elif "SPACING" in sp:
		grid['dx'] = np.array(sp[-3:]).astype('float64')
	elif "CELL_DATA" in sp:
		grid['ncells'] = int(sp[-1])
	elif "SCALARS" in sp:
		grid['read_field'] = sp[1]
		grid['read_type'] = 'scalar'
	elif "VECTORS" in sp:
		grid['read_field'] = sp[1]
		grid['read_type'] = 'vector'
	elif "NSTARS" in sp:
		grid['nstar'] = eval(sp[1])
	elif "POINTS" in sp:
		grid['nstar'] = eval(sp[1])
		grid['ncells'] = eval(sp[1])


def set_field_map(grid):
	file=open(grid['filename'],'rb')
	file.seek(0,2)
	eof = file.tell()
	offset = grid['data_offset']
	file.seek(offset)

	field_map={}

	if grid.has_key('Nx'): Nx=grid['Nx']

	while offset < eof:

		line=file.readline()
		sp = line.strip().split()
		#print line,sp
			
		field_map[sp[1]] = {}
		field_map[sp[1]]['read_table']=False

		if "SCALARS" in line:
			tmp=file.readline()
			field_map[sp[1]]['read_table']=True
			field_map[sp[1]]['nvar'] = 1
		elif "VECTORS" in line:
			field_map[sp[1]]['nvar'] = 3
		else:
			print 'Error: '+sp[0] + ' is unknown type'
			raise TypeError

		field_map[sp[1]]['offset']=offset
		field_map[sp[1]]['ndata']=field_map[sp[1]]['nvar']*grid['ncells']
		if sp[1] == 'face_centered_B1':
			field_map[sp[1]]['ndata']=(Nx[0]+1)*Nx[1]*Nx[2]
		elif sp[1] == 'face_centered_B2':
			field_map[sp[1]]['ndata']=Nx[0]*(Nx[1]+1)*Nx[2]
		elif sp[1] == 'face_centered_B3':
			field_map[sp[1]]['ndata']=Nx[0]*Nx[1]*(Nx[2]+1)
				
		if sp[2]=='int': dtype='i'
		elif sp[2]=='float': dtype='f'
		elif sp[2]=='double': dtype='d'
		field_map[sp[1]]['dtype']=dtype
		field_map[sp[1]]['dsize']=field_map[sp[1]]['ndata']*struct.calcsize(dtype)
		file.seek(field_map[sp[1]]['dsize'],1)
		offset = file.tell()
		tmp=file.readline()
		if len(tmp)>1: file.seek(offset)
		else: offset = file.tell()

	#grid['field_map'] = field_map
	#grid['data']={}
	return field_map

def read_field(file_pointer,field_map):
	ndata=field_map['ndata']
	dtype=field_map['dtype']
	file_pointer.seek(field_map['offset'])
	file_pointer.readline() # HEADER
	if field_map['read_table']: file_pointer.readline()
	data = file_pointer.read(field_map['dsize'])
	var = np.asarray(struct.unpack('>'+ndata*dtype,data))

	return var


def read_starvtk(starfile,time_out=False):
	file=open(starfile,'rb')
	star = {}
	star['filename']=starfile
	star['read_field'] = None
	star['read_type'] = None
	while star['read_field'] is None:
		star['data_offset']=file.tell()
		line = file.readline()
		parse_line(line, star)

        time=star['time']
	nstar=star['nstar']
	#print nstar
	fm=set_field_map(star)
	#print fm.keys()
	id=read_field(file,fm['star_particle_id'])
	mass=read_field(file,fm['star_particle_mass'])
	age=read_field(file,fm['star_particle_age'])
	pos=read_field(file,fm['star_particle_position']).reshape(nstar,3)
	vel=read_field(file,fm['star_particle_velocity']).reshape(nstar,3)
	file.close()
	star=[]
	for i in range(nstar):
		star.append({})

	for i in range(nstar):
		star_dict = star[i]
		star_dict['id']=id[i]
		star_dict['mass']=mass[i]
		star_dict['age']=age[i]
		star_dict['v1']=vel[i][0]
		star_dict['v2']=vel[i][1]
		star_dict['v3']=vel[i][2]
		star_dict['x1']=pos[i][0]
		star_dict['x2']=pos[i][1]
		star_dict['x3']=pos[i][2]
		star_dict['time']=time

	return star



from scipy import constants
from scipy.interpolate import interp1d
from scipy.integrate import odeint
from scipy.optimize import newton
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1 import axes_grid
from mpl_toolkits.axes_grid1 import inset_locator
import glob
import os

def mystyle():
	fig=plt.gcf()
	axes=fig.get_axes()
	for ax in axes:
		plt.setp(ax.get_lines(),'linewidth',2)
		plt.setp(ax.xaxis.label,'fontsize',16)
		plt.setp(ax.yaxis.label,'fontsize',16)
		plt.setp(ax.get_xticklabels()+ax.get_yticklabels(),'fontsize',16)

mp=constants.m_p*1.e3
kbol=constants.Boltzmann*1.e7
pc=constants.parsec*1.e2
Gconst=constants.G*1.e3
msun=1.9891e33
Rsun=6.9599e10
Lsun=3.826e33
yr=constants.year

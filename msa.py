#!/usr/bin/python

from init import *
import ath_hst
import glob
import os
from optparse import OptionParser

def RK4(dydt,y0,t0,dt):
	dy1=dt*dydt(y0,t0)
	dy2=dt*dydt(y0+dy1/2.0,t0+dt/2.0)
	dy3=dt*dydt(y0+dy2/2.0,t0+dt/2.0)
	dy4=dt*dydt(y0+dy3,t0+dt)

	return y0+(1.0/6.0)*(dy1+2.0*dy2+2.0*dy3+dy4)


class msa:
	def __init__(self,filename):
		self.setup(filename)
		t, q = self.anal()
		self.set_sol(t,q)

	def set_sol(self,t,q):
		kxt=self.kx+self.qshear*self.ky*np.array(t)
		k2=kxt**2+self.ky**2
		k=np.sqrt(k2)
		self.t=np.array(t)
		self.sigma=np.array(q[:,0])
		self.ux=np.array(q[:,1])
		self.uy=np.array(q[:,2])
		self.bx=self.ky*np.array(q[:,3])
		self.by=-kxt*np.array(q[:,3])
		self.Pi=self.cs2*self.sigma
		self.Phi=-4*np.pi*self.nJ*self.cs2/k2*self.sigma

		kxt=self.kx+self.qshear*self.ky*np.array(self.hst['time'])
		k2=kxt**2+self.ky**2
		k=np.sqrt(k2)
		kLz=2*k*self.hLz
		gfact = np.sqrt(1+0.5*np.exp(-kLz) - 2.0*(1-np.exp(-kLz))/kLz + 0.25*(1-np.exp(-2.0*kLz))/kLz)
		self.hst['dPhi'] = self.hst['dPhi']/gfact

	def setup(self,filename):
		self.filename=filename
		self.hst=ath_hst.read(filename,sortable_key=False)
		self.hst['sigma']=np.sqrt(self.hst['sigma'])
		self.hst['ux']=np.sqrt(self.hst['ux'])
		self.hst['uy']=np.sqrt(self.hst['uy'])
		self.hst['m1']=np.sqrt(self.hst['m1'])
		self.hst['m2']=np.sqrt(self.hst['m2'])
		self.hst['dPhi']=np.sqrt(self.hst['dPhi'])
		self.hst['dP']=np.sqrt(self.hst['dP'])

		self.amp = 1.e-4
		self.nwx = -6
		self.nwy = 1
		self.qshear = 1.0
		self.Q = 2.0
		self.nJ = 2.5
		self.beta = 1
		
		self.kx = self.nwx*2.0*np.pi
		self.ky = self.nwy*2.0*np.pi
		self.cs2 = (4.0-2.0*self.qshear)/(np.pi*self.nJ*self.Q)**2
		self.B2 = self.cs2/self.beta
		self.B0 = np.sqrt(self.B2)

		self.hLz=0.01

	def deriv(self,y,x):
 
		kxt=self.kx+self.qshear*self.ky*x
		k2=kxt**2+self.ky**2
		k=np.sqrt(k2)
		gfact=1-0.5*(np.exp(-k*0.5*self.hLz)+np.exp(-k*1.5*self.hLz))
		#gfact=1.0
		four_pi_G_o_k= 4.0*np.pi*self.nJ/k2*gfact

		dqdt=[]
		dqdt.append(-kxt*y[1]-self.ky*y[2])
		dqdt.append(2.0*y[2]+kxt*self.cs2*(1.0-four_pi_G_o_k)*y[0]-self.B2*k2*y[3])
		dqdt.append(-(2.0-self.qshear)*y[1]+self.ky*self.cs2*(1-four_pi_G_o_k)*y[0])
		dqdt.append(y[1])

  		return np.array(dqdt)

	def anal(self):
		dt=1.e-3
		q0=self.amp*np.array([1.,self.kx/self.ky,1.,0.5/self.B0/np.pi])
		t=np.arange(dt,15.0,dt)
		q=odeint(self.deriv,q0,t)

		return t, q
	def draw_fig(self,fig):
		fig.clf()

		ax = fig.add_subplot(321)
		line,=ax.plot(self.hst['time'],self.hst['sigma'],marker='o')
		ax.plot(self.t,abs(self.sigma))
		ax.set_ylim(line.get_ydata().min(),line.get_ydata().max()*1.5)
		ax.set_ylabel(r'$|\delta\rho|$')

		ax = fig.add_subplot(322)
		line,=ax.plot(self.hst['time'],self.hst['dPhi'],marker='o')
		ax.plot(self.t,abs(self.Phi))
		ax.set_ylim(line.get_ydata().min(),line.get_ydata().max()*1.5)
		ax.set_ylabel(r'$|\delta\Phi|$')

		ax = fig.add_subplot(323)
		line,=ax.plot(self.hst['time'],self.hst['ux'],marker='o')
		ax.plot(self.t,abs(self.ux))
		ax.set_ylim(line.get_ydata().min(),line.get_ydata().max()*1.5)
		ax.set_ylabel(r'$|u_x|$')

		ax = fig.add_subplot(324)
		line,=ax.plot(self.hst['time'],self.hst['uy'],marker='o')
		ax.plot(self.t,abs(self.uy))
		ax.set_ylim(line.get_ydata().min(),line.get_ydata().max()*1.5)
		ax.set_ylabel(r'$|u_y|$')

		ax = fig.add_subplot(325)
		line,=ax.plot(self.hst['time'],self.hst['m1'],marker='o')
		ax.plot(self.t,abs(self.bx))
		ax.set_ylim(line.get_ydata().min(),line.get_ydata().max()*1.5)
		ax.set_ylabel(r'$|b_x|$')

		ax = fig.add_subplot(326)
		line,=ax.plot(self.hst['time'],self.hst['m2'],marker='o')
		ax.plot(self.t,abs(self.by))
		ax.set_ylim(line.get_ydata().min(),line.get_ydata().max()*1.5)
		ax.set_ylabel(r'$|b_y|$')
		
		axes=fig.get_axes()
		plt.setp(axes,'yscale','log')
		plt.setp(axes,'xlabel','t')
		plt.setp(axes,'xlim',(0,15))

		plt.draw()
		plt.tight_layout()

#parser = OptionParser()
#parser.add_option('-b','--base-directory', dest='base_directory',
#                  help='base directory for data',
#                  default='/scr1/cgkim/Research/athena_StarParticle/bin/')
#parser.add_option('-d','--directory',dest='directory',
#                  help='working directory')
#parser.add_option('-i','--id',dest='id',
#                  help='id of dataset')
#parser.add_option('-o','--output',dest='out_fname',
#                  help='output file name')
#(options, args) = parser.parse_args()
#

#fsearch = options.base_directory 
#fsearch = fsearch+options.directory
#fsearch = fsearch+options.id
#
#if options.out_fname == None: out_fname='png/msa.png'
#else: out_fname=options.out_fname
#
#

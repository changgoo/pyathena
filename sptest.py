from init import *

def RK4(dydt,y0,t0,dt):
	dy1=dt*dydt(y0,t0)
	dy2=dt*dydt(y0+dy1/2.0,t0+dt/2.0)
	dy3=dt*dydt(y0+dy2/2.0,t0+dt/2.0)
	dy4=dt*dydt(y0+dy3,t0+dt)

	return y0+(1.0/6.0)*(dy1+2.0*dy2+2.0*dy3+dy4)

def deriv(y,x):
        r2 = y[0]*y[0]+y[1]*y[1]
        r = np.sqrt(r2)
	dvxdt = 2.0*Omega*y[3]+2.0*Omega*Omega*qshear*y[0]-GM/r2*y[0]/r
	dvydt = -2*Omega*y[2]-GM/r2*y[1]/r
	dxdt = y[2]
	dydt = y[3]

	return np.array([dxdt,dydt,dvxdt,dvydt])

Omega=1.0
qshear=1.0
GM = Gconst*1.4*mp*pc*pc/1.e10*2.e4
vc=np.sqrt(GM/3.0)
y0=np.array([0.,1+3.0,1.0,0.0])
dt=1.e-4
t=np.arange(dt,20,dt)
y=odeint(deriv,y0,t)

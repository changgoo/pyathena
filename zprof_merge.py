import pyathena as pa
import pandas as pd
import glob
import os
import argparse
import string
from ath_hst import test_pickle

def main(**kwargs):
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  if kwargs['serial']: 
    base=dir+id
  else:
    base=dir+"zprof/"+id
  print base
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
    file=[]
    for i in range(start,end,fskip):
      file.append(base+".%4.4d.whole.zprof.p" % i)
  else:
    file = glob.glob(base+".????.whole.zprof.p")
    file.sort()

  print len(file)
  if len(file) > 0:
    pfile = glob.glob(file[0][:-13]+"*.zprof.p")
    pfile.sort()
    plist = []
    for f in pfile:
      phase=f.split('.')[-3]
      plist.append(phase)
    print plist

  if not os.path.isdir(dir+'zprof_merged/'): os.mkdir(dir+'zprof_merged/')

  time=[]
  if kwargs['write']:
    zp_panels={}
    for p in plist:
      zp_panels[p]={}

  for f in file:
    vtkfname=f.replace('whole.zprof.p','vtk').replace('zprof/','id0/')
    ds=pa.AthenaDataSet(vtkfname,serial=True)
    print vtkfname,ds.domain['time']
    time.append(ds.domain['time'])

    if kwargs['write']:
      for p in plist:
        zp_panels[p][f]=pd.read_pickle(f.replace('whole',p))

  if kwargs['write']:
    for p in plist:
      pn=pd.Panel.from_dict(zp_panels[p],orient='minor')
      pn.minor_axis=time
      fname=dir+'zprof_merged/'+string.join([id,p,'zprof'],'.')
      pn.to_pickle(fname+'.p')
      print 'Output: ',fname+'.p'

#  for f in file:
#    print 'Cleaning... ',f
#    zp=pa.AthenaZprof(f,stitch=False,clean=True)
  

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-w','--write',action='store_true',help='write merged pickle')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))

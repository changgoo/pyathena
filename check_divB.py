import pyathena
import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.pyplot as plt

def compute_divB(file):
#  base = '/scr1/cgkim/Research/athena_sfr3d/bin/test/id0/'
  base = '/scratch/gpfs/changgoo/QA10_b1_Turb_SN_2pc_p4/'
  f = base+file
  ds=pyathena.AthenaDataSet(f)

  fall=ds.flist

  print ds.field_list
  B=ds.read_all_data('magnetic_field')
    
  B1=B[:,:,:,0]
  B2=B[:,:,:,1]
  B3=B[:,:,:,2]
  #B1=ds.read_all_data('face_centered_B1')
  #B2=ds.read_all_data('face_centered_B2')
  #B3=ds.read_all_data('face_centered_B3')
  Nx=ds.domain['Nx']-1
  divB=np.zeros((Nx[2],Nx[1],Nx[0]),dtype='d')

  for k in range(Nx[2]):
    for j in range(Nx[1]):
        for i in range(Nx[0]):
            divB[k,j,i]=B1[k,j,i+1]-B1[k,j,i]
            divB[k,j,i]=divB[k,j,i]+B2[k,j+1,i]-B2[k,j,i]
            divB[k,j,i]=divB[k,j,i]+B3[k+1,j,i]-B3[k,j,i]

  print f,divB.max()
  return ds,divB

def axis_setup(fig,nrow,ncol):
  grid = ImageGrid(fig, 111, nrows_ncols = (nrow, ncol),direction="row")
  return grid

def plot(divB,ax,zind):
  im=ax.imshow(np.log10(divB[zind,:,:]),vmax=-5,origin="lower",interpolation='nearest')
  return im

def test1(fname):
  ds,divB=compute_divB(fname)
  den=ds.read_all_data('density')
  fig=plt.gcf()
  fig.clf()
  nrow, ncol = 4,3
  grid=axis_setup(fig,nrow,ncol)
  ind=[0,63,127]
  im=[]
  for ax, k in zip(grid[0:ncol],ind): 
    im.append(ax.imshow(divB[k,:,:],vmax=1.e-10,vmin=-1.e-10,origin="lower",interpolation='nearest'))
    ax.axhline(31,ls=':')
    ax.axvline(31,ls=':')

  for ax, k in zip(grid[ncol:ncol*2],ind): 
    im.append(ax.imshow(np.log10(den[k,:,:]),vmax=1,vmin=-1,origin="lower",interpolation='nearest'))

  for ax, j in zip(grid[ncol*2:ncol*3],ind): 
    im.append(ax.imshow(divB[:,j,:],vmax=1.e-10,vmin=-1.e-10,origin="lower",interpolation='nearest'))
    ax.axhline(31,ls=':')
    ax.axvline(31,ls=':')

  for ax, j in zip(grid[ncol*3:ncol*4],ind): 
    im.append(ax.imshow(np.log10(den[:,j,:]),vmax=1,vmin=-1,origin="lower",interpolation='nearest'))
  plt.draw() 


#!python

import rprof
import matplotlib.pyplot as plt

rprof.dir='../../bin/'
rprof.i=0

rprof.id='PS_old_high'
reload(rprof)
fPSold=rprof.fr3d

rprof.id='PS_new_high'
reload(rprof)
fPSnew=rprof.fr3d

rprof.id='PS_k2_high'
reload(rprof)
fPSk2=rprof.fr3d

rPS=rprof.r3d

fig=plt.figure()
ax1=fig.add_subplot(2,1,1)
ax2=fig.add_subplot(2,1,2)

ax1.plot(rPS,fPSk2,'m.',label="1/$k^2$")
ax1.plot(rPS,fPSold,'b.',label="FDM")
ax1.plot(rPS,fPSnew,'r.',label="Green")
ax1.plot(rprof.r1d,rprof.fP,'g-',label="analytic")
handles, labels = ax1.get_legend_handles_labels()
ax1.legend(handles,labels)
ax1.set_ylabel('$f_r$')

ax2.plot(rPS,fPSk2-rprof.fP3d,'m.')
ax2.plot(rPS,fPSold-rprof.fP3d,'b.')
ax2.plot(rPS,fPSnew-rprof.fP3d,'r.')
ax2.set_xlabel('r/a')
ax2.set_ylabel('$f_r-f_{r,P}$')

fig.set_size_inches(8,10)
fig.savefig('plummer.png')

rprof.id='BE_old'
reload(rprof)
fPSold=rprof.fr3d

rprof.id='BE_new'
reload(rprof)
fPSnew=rprof.fr3d

rPS=rprof.r3d

fig=plt.figure()
ax1=fig.add_subplot(2,1,1)
ax2=fig.add_subplot(2,1,2)

ax1.plot(rPS,fPSold,'b.',label="FDM")
ax1.plot(rPS,fPSnew,'r.',label="Green")
handles, labels = ax1.get_legend_handles_labels()
ax1.legend(handles,labels)
ax1.set_ylabel('$f_r$')

ax2.plot(rPS,fPSold-fPSnew,'b.')
ax2.set_xlabel('r/a')
ax2.set_ylabel('$f_r$(FDM)-$f_r$(Green)')

fig.set_size_inches(8,10)
fig.savefig('BE.png')

from numpy import *

rprof.id='r2_old'
reload(rprof)
fPSold=rprof.fr3d

rprof.id='r2_new'
reload(rprof)
fPSnew=rprof.fr3d

rPS=rprof.r3d


fanal=4.0*pi/rprof.r3d
ind=where(rprof.r3d > rprof.r0)
fanal[ind]=4.0*pi*rprof.r0/rprof.r3d[ind]**2

fig=plt.figure()
ax1=fig.add_subplot(2,1,1)
ax2=fig.add_subplot(2,1,2)

ax1.plot(rPS,fPSold,'b.',label="FDM")
ax1.plot(rPS,fPSnew,'r.',label="Green")
ax1.plot(rprof.r1d,4.0*pi/rprof.r1d,'g-',label="1/r")
ax1.plot(rprof.r1d,4.0*pi*rprof.r0/rprof.r1d**2,'m-',label="1/$r^2$")
ax1.set_ylim(1,1.e3)
ax1.set_ylabel('$f_r$')
ax1.set_xscale('log',nonposx='clip')
ax1.set_yscale('log',nonposy='clip')

ax2.plot(rPS,abs((fPSold-fanal)/fanal),'b.')
ax2.plot(rPS,abs((fPSnew-fanal)/fanal),'r.')
ax2.set_xlabel('r/a')
ax2.set_ylabel('|$f_r$(numerical)/$f_r$(analytic)-1|')
ax2.set_xscale('log',nonposx='clip')
ax2.set_yscale('log',nonposy='clip')

rprof.id='r2_oldh'
reload(rprof)
fPSold=rprof.fr3d

rprof.id='r2_newh'
reload(rprof)
fPSnew=rprof.fr3d

rPS=rprof.r3d

fanal=4.0*pi/rprof.r3d
ind=where(rprof.r3d > rprof.r0)
fanal[ind]=4.0*pi*rprof.r0/rprof.r3d[ind]**2

ax1.plot(rPS,fPSold,'b+',label="FDM high res.")
ax1.plot(rPS,fPSnew,'r+',label="Green high res.")
handles, labels = ax1.get_legend_handles_labels()
ax1.legend(handles,labels,loc=3)

ax2.plot(rPS,abs((fPSold-fanal)/fanal),'b+')
ax2.plot(rPS,abs((fPSnew-fanal)/fanal),'r+')


fig.set_size_inches(8,10)
fig.savefig('r2.png')

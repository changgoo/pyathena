from startup import *
import pyathena as pa

bbox_props = dict(boxstyle="round,pad=0.5", fc="black", ec="b", lw=2,alpha=0.5)
texteffect=pa.texteffect()
def rotate_xaxis(angle=45):
    fig=plt.gcf()
    plt.setp([ax.get_xticklabels() for ax in fig.axes], rotation=angle)

class figure_2d(object):
    def __init__(self,tzdata):
        self.id=tzdata.id
        self.setup_kwargs(tzdata)
        self.setup_figure()
    
    def setup_kwargs(self,tzdata):
        extent=[tzdata.t[0],tzdata.t[-1],tzdata.z[0],tzdata.z[-1]]
        Lz=extent[3]-extent[2]
        Lt=extent[1]-extent[0]
        self.kwargs={'norm':LogNorm(),'origin':'lower','aspect':Lt/Lz,'extent':extent}

    def setup_figure(self):
        self.fig=plt.figure(figsize=(15,10))
        self.grid=AxesGrid(plt.gcf(),111,nrows_ncols=(2,3),cbar_location='right',cbar_mode='single')

        self.bbox_props = dict(boxstyle="round,pad=0.5", fc="black", ec="b", lw=2,alpha=0.5)
        self.texteffect=pa.texteffect()
        
    def draw_figure(self,tzdata,field,unit=1):
        data=tzdata.get_field(field,unit)

        for p,ax in zip(tzdata.plist,self.grid):
            im=ax.imshow(data[p],**self.kwargs)
            ax.text(0.1,0.9,p,ha='left',va='top',transform=ax.transAxes,bbox=self.bbox_props,**self.texteffect)
            plt.colorbar(im,cax=ax.cax)
            
    def save_figure(self,id,fieldname):
        self.fig.savefig('figure/'+id+'_'+fieldname+'.png',bbox_inches='tight')
        
class figure_1d(object):
    def __init__(self):
        self.setup_figure()
        
    def setup_figure(self):
        self.fig=plt.figure(figsize=(15,10))
    
    def draw_figure(self,x,data,field,unit=1,op=None):
        plist=data.keys()
        plist.sort()
        for i,p in enumerate(plist):
            prof = data[p][field]
            if op: prof=op(prof)
            plt.subplot(2,3,i+1)
            plt.plot(x,prof*unit,label=field)
            
            plt.title(p)
        plt.legend(loc=0)
    
    def draw_velocity_m(self,data):
        z=data.z
        for i,p in enumerate(data.plist):
            prof = data.zprof_m[p]
            plt.subplot(2,3,i+1)
            cs=np.sqrt(prof['P'])
            vz=np.sqrt(prof['Ek3']*2.0)
            plt.plot(z,cs,label=r'$c_s$')
            plt.plot(z,vz,label=r'$v_z$')
            if 'PB1' in prof:
                vA=np.sqrt(2.0*(prof['PB1']+prof['PB2']+prof['PB3']))
                plt.plot(z,vA,label=r'$v_A$')
            if 'dPB1' in prof:
                dvA=np.sqrt(2.0*(prof['dPB1']+prof['dPB2']+prof['dPB3']))
                plt.plot(z,dvA,label=r'$\delta v_{A}$')
            plt.title(p)
        plt.legend(loc=0)
        
    def draw_velocity_v(self,data):
        z=data.z
        for i,p in enumerate(data.plist):
            prof = data.zprof_v[p]
            plt.subplot(2,3,i+1)
            cs=np.sqrt(data.zprof_m[p]['P'])
            vz=prof['pvz']-prof['mvz']
            plt.plot(z,cs,label=r'$c_s$')
            plt.plot(z,vz,label=r'$v_z$')
            plt.title(p)
        plt.legend(loc=0)

    def draw_pressure(self,data):
        Punit=(data.units['pressure']/c.k_B).cgs.value
        z=data.z
        for i,p in enumerate(data.plist):
            prof=data.zprof_v[p]
            plt.subplot(2,3,i+1)
            P=prof['P']*Punit
            Pt=prof['Ek3']*2.0*Punit
    
            plt.plot(z,P,label=r'$P_{\rm th}$')
            plt.plot(z,Pt,label=r'$P_{\rm turb}$')
    
            if 'PB1' in prof:
                PB=(prof['PB1']+prof['PB2']-prof['PB3'])*Punit
                plt.plot(z,PB,label=r'$\Pi_{\rm B}$')
        
            if 'dPB1' in prof:
                dPB=(prof['dPB1']+prof['dPB2']-prof['dPB3'])*Punit
                plt.plot(z,dPB,label=r'$\Pi_{\rm B,turb}$')
            plt.title(p)
        plt.legend(loc=0)

def comp_eta(data,ids,hst=False,t1=200,t2=300,box=True,zcut=50,eta=True,p='2p',sfrbin='sfr40'):
  unit=data[ids[0]].units
  plist=data[ids[0]].plist
  Punit=(unit['pressure']).cgs
  sfrunit=(c.M_sun/c.pc**2/u.Myr).cgs
  Myr=unit['time'].to('Myr').value

  Ptot={}
  for i,field in enumerate(['P','Pturb','tPiB','Ptot']):
    plt.subplot(2,2,i+1)
    flist=[]
    for id in ids:
      h=data[id].hst
      z=data[id].z
      Nz=len(z)
      t=data[id].t*Myr
      tidx=np.where((t>t1)*(t<t2))
      zidx=np.where(np.abs(z) < zcut)[0]
        
      area=np.array(data[id].pn4d['whole']['A'])
      sfr=np.interp(data[id].t,h['time'],h[sfrbin])*sfrunit

      pn4d=data[id].pn4d
      pres={}
      pres['whole']=pn4d['whole'][field]
      pres['2p']=pn4d['phase1'][field]+pn4d['phase2'][field]+pn4d['phase3'][field]
      pres['hot']=pn4d['phase4'][field]+pn4d['phase5'][field]

      fdata=np.array(pres[p]/area)[zidx,:]
      tp=fdata.mean(axis=0)*Punit.cgs/c.k_B.cgs
      if eta: tp=(tp*c.k_B.cgs/sfr).to('km/s')
      l,=plt.plot(t,tp,label=id)

    plt.title(field)

  plt.legend(loc=0)

def comp_outflow(data,ids,plist=[],mass_loading=True,hst=False,t1=200,t2=300,box=True):
  unit=data[ids[0]].units
  if len(plist) == 0:
    plist=data[ids[0]].plist
  funit=(unit['mass']/unit['time']).to('Msun/Myr').value
  Myr=unit['time'].to('Myr').value

  for i,p in enumerate(plist):
    plt.subplot(2,3,i+1)
    flist=[]
    for id in ids:
      h=data[id].hst
      z=data[id].z/1.e3
      Nz=len(z)
      t=data[id].t*Myr
      tidx=np.where((t>t1)*(t<t2))
        
      sfr40=np.interp(t,h['time'],h['sfr40'])
      pFlux=np.array(data[id].pn4d[p]['pFzd'])*funit
      mFlux=np.array(data[id].pn4d[p]['mFzd'])*funit
      pnw=np.array(data[id].pn4d['whole']['A'])
        
      tp=(pFlux[Nz-1,:]-mFlux[0,:])/pnw[Nz-1,:]
      if mass_loading: tp=tp/sfr40
      if not box: l,=plt.plot(t,tp,label=id)
      flist.append(tp[tidx])

      tp_mid=(pFlux[Nz*3/4,:]-mFlux[Nz/4,:])/pnw[Nz*3/4,:]
      if mass_loading: tp_mid=tp_mid/sfr40
      #plt.plot(t,tp_mid,color=l.get_color(),ls=':')
        
      if hst:
        if p=='phase1' : flux=h['F3c']*2.0*funit
        if p=='phase2' : flux=h['F3u']*2.0*funit
        if p=='phase3' : flux=h['F3w']*2.0*funit
        if p=='phase4' : flux=h['F3h1']*2.0*funit
        if p=='phase5' : flux=h['F3h2']*2.0*funit
        if p=='whole': flux=(h['F3c']+h['F3u']+h['F3w']+h['F3h1']+h['F3h2'])*2.0*funit
        
        if mass_loading: flux=flux/h['sfr40'] 
        if not box: plt.plot(h['time']*Myr,flux,color=l.get_color(),lw=1,ls=':')
    if box: plt.boxplot(flist,labels=ids,showmeans=True)
    plt.title(p)

  if box:
    rotate_xaxis(angle=30)
  else:
    plt.legend(loc=0)

def comp_hstfield(data,ids,weight='volume',field='frac_H',t1=300,t2=500):
  unit=data[ids[0]].units
  Myr=unit['time'].to('Myr').value

  flist=[]
  plt.subplot(1,2,1)
  for j,id in enumerate(ids):
      t=data[id].hst['time']*Myr
      if weight == 'mass':
        f=np.array(data[id].hst[field]/data[id].hst['mass'])
      elif weight == 'volume':
        f=np.array(data[id].hst[field])
      tidx=np.where((t>t1)*(t<t2))
      #flist.append(np.log10(f[tidx]))
      flist.append(f[tidx])
      plt.plot(t,f,label=id)
  plt.axvline(t1,ls=':')
  plt.axvline(t2,ls=':')
  plt.legend(loc=0)

  plt.subplot(1,2,2)
  plt.boxplot(flist,labels=ids)

  rotate_xaxis(angle=30)
  ax=plt.gca()
  texteffect['fontsize']=16
  bbox_props['alpha']=0.2
  ax.text(0.1,0.95,'t=%d~%d' % (t1,t2),ha='left',va='top',transform=ax.transAxes,
         bbox=bbox_props,**texteffect)



def comp_tfield(data,ids,plist=[],tprof='mass',field='frac_H',t1=300,t2=500,box=True,factor=1):
  unit=data[ids[0]].units
  if len(plist) == 0:
    plist=data[ids[0]].plist
    if box: plist=plist[:-1]
  Myr=unit['time'].to('Myr').value

  for i,p in enumerate(plist):
    plt.subplot(2,3,i+1)
    flist=[]
    for j,id in enumerate(ids):
      t=data[id].t*Myr
      if tprof == 'mass':
        f=data[id].tprof_m[p][field]
      elif tprof == 'volume':
        f=data[id].tprof_v[p][field]
      f=f*factor
      tidx=np.where((t>t1)*(t<t2))
      #flist.append(np.log10(f[tidx]))
      flist.append(f[tidx])
      if not box: plt.plot(t,f,label=id)
    if box: plt.boxplot(flist,labels=ids)
    plt.title(p)

  fig=plt.gcf()
  if box: 
    rotate_xaxis(angle=30)
    ax=plt.gca()
    ax.text(1.1,0.9,'t=%d~%d' % (t1,t2),ha='left',va='top',fontsize='large',transform=ax.transAxes)
  plt.setp([ax.get_xticklabels() for ax in fig.axes[:3]],visible=False)

  if not box: 
    plt.legend(bbox_to_anchor=(1.01, 1),loc=2)

def comp_zfield(data,ids,plist=[],zprof='mass',field='d',t1=300,t2=500,box=True,unit=1.0):
  u=data[ids[0]].units
  if len(plist)==0:
    plist=data[ids[0]].plist
  Myr=u['time'].to('Myr').value
  nid=len(ids)

  for i,p in enumerate(plist):
    if nid > 1: plt.subplot(2,3,i+1)
    for id in ids:
      t=data[id].t*Myr
      tidx=np.where((t>t1)*(t<t2))[0]
      z=data[id].z/1.e3
      if zprof == 'mass':
        pn4d=np.array(data[id].pn4d[p][field])[:,tidx[0]:tidx[-1]]
        pnw=np.array(data[id].pn4d[p]['d'])[:,tidx[0]:tidx[-1]]
        zp=pn4d.sum(axis=1)/pnw.sum(axis=1)
      elif zprof == 'volume':
        pn4d=np.array(data[id].pn4d[p][field])[:,tidx[0]:tidx[-1]]
        pnw=np.array(data[id].pn4d[p]['A'])[:,tidx[0]:tidx[-1]]
        zp=pn4d.sum(axis=1)/pnw.sum(axis=1)
      elif zprof == 'mass_fraction':
        pn4d=np.array(data[id].pn4d[p][field])[:,tidx[0]:tidx[-1]]
        pnw=np.array(data[id].pn4d['whole']['d'])[:,tidx[0]:tidx[-1]]
        zp=pn4d.sum(axis=1)/pnw.sum(axis=1)
      elif zprof == 'volume_fraction':
        pn4d=np.array(data[id].pn4d[p][field])[:,tidx[0]:tidx[-1]]
        pnw=np.array(data[id].pn4d['whole']['A'])[:,tidx[0]:tidx[-1]]
        zp=pn4d.sum(axis=1)/pnw.sum(axis=1)
      if nid>1: plt.plot(z,zp*unit,label=id)
      else: plt.plot(z,zp*unit,label=p)
    plt.title(p)
  plt.legend(bbox_to_anchor=(1.01, 1),loc=2)

def comp_pressure(data,id,plist=[],t1=300,t2=500,weight=True):
  u=data[id].units
  if len(plist)==0: plist=data[id].plist
  Myr=u['time'].to('Myr').value
  Punit=(u['pressure']/c.k_B).cgs.value
  
  Ptot={}
  Wtot={}
  for i,p in enumerate(plist):
    plt.subplot(2,3,i+1)

    t=data[id].t*Myr
    tidx=np.where((t>t1)*(t<t2))[0]
    z=data[id].z/1.e3
    dz=data[id].z[1]-data[id].z[0]
    Nz=len(z)
    Ptot[p]=np.zeros(Nz)
    Wtot[p]=np.zeros(Nz)
    pn4d=data[id].pn4d[p]
    area=np.array(data[id].pn4d['whole']['A'])[:,tidx[0]:tidx[-1]]
    #area=np.array(data[id].pn4d[p]['A'])[:,tidx[0]:tidx[-1]]
    for field in ['P','Pturb','PiB']:
      if field in pn4d:
        fdata=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
        zp=fdata.sum(axis=1)/area.sum(axis=1)*Punit
        Ptot[p] = Ptot[p]+zp
        plt.plot(z,zp-zp[0],label=field)

    if weight:
#      for field in ['gsg','gext']:
#        g=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
#        g=g.sum(axis=1)/area.sum(axis=1)
#        d=np.array(pn4d['d'])[:,tidx[0]:tidx[-1]]
#        d=d.sum(axis=1)/area.sum(axis=1)
#        dW=d*g*dz
      for field in ['dWsg','dWext']:
        dW=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
        dW=dW.sum(axis=1)/area.sum(axis=1)*dz
        Wp=dW[::-1].cumsum()[::-1]
        Wp[:Nz/2]=-dW.cumsum()[:Nz/2]
        Wp=Wp*Punit
        
        Wtot[p]=Wtot[p]+Wp
        plt.plot(z,Wp,label=field)
    plt.title(p)
  plt.legend(bbox_to_anchor=(1.01, 1),loc=2)

  return z,Ptot,Wtot

def comp_dPdz(data,id,t1=300,t2=500,weight=True,smooth=5,noplot=False):
  u=data[id].units
  plist=data[id].plist
  Myr=u['time'].to('Myr').value
  Punit=(u['pressure']/c.k_B).cgs.value
  
  dPtot={}
  dWtot={}
  for i,p in enumerate(plist):
    if not noplot: plt.subplot(2,3,i+1)

    t=data[id].t*Myr
    tidx=np.where((t>t1)*(t<t2))[0]
    z=data[id].z
    zkpc=z/1.e3
    Nz=len(z)
    dPtot[p]=np.zeros(Nz)
    dWtot[p]=np.zeros(Nz)
    pn4d=data[id].pn4d[p]
    area=np.array(data[id].pn4d['whole']['A'])[:,tidx[0]:tidx[-1]]
    #area=np.array(data[id].pn4d[p]['A'])[:,tidx[0]:tidx[-1]]
    for field in ['P','Pturb','PiB']:
      if field in pn4d:
        fdata=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
        zp=fdata.sum(axis=1)/area.sum(axis=1)
        dpdz=deriv(z,zp,smooth=smooth)
        dPtot[p] = dPtot[p]+dpdz
        if not noplot: plt.plot(zkpc,dpdz,label=field)

    if weight:
#      for field in ['gsg','gext']:
#        g=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
#        g=g.sum(axis=1)/area.sum(axis=1)
#        d=np.array(pn4d['d'])[:,tidx[0]:tidx[-1]]
#        d=d.sum(axis=1)/area.sum(axis=1)
#        dW=d*g*dz
      for field in ['dWsg','dWext']:
        dW=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
        dW=-dW.sum(axis=1)/area.sum(axis=1)
        dWtot[p]=dWtot[p]+dW
        if not noplot: plt.plot(zkpc,dW,label=field)
    if not noplot: plt.title(p)
  if not noplot: plt.legend(bbox_to_anchor=(1.01, 1),loc=2)

  return zkpc,dPtot,dWtot

def force_balance(data,id,t1=300,t2=500,smooth=5):
  plist=data[id].plist
  z,dPtot,dWtot=comp_dPdz(data,id,t1=t1,t2=t2,smooth=smooth,weight=True,noplot=True)
  Ptot=np.zeros(len(z))
  Wtot=np.zeros(len(z))
  lp=[]
  lw=[]
  for p in plist:
    dPtot[p]=np.nan_to_num(dPtot[p])
    dWtot[p]=np.nan_to_num(dWtot[p])
    l,=plt.plot(z,dPtot[p],label=p)
    lp.append(l)
    l,=plt.plot(z,dWtot[p],color=l.get_color(),ls='--')
    lw.append(l)
    Ptot+=dPtot[p]
    Wtot+=dWtot[p]

  l1,=plt.plot(z,Ptot,color='k',label=r'$dP/dz$')
  l2,=plt.plot(z,Wtot,color='k',ls='--',label=r'$-\rho d\Phi/dz$')
  legend1=plt.legend(handles=lp,fontsize='large',loc=1)
  plt.gca().add_artist(legend1)
  legend2=plt.legend(handles=[l1,l2],fontsize='large',loc=2)
  plt.setp(plt.gcf().axes,'xlim',(-2,2))

def deriv(z,f,smooth=5):
  from derivative import deriv_central
  dfdz=deriv_central(np.array(f),z)
  dfdz=np.insert(dfdz,0,dfdz[0])
  dfdz=np.insert(dfdz,-1,dfdz[-1])

  if smooth:
    from astropy.convolution import convolve, convolve_fft
    from astropy.convolution import Gaussian1DKernel,Gaussian2DKernel
    gauss=Gaussian1DKernel(smooth)
    x=np.linspace(0,gauss.shape[0]-1,gauss.shape[0])
    return convolve(dfdz,gauss)
  else:
    return dfdz

def comp_Eflux(data,id,t1=300,t2=500,smooth=5,noplot=False,nocool=False,Efields=['FzKE','FzTE','FzGE','Poyn'],Hfields=['Stress','cool','heat']):
  u=data[id].units
  plist=data[id].plist
  Myr=u['time'].to('Myr').value
  Efunit=(u['density']*u['velocity']**3*u['length']).to('erg/s/pc')
  
  dFtot={}
  Htot={}
  for i,p in enumerate(plist):
    if not noplot: plt.subplot(2,3,i+1)

    t=data[id].t*Myr
    tidx=np.where((t>t1)*(t<t2))[0]
    z=data[id].z
    dz=z[1]-z[0]
    zkpc=z/1.e3
    Nz=len(z)
    dFtot[p]=np.zeros(Nz)
    Htot[p]=np.zeros(Nz)
    pn4d=data[id].pn4d[p]
    area=np.array(data[id].pn4d['whole']['A'])[:,tidx[0]:tidx[-1]]
    #area=np.array(data[id].pn4d[p]['A'])[:,tidx[0]:tidx[-1]]
    if not ('FzKE' in pn4d):
      pKEflux=pn4d['pFzE1']+pn4d['pFzE2']+pn4d['pFzE3']
      mKEflux=pn4d['mFzE1']+pn4d['mFzE2']+pn4d['mFzE3']
      netKEflux=pKEflux+mKEflux
      pn4d['FzKE']=netKEflux
      pn4d['pFzKE']=pKEflux
      pn4d['mFzKE']=mKEflux
    
    if not ('FzTE' in pn4d):
      netTEflux = pn4d['pFzP']+pn4d['mFzP']
      pn4d['FzTE'] = netTEflux

    if not ('FzGE' in pn4d):
      pGEflux = pn4d['pFzEge']+pn4d['pFzEgsg']+pn4d['pFzEtidal'];
      mGEflux = pn4d['mFzEge']+pn4d['mFzEgsg']+pn4d['mFzEtidal'];
      netGEflux = pGEflux+mGEflux
      pn4d['FzGE']=netGEflux
      pn4d['pFzGE']=pGEflux
      pn4d['mFzGE']=mGEflux

    if not ('Poyn' in pn4d):
      pSflux = pn4d['pSzEm1']+pn4d['pSzEm2']+pn4d['pSzvB1']+pn4d['pSzvB2']
      mSflux = pn4d['mSzEm1']+pn4d['mSzEm2']+pn4d['mSzvB1']+pn4d['mSzvB2']
      netSflux = pSflux+mSflux
      pn4d['Poyn']=netSflux
      pn4d['pPoyn']=pSflux
      pn4d['mPoyn']=mSflux

    if not ('Rxy' in pn4d):
      pn4d['Rxy'] = 0.5*(pn4d['RxyL']+pn4d['RxyR'])

    if not ('Mxy' in pn4d):
      pn4d['Mxy'] = 0.5*(pn4d['MxyL']+pn4d['MxyR'])

    if not ('Stress' in pn4d):
      pn4d['Stress'] = pn4d['Rxy']+pn4d['Mxy']

    for field in Efields:
      fdata=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
      #zp=fdata.sum(axis=1)/area.sum(axis=1)
      zp=fdata.sum(axis=1)
      dfdz=deriv(z,zp,smooth=smooth)
      dFtot[p] = dFtot[p]+dfdz
      if not noplot: plt.plot(zkpc,dfdz*Efunit,label=field)

    for field in Hfields:
      heat=np.array(pn4d[field])[:,tidx[0]:tidx[-1]]
      #heat=heat.sum(axis=1)/area.sum(axis=1)
      heat=heat.sum(axis=1)
      #if field is 'cool': heat *= -1
      Htot[p]=Htot[p]+heat
      if not noplot and not nocool: plt.plot(zkpc,heat*Efunit,label=field)
    if not noplot: plt.title(p)
  if not noplot: plt.legend(bbox_to_anchor=(1.01, 1),loc=2)

  return zkpc,dFtot,Htot

def energy_balance(data,id,t1=300,t2=500,smooth=5):
  plist=data[id].plist
  z,dFtot,Htot=comp_Eflux(data,id,t1=t1,t2=t2,smooth=smooth,noplot=True)
  for p in plist:
    dFtot[p]=np.nan_to_num(dFtot[p])
    Htot[p]=np.nan_to_num(Htot[p])
  F2p=dFtot['phase1']+dFtot['phase2']+dFtot['phase3']
  H2p=Htot['phase1']+Htot['phase2']+Htot['phase3']
  lp,=plt.plot(z,F2p,ls=':')
  lw,=plt.plot(z,H2p,ls=':')

  Fh=dFtot['phase4']+dFtot['phase5']
  Hh=Htot['phase4']+Htot['phase5']
  plt.plot(z,Fh,color=lp.get_color(),ls='--')
  plt.plot(z,Hh,color=lw.get_color(),ls='--')

  F=dFtot['whole']
  H=Htot['whole']
  plt.plot(z,F,color=lp.get_color(),label=r'$d\mathcal{B}/dz$')
  plt.plot(z,H,color=lw.get_color(),label=r'$\mathcal{H}$')
  plt.legend(loc=0,fontsize='large')
  plt.setp(plt.gcf().axes,'xlim',(-2,2))

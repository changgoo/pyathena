import pyathena as pa

#astropy
import astropy.constants as c
import astropy.units as u

#matplotlib
import matplotlib
matplotlib.use('agg')
from matplotlib.colors import LogNorm
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import argparse
import cPickle as pickle

#numpy
import numpy as np

#pandas
import pandas as pd

plt.rc('figure',figsize=(15,12))
plt.rc('font',family='serif')
plt.rc('lines',lw=2)
plt.rc('font',size=14)
plt.rc('xtick',labelsize=14)
plt.rc('ytick',labelsize=14)

aux={}
aux['number_density']={'title':r'$n_{\rm H} [{\rm cm}^{-3}]$','cmap':'RdYlBu_r','cmin':1.e-5,'cmax':1.e2,'factor':1.0,'scale':'log'}
aux['temperature']={'title':r'$T [{\rm K}]$','cmap':'Spectral_r','cmin':10,'cmax':1.e7,'factor':1.0,'scale':'log'}
aux['surface_density']={'title':r'$\Sigma [{\rm M}_{\odot} {\rm pc}^{-2}]$','cmap':'pink_r','cmin':0.5,'cmax':100,'factor':1.0,'scale':'log'}

def texteffect():
    try:
        from matplotlib.patheffects import withStroke
        myeffect = withStroke(foreground="w", linewidth=3)
        kwargs = dict(path_effects=[myeffect], fontsize=12)
    except ImportError:
        kwargs = dict(fontsize=12)
    return kwargs

def scatter_sp(sp,ax,axis=0,thickness=10,type='slice',kpc=True):
    from pyathena import set_units
    from matplotlib import cm
    unit=set_units(muH=1.4271)
    Msun=unit['mass'].to('Msun').value
    Myr=unit['time'].to('Myr').value
    if len(sp) > 0: 
        if axis == 0:
            spx=sp['x1']
            spy=sp['x2']
            if type == 'slice': xbool=abs(sp['x3']) < thickness
        elif axis == 1:
            spx=sp['x1']
            spy=sp['x3']
            if type == 'slice': xbool=abs(sp['x2']) < thickness
        elif axis == 2:
            spx=sp['x2']
            spy=sp['x3']
            if type == 'slice': xbool=abs(sp['x1']) < thickness
        if kpc:
            spx = spx/1.e3
            spy = spy/1.e3
        spm=np.sqrt(sp['mass']*Msun)/4. 
        spa=sp['age']*Myr
        iyoung=np.where(spa < 40.)
        if type == 'slice':
            islab=np.where(xbool*(spa<40))
            ax.scatter(spx.loc[islab],spy.loc[islab],marker='o',\
                s=spm.loc[islab],c=spa.loc[islab],\
                vmax=40,vmin=0,cmap=cm.cool_r,alpha=1.0)
        ax.scatter(spx.loc[iyoung],spy.loc[iyoung],marker='o',\
            s=spm.loc[iyoung],c=spa.loc[iyoung],\
            vmax=40,vmin=0,cmap=cm.cool_r,alpha=0.5)

def tslice_nT(interval=5,ncols=16,imin=0,legend=True,cbar=True,labels=True,**kwargs):
    
        
    cf=pa.coolftn()

    dsfname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.0000.ds.p' % (kwargs['id'])
    domain=pickle.load(open(dsfname,'rb')).domain

    fig = matplotlib.figure.Figure(figsize=(4,14))

    for i in range(ncols):
        itime=i*interval+imin
        stime='%4.4d' % (itime)
        surffname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.surf.0.p' % (kwargs['id'],stime)
        surf=pickle.load(open(surffname,'rb'))
        slc1fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.0.p' % (kwargs['id'],stime)
        slc1=pickle.load(open(slc1fname,'rb'))
        slc2fname=kwargs['base_directory']+kwargs['directory']+'pickles/%s.%s.slice.1.p' % (kwargs['id'],stime)
        slc2=pickle.load(open(slc2fname,'rb'))

                
        vtkfname=kwargs['base_directory']+kwargs['directory']+'id0/%s.%s.vtk' % (kwargs['id'],stime)
    
        ds=pa.AthenaDataSet(vtkfname,setgrid=False)
        sp = pd.DataFrame(ds.read_starvtk())
        
        gs = gridspec.GridSpec(3, 2,height_ratios=[2,4,1],hspace=0.01,wspace=0.01)

        ax0 = fig.add_subplot(gs[0, :])
        ax=ax0
        f='surface_density'

        scatter_sp(sp,ax,axis=0,type='surf')
        imdata=surf['density'].data*surf['density'].units.cgs*aux[f]['factor']*domain['Lx'][2]*c.pc
        imdata=imdata.to('Msun/pc^2')

        im=ax.imshow(imdata.value,extent=np.array(surf['density'].bound)/1.e3,norm=LogNorm(),origin='lower')
        im.set_cmap(aux[f]['cmap'])
        im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
        ax.text(0.5, 1.2,r't=%4.2f$\,t_{\rm orb}$' % (itime/224.), horizontalalignment='center',transform = ax.transAxes)
        
        pngname=kwargs['base_directory']+kwargs['directory']+'png/%s.%s.surf.png' % (kwargs['id'],stime)

        slc=[slc1,slc2]
        
        for  j,f in enumerate(['number_density','temperature']):
            ax1 = fig.add_subplot(gs[1,j])
            ax2 = fig.add_subplot(gs[2,j])
            axes=[ax1,ax2]
            for ax,iax in zip(axes,[1,0]):
                scatter_sp(sp,ax,axis=iax,thickness=10,type='slice')
                if f is 'temperature' and slc[iax].has_key('T1'):
                    slc[iax][f].data = cf.get_temp(slc[iax]['T1'].data)
                imdata=slc[iax][f].data*slc[iax][f].units.cgs*aux[f]['factor']
            
                im=ax.imshow(imdata.value,extent=np.array(slc[iax][f].bound)/1.e3,norm=LogNorm(),origin='lower')
                im.set_cmap(aux[f]['cmap'])
                im.set_clim(aux[f]['cmin'],aux[f]['cmax'])
                


            
# labels
        axes=fig.axes
        plt.setp([ax.get_xticklabels() for ax in axes],visible=False)
        plt.setp([ax.get_yticklabels() for ax in axes],visible=False)

        if labels==True:
            ax0.text(0.5, 0.9,r'$\Sigma$',size=20, horizontalalignment='center',transform = ax0.transAxes,**(texteffect()))
            ax0.set_xlabel('x [kpc]')
            ax0.set_ylabel('y [kpc]')
            ax0.xaxis.set_label_position('top')
            ax0.xaxis.tick_top()
            ax0.xaxis.set_ticks_position('both')
            
            
            plt.setp([ax.get_xticklabels() for ax in [axes[0],axes[2],axes[4]]], visible=True)
            plt.setp([ax.get_yticklabels() for ax in [axes[0],axes[1],axes[2]]], visible=True)
            plt.setp([ax.xaxis.get_majorticklabels() for ax in [axes[2],axes[4]]], rotation=45 )

            axes[1].text(0.5, 0.95,r'$n_H$', size=20, horizontalalignment='center',transform = axes[1].transAxes,**(texteffect()))
            axes[3].text(0.5, 0.95,r'$T$', size=20,horizontalalignment='center',transform = axes[3].transAxes,**(texteffect()))
            axes[1].set_ylabel('z [kpc]')
            axes[2].set_ylabel('y [kpc]')
            axes[2].set_xlabel('x [kpc]')
            axes[4].set_xlabel('x [kpc]')
# legend
        if legend==True:
            ext=ax0.images[0].get_extent()
   
            s1=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e3)/4.,color='k',alpha=.5,label=r'$10^3 M_\odot$')
            s2=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e4)/4.,color='k',alpha=.5,label=r'$10^4 M_\odot$')
            s3=ax0.scatter(ext[1]*2,ext[3]*2,s=np.sqrt(1.e5)/4.,color='k',alpha=.5,label=r'$10^5 M_\odot$')
            ax0.set_xlim(ext[0],ext[1])
            ax0.set_ylim(ext[2],ext[3])
            ax0.legend((s1,s2,s3),(r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), \
                       scatterpoints=1, bbox_to_anchor=(1.01, 1),loc=2,fontsize=12, frameon=False)
# colorbar
            cax3 = fig.add_axes([0.93, 0.1, 0.05, 0.15])
            cax2 = fig.add_axes([0.93, 0.3, 0.05, 0.15])
            cax1 = fig.add_axes([0.93, 0.5, 0.05, 0.15])
            cax4 = fig.add_axes([0.93, 0.7, 0.05, 0.1])

            for f,cax in zip(['surface_density','number_density','temperature'],[cax1,cax2,cax3]):
                cmap = plt.get_cmap(aux[f]['cmap'])
                norm = LogNorm(vmin=aux[f]['cmin'], vmax=aux[f]['cmax'])
                cb = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm, orientation='vertical')
                cb.set_label(aux[f]['title'])
            cb = mpl.colorbar.ColorbarBase(cax4, ticks=[0,20,40],cmap=plt.cm.cool_r, norm=plt.Normalize(vmin=0,vmax=40), orientation='vertical')
            cb.set_label('age [Myr]')
        
        pngname=kwargs['base_directory']+kwargs['directory']+'png/%s.%s.png' % (kwargs['id'],stime)
                       
        canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
        canvas.print_figure(pngname,bbox_inches='tight')
        fig.clf()
        print pngname
    return fig

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-r','--range',type=str,default='0,1,1',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  print vars(args)

  sp=vars(args)['range'].split(',')
  start = eval(sp[0])
  end = eval(sp[1])
  ncols=end-start
  fskip = eval(sp[2])

  tslice_nT(imin=start,ncols=ncols,interval=fskip,**vars(args))

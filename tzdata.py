import pandas as pd
import pyathena as pa
from matplotlib.colors import LogNorm,SymLogNorm
import glob
import numpy as np
import matplotlib.pyplot as plt

class tzdata(object):
    def __init__(self,id,base='/tiger/scratch/gpfs/changgoo/',dir=None):
        self.id=id
        self.base=base
        if dir: self.basedir=self.base+dir 
        else: self.basedir=self.base+id
        self.pn4d=self.get_pn4d()
        pn=self.pn4d['whole']
        extent=[pn.minor_axis[0],pn.minor_axis[-1],pn.major_axis[0],pn.major_axis[-1]]
        Lz=extent[3]-extent[2]
        Lt=extent[1]-extent[0]
        self.kwargs={'norm':LogNorm(),'origin':'lower','aspect':Lt/Lz,'extent':extent}

        
        self.plist=self.pn4d.keys()
        self.plist.sort()
        
        self.units=pa.set_units(muH=1.4271)
        
        self.z, self.zprof_v, self.zprof_m = self.to_zprof()
        self.t, self.tprof_v, self.tprof_m = self.to_tprof()

    def get_field(self,field,unit):
        data={}
        pnw=self.pn4d['whole']
        for p in self.plist:
            pn=self.pn4d[p]
            if field in pn.keys():
                data[p]=pn[field]/pn.A*unit
            else:
                data[p]=field(pn,pnw,unit)
        return data
        
    def get_pn4d(self,correct_columns=False):
        
        flist=glob.glob(self.basedir+'/zprof_merged/%s.*.zprof.p' % self.id)
        plist=[]
        pn4d={}
        for f in flist:
            plist.append(f.split('.')[-3])
        plist.sort()
        print plist
        for p in plist:
            pn=pd.read_pickle(self.basedir+'/zprof_merged/%s.%s.zprof.p' % (self.id,p))
            if correct_columns: correct_variables(pn)
            pn4d[p]=pn

        return pn4d
        
    def to_zprof(self,trange=None):
        pnw=self.pn4d['whole']
        t=np.array(pnw.minor_axis)
        z=np.array(pnw.major_axis)
        if trange:
            tstr=trange[0]
            tend=trange[1]
        else:
            tstr=t[len(t)/2]
            tend=t[-1]
        print tstr,tend
        zprof_v={}
        zprof_m={}
        for p in self.plist:
            pn4d=self.pn4d[p]
            zprof_v[p]={}
            zprof_m[p]={}
            for field in pn4d.keys():
                zprof_v[p][field]=np.array(np.nanmean(pn4d[field].loc[:,tstr:tend],axis=1)
                                           /np.nanmean(pn4d['A'].loc[:,tstr:tend],axis=1))
    
                zprof_m[p][field]=np.array(np.nanmean(pn4d[field].loc[:,tstr:tend],axis=1)
                                           /np.nanmean(pn4d['d'].loc[:,tstr:tend],axis=1))

        return z,zprof_v,zprof_m

    def to_tprof(self,zrange=None):
        pnw=self.pn4d['whole']
        t=np.array(pnw.minor_axis)
        z=np.array(pnw.major_axis)
        if zrange:
            zstr=zrange[0]
            zend=zrange[1]
        else:
            zstr=z[0]
            zend=z[-1]
        print zstr,zend
        tprof_v={}
        tprof_m={}
        for p in self.plist:
            pn4d=self.pn4d[p]
            tprof_v[p]={}
            tprof_m[p]={}
            for field in pn4d.keys():
                tprof_v[p][field]=np.array(np.nanmean(pn4d[field].loc[zstr:zend,:],axis=0)
                                           /np.nanmean(pn4d['A'].loc[zstr:zend,:],axis=0))
    
                tprof_m[p][field]=np.array(np.nanmean(pn4d[field].loc[zstr:zend,:],axis=0)
                                           /np.nanmean(pn4d['d'].loc[zstr:zend,:],axis=0))

        Hw=get_scaleH(pnw.d,z)
        for p in self.plist:
            pn4d=self.pn4d[p]
            tprof_v[p]['H']=get_scaleH(pn4d.A,z)
            tprof_v[p]['frac']=get_fraction(pn4d.A,pnw.A,z,t)
            tprof_v[p]['frac_H']=get_fraction(pn4d.A,pnw.A,z,t,zcut=Hw)
            tprof_v[p]['frac_2H']=get_fraction(pn4d.A,pnw.A,z,t,zcut=Hw*2)
            tprof_v[p]['frac_3H']=get_fraction(pn4d.A,pnw.A,z,t,zcut=Hw*3)
            tprof_v[p]['frac_4H']=get_fraction(pn4d.A,pnw.A,z,t,zcut=Hw*4)

            tprof_m[p]['H']=get_scaleH(pn4d.d,z)
            tprof_m[p]['frac']=get_fraction(pn4d.d,pnw.d,z,t)
            tprof_m[p]['frac_H']=get_fraction(pn4d.d,pnw.d,z,t,zcut=Hw)
            tprof_m[p]['frac_2H']=get_fraction(pn4d.d,pnw.d,z,t,zcut=Hw*2)
            tprof_m[p]['frac_3H']=get_fraction(pn4d.d,pnw.d,z,t,zcut=Hw*3)
            tprof_m[p]['frac_4H']=get_fraction(pn4d.d,pnw.d,z,t,zcut=Hw*4)
        
        return t,tprof_v,tprof_m

def get_scaleH(d,z):
        
        H=np.sqrt((d.T*z**2).mean(axis=1)/d.mean(axis=0))
        return np.array(H.fillna(0.))
    
def get_fraction(d,dtot,z,t,zcut=[]):
        A=np.array(d)
        Atot=np.array(dtot)
        if len(zcut) > 0:
            for i in range(len(t)):
                idx=~(np.abs(z)<zcut[i])
                A[idx,i]=0.0
                Atot[idx,i]=0.0
        
        mf=A.sum(axis=0)/Atot.sum(axis=0)
        return np.array(mf)

def get_magnetic_energy(data,p,axis=2,zcut=[]):
        PB=np.array(data[p][('PB%s' % axis)]/data[p].A)
        z=data['whole'].major_axis
        t=data['whole'].minor_axis
        PB1d=np.zeros(len(t))
        if len(zcut) > 0:
            for i in range(len(t)):
                idx=(np.abs(z)<zcut[i])
                PB1d[i]=np.nanmean(PB[idx,i],axis=0)
        else:
            PB1d=np.nanmean(PB,axis=0)
        
        return PB1d

def get_mean(data,field,axis='t',cut=[]):
    data_tz=np.array(data[field]/data.A)
    if axis=='t':
        xaxis=data.minor_axis
        mean_axis=data.major_axis
    elif axis=='z': 
        xaxis=data.major_axis
        mean_axis=data.minor_axis
        data_tz=data_tz.T
    else: print "axis should be t or z"
    
    data_1d=np.zeros(len(axis))
    if len(cut) > 0:
        if axis=='t':
            for i in range(len(xaxis)):
                idx=(np.abs(mean_axis) < cut[i])
                data_1d[i]=np.nanmean(data_tz[idx,i],axis=0)
        elif axis=='z':
            if cut[1] > len(xaxis): cut[1]=len(xaxis)
            data_1d=np.nanmean(data_tz[cut[0]:cut[1],:],axis=0)
    else:
        data_1d = np.nanmean(data_tz,axis=0)
    
    return xaxis, data_1d

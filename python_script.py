# yt
from yt.mods import *
from yt.utilities.physical_constants import \
    mh, \
    me, \
    sigma_thompson, \
    clight, \
    kboltz, \
    G
import yt.units as u

def _ndensity(field, data):
        return data["gas","density"]/(1.4*mh)
add_field(("gas","nH"),function=_ndensity,units='cm**(-3)',display_name=r'$n_{\rm H}$')

def _pok(field, data):
        return data["gas","pressure"]/kboltz
add_field(("gas","pok"),function=_pok,units='K*cm**(-3)',display_name=r'$P/k_{\rm B}$')

def _temp(field, data):
        return data["pok"]/1.1/data["nH"]
add_field(("gas","temp"),function=_temp,units='K',display_name=r'T')

def _cs(field, data):
        return np.sqrt(data["athena","pressure"]/data["athena","density"])
add_field(("gas","cs"),function=_cs,units='cm*s**(-1)',display_name=r'$c_s$')

unit_base={"length_unit": (1.0,"pc"), 
           "time_unit": (1.0,"s*pc/km"), 
           "mass_unit": (2.34262e-24,"g/cm**3*pc**3"),
           "mu": 1.27}


def main(**kwargs):

	if not kwargs.has_key('file'):
		file='QA10_binf_TurbSN_joined.0193.vtk'
	else:
		file=kwargs['file']


	ds = load(file,parameters=unit_base)
	base = "%s" % ds

	fields=['nH','pok','temp']
	c=[25,-25,5]*u.pc
	W=100*u.pc
	N=512
	L=[1,1,1]
	for ax in ('x','y','z'):
  		slc=SlicePlot(ds,ax,fields,center=c,width=W)
  		slc.save()

# Volume rendering
	use_log=True
	tf=ColorTransferFunction((-1,3))
	tf.clear()
	tf.add_layers(10, 0.02, colormap = 'jet',alpha=[1,1,1,1,2,5,10,50,50,100])
	cam = ds.camera(c, L, W, N, tf, fields = fields[0], log_fields = [use_log])
	im = cam.snapshot(clip_ratio=6)
	cam.draw_coordinate_vectors(im)
	im.write_png(base+'_volume.png')


if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-f','--file',type=str, help='filename')
	args = parser.parse_args()
	main(**vars(args))

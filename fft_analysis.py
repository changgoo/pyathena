import glob
import os
import argparse
import cPickle as pickle


import numpy as np
from scipy import fftpack
from turb_bfield import construct3d
import pyathena as pa

def azimuthalAverage(image, center=None):
    """
    Calculate the azimuthally averaged radial profile.

    image - The 2D image
    center - The [x,y] pixel coordinates used as the center. The default is 
             None, which then uses the center of the image (including 
             fracitonal pixels).
    
    """
    # Calculate the indices from the image
    y, x = np.indices(image.shape)

    if not center:
        center = np.array([(x.max()-x.min())/2.0, (y.max()-y.min())/2.0])

    r = np.hypot(x - center[0], y - center[1])

    # Get sorted radii
    ind = np.argsort(r.flat)
    r_sorted = r.flat[ind]
    i_sorted = image.flat[ind]

    # Get the integer part of the radii (bin size = 1)
    r_int = r_sorted.astype(int)

    # Find all pixels that fall within each radial bin.
    deltar = r_int[1:] - r_int[:-1]  # Assumes all radii represented
    rind = np.where(deltar)[0]       # location of changed radius
    nr = rind[1:] - rind[:-1]        # number of radius bin

    # Cumulative sum to figure out sums for each radius bin
    csim = np.cumsum(i_sorted, dtype=float)
    tbin = csim[rind[1:]] - csim[rind[:-1]]

    radial_prof = tbin / nr

    return radial_prof

def get_power(ds):
    Nz=ds.domain['Nx'][2]
    z1=Nz/2-1
    z2=Nz/2

    B1 = ds.read_all_data('magnetic_field1')
    B2 = ds.read_all_data('magnetic_field2')
    B3 = ds.read_all_data('magnetic_field3')
    v1 = ds.read_all_data('velocity1')
    v2 = ds.read_all_data('velocity2')
    v3 = ds.read_all_data('velocity3')
    rho = ds.read_all_data('density')

    B1mean = (B1.mean(axis=1)).mean(axis=1)
    B2mean = (B2.mean(axis=1)).mean(axis=1)
    B3mean = (B3.mean(axis=1)).mean(axis=1)

    B1turb = B1 - construct3d(B1mean,B1.shape)
    B2turb = B2 - construct3d(B2mean,B2.shape)
    B3turb = B3 - construct3d(B3mean,B3.shape)

    b2=0.5*(B1turb**2+B2turb**2+B3turb**3)/rho
    v2=0.5*(v1**2+v2**2+v3**2)

    b2mid=b2[z1:z2,:,:].mean(axis=0)
    v2mid=v2[z1:z2,:,:].mean(axis=0)

    PS={}
    PSb=np.abs(fftpack.fftshift(fftpack.fft2(b2mid)))**2
    PSv=np.abs(fftpack.fftshift(fftpack.fft2(v2mid)))**2
    PSb1d=azimuthalAverage(PSb)
    PSv1d=azimuthalAverage(PSv)
    PS['B2d']=PSb
    PS['v2d']=PSv
    PS['B1d']=PSb1d
    PS['v1d']=PSv1d
    
    pfile=ds.dir+'pickles/%s.%s.PS.p' % (ds.id, ds.step)
    pickle.dump(PS,open(pfile,'wb'),pickle.HIGHEST_PROTOCOL)

def main(**kwargs):
    id=kwargs['id']
    if kwargs['directory'] == '': kwargs['directory'] = id+'/'
    dir=kwargs['base_directory']+kwargs['directory']
    if kwargs['serial']: 
      file = glob.glob(dir+id+".????.vtk")
    else:
      file = glob.glob(dir+"id0/"+id+".????.vtk")
    file.sort()
    if kwargs['range'] != '':
      sp=kwargs['range'].split(',')
      start = eval(sp[0])
      end = eval(sp[1])
      fskip = eval(sp[2])
    else:
      start = 0
      end = len(file)
      fskip = 1

    file=file[start:end:fskip]

    ds=pa.AthenaDataSet(file[0])
    grids=ds.grids

    for f in file:
      print f
      ds=pa.AthenaDataSet(f,grids=grids)
      get_power(ds)

if __name__== '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))

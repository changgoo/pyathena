#!python
"""

[grid] = ath_bin.init_grid(filename,dtype='d')
[var] = ath_bin.readvar(grid,filename,varname,dtype='d')
    varname should be
	'd' for density
	'M1', 'M2', 'M3' for momentum
	'E' for total energy
	or
	'V1', 'V2', 'V3' for velocity
	'P' for pressure
"""

import struct

def readall(filename,dtype='d'):
	"""
	%ATH_READ    Read in Athena files
	% 
	%   [TIME,DT,VAR,STATUS] = ATH_READ(GRID,FILENAME,VARNAME) reads
	%   file at location FILENAME and returns the desired variable indicated by
	%   the string VARNAME.  If the flag consistency_check is set to true, then
	%   a check is performed to ensure that the same grid metadata is used in
	%   each opened file.
	%
	%   The only supported strings for VARNAME are
	%       'd'             - density
	%       'M1','M2','M3'  - momentum density components
	%       'E'             - total energy density
	%       'B1','B2','B3'  - magnetic field components
	%       'Phi'           - gravitational potential
	%       'Er'            - radiation energy density
	%       'F1','F2','F3'  - radiation flux components
	%
	%   For a longer list of derived variables, see ATH_GETVAR
	%
	%   AUTHOR:  Chang-Goo Kim after ath_read.m by Aaron Skinner
	%   LAST MODIFIED:  11/18/2013
	"""

	grid = init_grid(filename)

	d = readvar(grid,filename,'d',dtype=dtype)
	M1 = readvar(grid,filename,'M1',dtype=dtype)
	M2 = readvar(grid,filename,'M2',dtype=dtype)
	M3 = readvar(grid,filename,'M3',dtype=dtype)

	V1 = M1/d
	V2 = M2/d
	V3 = M3/d

	if grid['nad']:
		E = readvar(grid,filename,'E',dtype=dtype)
		Ekin = 0.5*(M1*V1+M2*V2+M3*V3)
		Gamma_1=grid['gamma_1']
		P = (E - Ekin)*Gamma_1
		return grid, d, V1, V2, V3, P
	else:
		return grid, d, V1, V2, V3, None



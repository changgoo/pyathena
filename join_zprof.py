import glob
import os
import string
import pandas as pd
import argparse

def get_time(filename):
  import re
  fp=open(filename,'r')
  line=fp.readline()
  fp.close()
  time=eval(re.split('=|\s',line)[-2])

  return time


def main(**kwargs):
  base=kwargs['base_directory']
  dir=kwargs['directory']
  id=kwargs['id']
  nend = kwargs['nend']

  current_dir=os.getcwd()
  os.chdir(base+dir)


  plist=['phase1','phase2','phase3','phase4','phase5','whole']

  for p in plist:
    zpfile='%s.%s.zprof.p' % (id,p)
    if os.path.isfile(zpfile):
      mtime_zpfile=os.path.getmtime(zpfile)
      pn_old=pd.read_pickle(zpfile)
      ntime=len(pn_old.minor_axis)
    else:
      mtime_zpfile=0.
      ntime = 0
    zp_dict={}
    for i in range(nend):
      tail='.%4.4d.%s.zprof' % (i,p)
      zfile='id0/'+id+tail
      if os.path.isfile(zfile):
        time = get_time('id0/'+id+tail)
        zp=pd.read_csv(zfile,comment='#',index_col=0)
        zp_dict[time]=zp
        mtime_zf=os.path.getmtime(zfile)

    pn=pd.Panel.from_dict(zp_dict,orient='minor')
    print 'old data has %d snapshots' % ntime
    print 'new data has %d snapshots' % (len(pn.minor_axis))
    if mtime_zpfile < mtime_zf:
      if ntime < len(pn.minor_axis):
        print 'write new data for ',p
        pn.to_pickle(zpfile)
      else:
        print 'something wrong...'
    else: 
      print 'there is no new data for ',p
      
  os.chdir(current_dir)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-n','--nend',type=int,default=250,
                      help='number of files')
  args = parser.parse_args()
  main(**vars(args))

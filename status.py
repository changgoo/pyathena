import glob
import re
import os
import pyathena as pa

units=pa.set_units(muH=1.4271)
msun=units['mass'].to('M_sun').value

def grep(of,pattern):
    fp = open(of,'r')
    for line in fp:
        if re.search(pattern,line):
            return re.split('\s',line)[3]
        
def sp_from_err(ef):
    
    lines=tuple(open(ef,'r'))
    print os.path.basename(ef)
    time=[]
    mass=[]
    for l in lines:
        if re.search('create',l):
            sp=re.split('\s|\=',l)
            if eval(sp[20]) != 0: 
                print 'Warning: Noverlap=',sp[20]
            time.append(eval(sp[9]))
            mass.append(eval(sp[16])*msun)
    return np.array(time),np.array(mass)
     
def runaway_from_err(ef):
    lines=tuple(open(ef,'r'))
    print os.path.basename(ef)
    time=[]
    mass=[]
    nrunaway=0
    snrunaway=0
    for l in lines:
        if re.search('creating runaway',l):
            #sp=re.split('\s|\=',l)
            nrunaway = nrunaway+1
        if re.search('feedback at runaway',l):
            snrunaway = snrunaway+1
            
    return nrunaway,snrunaway
    
def get_status(dir):
    base1='/tiger/scratch/gpfs/changgoo/'
    base2='/tigress/changgoo/'
    
    for base in [base1,base2]:
        out_files = glob.glob(base+dir+'/*.out')
        err_files = glob.glob(base+dir+'/*.err')
        out_files.sort()
        #print base,dir
        nf=len(out_files)
        if nf != 0:
            break
            
    if nf == 0: return
    of = out_files[-1]
    
    lines=tuple(open(of,'r'))
    id=grep(of,'problem_id')  
    tlim=grep(of,'tlim')
    
    for l in lines[::-1]:
        if l.startswith('cycle'):
            sp=re.split('\s|\=',l)
            print os.path.basename(of),os.path.getmtime(of),tlim,sp[3],sp[1],sp[6]
            break
        
    for base in [base1,base2]:
        vtkfiles = glob.glob(base+dir+'/id1/%s-id1.????.vtk' % id)
        slicefiles = glob.glob(base+dir+'/pickles/%s.????.surf.0.p' % id)
        zproffiles = glob.glob(base+dir+'/id0/%s.????.whole.zprof.p' % id)
        phasefiles = glob.glob(base+dir+'/pickles/%s.????.phase.p' % id)
        rstfiles = glob.glob(base+dir+'/id1/%s-id1.????.rst' % id)
        print base, len(vtkfiles),len(slicefiles),len(zproffiles),len(phasefiles),len(rstfiles)
    
    return id

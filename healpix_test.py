import numpy as np
# healpix
import healpy as hp

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import pyathena as pa
import pandas as pd
import glob

def test_array(domain):
  dx=domain['dx']
  le=domain['left_edge']+0.5*dx
  re=domain['right_edge']-0.5*dx
  Nx=domain['Nx']
  Lx=domain['Lx']
  H=Lx[2]/4.
  x=np.linspace(le[0],re[0],Nx[0])
  y=np.linspace(le[1],re[1],Nx[1])
  z=np.linspace(le[2],re[2],Nx[2])
  array=np.empty(Nx[::-1])
  for j in range(Nx[1]):
    for i in range(Nx[0]):
      xx=x[i]
      yy=y[j]
      zz=z
      r=np.sqrt(xx**2+yy**2+zz**2)
      costh=zz/r
      sinphi=yy/np.sqrt(r**2-zz**2)
#      array[:,j,i]=np.exp(-r/H)*costh*sinphi
      array[:,j,i]=np.exp(-r/H)*np.cos(xx/Lx[0]*2.*np.pi)*np.sin(yy/Lx[1]*4*np.pi)
  return array

def cc_idx(domain,pos,periodic=True):
  le=domain['left_edge']
  dx=domain['dx']
  Nx=domain['Nx']
  if np.array(pos).ndim == 2:
    le=le[:,np.newaxis]
    dx=dx[:,np.newaxis]
    Nx=Nx[:,np.newaxis]
  idx=(pos-le-0.5*dx)/dx
  if periodic: 
    for i in range(3):
      idx[i]=np.fmod(idx[i],domain['Nx'][i])
      idx[idx < 0] += domain['Nx'][i]
  return idx

def set_domain():
  domain={}
  domain['left_edge']=np.array([-256,-256,-512])
  domain['right_edge']=np.array([256,256,512])
  domain['dx']=np.array([2,2,2])
  domain['Lx']=domain['right_edge']-domain['left_edge']
  domain['Nx']=domain['Lx']/domain['dx']
  return domain

def interp3D(input_array,indices):
  output = np.empty(indices[0].shape)
  x_indices = indices[2]
  y_indices = indices[1]
  z_indices = indices[0]

  x0 = x_indices.astype(np.integer)
  y0 = y_indices.astype(np.integer)
  z0 = z_indices.astype(np.integer)
  x1 = x0 + 1
  y1 = y0 + 1
  z1 = z0 + 1

#Check if xyz1 is beyond array boundary:
  x1[np.where(x1==input_array.shape[0])] = 0
  y1[np.where(y1==input_array.shape[1])] = 0
  z1[np.where(z1==input_array.shape[2])] = 0

  x = x_indices - x0
  y = y_indices - y0
  z = z_indices - z0
  output = (input_array[x0,y0,z0]*(1-x)*(1-y)*(1-z) +
             input_array[x1,y0,z0]*x*(1-y)*(1-z) +
             input_array[x0,y1,z0]*(1-x)*y*(1-z) +
             input_array[x0,y0,z1]*(1-x)*(1-y)*z +
             input_array[x1,y0,z1]*x*(1-y)*z +
             input_array[x0,y1,z1]*(1-x)*y*z +
             input_array[x1,y1,z0]*x*y*(1-z) +
             input_array[x1,y1,z1]*x*y*z)
  return output

def get_los(hat,domain,smax=3000.,ds=1.,vectorize=True):
  zmax=domain['right_edge'][2]-0.5*domain['dx'][2]
  zmin=domain['left_edge'][2]+0.5*domain['dx'][2]
  xhat=hat[0]
  yhat=hat[1]
  zhat=hat[2]
# vectorlized version
  if vectorize:
    sarr=np.arange(ds,smax,ds)
    zarr=zhat*sarr
    ind = np.where((zarr < zmax)*(zarr > zmin))
    sarr = sarr[ind]
    xarr=xhat*sarr
    yarr=yhat*sarr
    zarr=zhat*sarr
    iarr = cc_idx(domain,[xarr,yarr,zarr])

  else:
# while loop for s from 0 to smax
    idx=[]
    s=ds
    sarr=[]
    while s < smax:
# get x,y,z by multiplying the path length s 
      x=xhat*s
      y=yhat*s
      z=zhat*s
      if abs(z)>zmax: break
# get i,j,k by applying periodic BCs in x and y
      idx.append(cc_idx(domain,[x,y,z]))
      sarr.append(s)
      s = s+ds
# store indices and path lengthes
    iarr=np.array(idx).T
    sarr=np.array(sarr)

  return iarr,sarr

def sn_prof(field=['density','temperature','pressure','rvelocity'],nside=16):
  dir='/tigress/changgoo/sntest_TI/gpfs/data/'
  file=glob.glob(dir+'id0/n1_TI_Lx192_Nx256_TE_s1_fm0.2_h2.????.vtk')
  file.sort()
  nf=len(file)

  ipix = np.arange(hp.nside2npix(nside))

  ds=pa.AthenaDataSet(file[0])
  domain=ds.domain
  grids=ds.grids
  zmax=domain['right_edge'][2]

  iarr=[]
  rhat=[]
  for i in ipix:
# idx to theta, phi
    theta,phi = hp.pix2ang(nside,i)
# idx to xhat = sin(theta)cos(phi), yhat = sin(theta)sin(phi), zhat = cos(theta)
    xhat,yhat,zhat = hp.pix2vec(nside,i)
    idx,sarr = get_los([xhat,yhat,zhat],domain,smax=zmax,ds=domain['dx'][0]/2.)
    iarr.append(idx)
    rhat.append([xhat,yhat,zhat])

  for i,f in enumerate(file):
    ds=pa.AthenaDataSet(f,grids=grids)
    out={}
    for fi in field:
      print 'Field: '+fi
      outtmp=[]
      if fi is 'rvelocity':
        v1=ds.read_all_data('velocity1') 
        v2=ds.read_all_data('velocity2') 
        v3=ds.read_all_data('velocity3') 
      else:
        data=ds.read_all_data(fi)
      for idx,hat in zip(iarr,rhat):
        if fi is 'rvelocity':
          v1int = interp3D(v1,idx)
          v2int = interp3D(v2,idx)
          v3int = interp3D(v3,idx)
          outtmp.append(v1int*hat[0]+v2int*hat[1]+v3int*hat[2]) 
          #data=v1*hat[0]+v2*hat[1]+v3*hat[2]
          #outtmp.append(interp3D(data,idx))
        else:
          outtmp.append(interp3D(data,idx))
      outarr=np.array(outtmp)
      out[fi]=outarr.T
    pn=pd.Panel(out)
    pn[fi].index=sarr
    pnfile=dir+'pickles/%s.%s.prof.p' % (ds.id,ds.step)
    pn.to_pickle(pnfile)
    print 'Saving to '+pnfile

  return 

def main(smax=3000.,ds=1.,vectorize=True):
  nside=4
  ipix = np.arange(hp.nside2npix(nside))

  domain=set_domain()
  zmax=domain['right_edge'][2]

  fig=plt.figure()
  ax=fig.add_subplot(111,projection='3d')
  for i in ipix:
# idx to theta, phi
    theta,phi = hp.pix2ang(nside,i)
# idx to xhat = sin(theta)cos(phi), yhat = sin(theta)sin(phi), zhat = cos(theta)
    xhat,yhat,zhat = hp.pix2vec(nside,i)
    iarr,sarr = get_los([xhat,yhat,zhat],domain,smax=smax,ds=ds,vectorize=vectorize)
# get interpolated data
    #out = interp3D(in,iarr)
    #ax.plot3D([xhat,x],[yhat,y],[zhat,z])
    ax.plot3D(iarr[0],iarr[1],iarr[2])
    print sarr.max(),theta,phi,iarr[2][-2],iarr[2][-1]#,x,y,z
  return 
## get n, B at (x,y,z)
## rotate vectors to (X,Y,Z) with phi for xy and theta for x'z
## get Q_Z ~ n*(B_X^2-B_Y^2) and U_Z ~ n*B_X*B_Y 
# summing up to get Q and U (also N)



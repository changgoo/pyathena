#!/usr/bin/python

from init import *
from ath_data import *


def set_2dimg(data,step):
	data.set_step(step)
	data.readall()

	d=data.data[0]
	v1=data.data[1]
	v2=data.data[2]
	v3=data.data[3]

	nx1=data.grid['nx1']
	nx2=data.grid['nx2']
	nx3=data.grid['nx3']

	dxy=d[(nx3-1)/2,:,:]
	v1xy=v1[(nx3-1)/2,:,:]
	v2xy=v2[(nx3-1)/2,:,:]

	dxy.reshape(nx2,nx1)
	v1xy.reshape(nx2,nx1)
	v2xy.reshape(nx2,nx1)

	return dxy,v1xy,v2xy
def set_starpar(data,step):
	nstar, id, mass, pos, vel=readvtk_star(stat.dir+'id0/'+data.base+'.starpar.vtk')
	if nstar !=0:
		pos.shape = (nstar, 3)
		vel.shape = (nstar, 3)
	return

dir='/scr1/cgkim/Research/athena_StarParticle/bin/'
data=ath_data(dir+'BE/BE32T20P3A1.0000.bin')
star=readvtk_star(stat.dir+'id0/'+data.base+'.starpar.vtk')

data.set_grid()
x1=data.grid['x1']
x2=data.grid['x2']
x3=data.grid['x3']

nx1=data.grid['nx1']
nx2=data.grid['nx2']
nx3=data.grid['nx3']

extent=(x1.min(),x1.max(),x2.min(),x2.max())

fig=plt.figure(figsize=(8,8))

grid = axes_grid.ImageGrid(fig,111,
	 nrows_ncols = (2,2),
	 label_mode = "L",
	 direction='row',
	 share_all = True,
	 cbar_location='right',
	 cbar_mode='single',
	 axes_pad=0.25,)

skip=1
steps=(40,51,52,53)
for ax, step in zip(grid, steps):
	dxy, v1xy, v2xy = set_2dimg(data,step)
	vxy=np.sqrt(v1xy**2+v2xy**2)
	im=ax.imshow(np.log10(dxy),extent=extent,origin='lower',cmap=cm.gist_ncar,vmin=2,vmax=5)
	ax.set_xlim(-1,1)
	ax.set_ylim(-1,1)
	cb=ax.cax.colorbar(im,ticks=(2,3,4,5))
	ax.cax.toggle_label(True)
	q=ax.quiver(x1[::skip],x2[::skip],
                    v1xy[::skip,::skip],v2xy[::skip,::skip],
                    pivot='tip',color='r',
	            linewidths=1,headaxislength=5)
	qk = ax.quiverkey(q, 0.8, 1.05, 1, r'$1 km/s$',
          labelpos='E', fontproperties={'weight': 'bold'})

fig.savefig('img_vel.png',dpi=100)

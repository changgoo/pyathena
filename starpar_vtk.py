import numpy as np
import struct
import string
import glob
import os
import re
import pandas as pd
import astropy.constants as c
import astropy.units as u
import cPickle as p
from matplotlib import cm

def set_units(dunit=None,lunit=None,vunit=None,cgs=False,muH=1.4):
	if dunit==None: dunit=muH*c.m_p/u.cm**3
	if vunit==None: vunit=1.0*u.km/u.s
	if lunit==None: lunit=c.pc
	units = {}
        units['muH'] = muH*c.m_p.cgs
	units['density'] = dunit.to('Msun/pc^3')
	units['velocity'] = vunit.to('km/s')
	units['length'] = lunit.to('pc')
	units['time'] = (lunit/vunit).to('Myr')
	units['pressure'] = (dunit*vunit**2).to('erg/cm**3')
	units['gravitational_potential'] = (vunit**2).to('km**2/s**2')
	units['number_density'] = (dunit/units['muH']).cgs
	units['magnetic_field'] = (np.sqrt(4*np.pi*dunit*vunit**2).cgs.value*u.Gauss).to('microGauss')
	units['temperature']=1.0*u.K
	units['mass'] = (dunit*lunit**3).to('Msun')

	if cgs:
		for k in units:
			units[k]=units[k].cgs

	return units

def cc_arr(domain):
	le=domain['left_edge']
	re=domain['right_edge']
	dx=domain['dx']
	x=[]
	for i in range(3):
		x.append(np.arange(le[i],re[i],dx[i])+0.5*dx[i])

	return x

def fc_arr(domain):
	le=domain['left_edge']
	re=domain['right_edge']
	dx=domain['dx']
	x=[]
	for i in range(3):
		x.append(np.arange(le[i],re[i]+dx[i],dx[i]))

	return x

def cc_pos(domain,idx):
	le=domain['left_edge']
	dx=domain['dx']
	return le+0.5*dx+dx*np.array(idx)

def cc_idx(domain,pos):
	le=domain['left_edge']
	dx=domain['dx']
	return (pos-le-0.5*dx)/dx


def parse_filename(filename):
	"""
	#   PARSE_FILENAME    Break up a full-path filename into its component
	#   parts to check the extension, make it more readable, and extract the step
	#   number.  
	#
	#   PATH,BASENAME,STEP,EXT = PARSE_FILENAME(FILENAME)
	#
	#   E.g. If FILENAME='/home/Blast.0000.bin', then PATH='/home',
	#   BASENAME='Blast', STEP='0000', and EXT='bin'.
	#
	"""


	path=os.path.dirname(filename)
	if path[-3:] == 'id0': 
		path=path[:-3]
		mpi_mode=True
	else:
		path=path+os.path.sep
		mpi_mode=False

	base=os.path.basename(filename)
	id=base[:-9]
	step, ext=base[-8:].split('.')

	return path,id,step,ext,mpi_mode

def parse_line(line, grid):
	sp = line.strip().split()

	if "vtk" in sp:
		grid['vtk_version'] = sp[-1]
	elif "time=" in sp:
		time_index = sp.index("time=")
        	grid['time'] = float(sp[time_index+1].rstrip(','))
        	if 'level' in sp: grid['level'] = int(sp[time_index+3].rstrip(','))
        	if 'domain' in sp: grid['domain'] = int(sp[time_index+5].rstrip(','))  
		if sp[0] == "PRIMITIVE": 
			grid['prim_var_type']=True
	elif "DIMENSIONS" in sp:
		grid['Nx'] = np.array(sp[-3:]).astype('int')
	elif "ORIGIN" in sp:
		grid['left_edge'] = np.array(sp[-3:]).astype('float64')
	elif "SPACING" in sp:
		grid['dx'] = np.array(sp[-3:]).astype('float64')
	elif "CELL_DATA" in sp:
		grid['ncells'] = int(sp[-1])
	elif "SCALARS" in sp:
		grid['read_field'] = sp[1]
		grid['read_type'] = 'scalar'
	elif "VECTORS" in sp:
		grid['read_field'] = sp[1]
		grid['read_type'] = 'vector'
	elif "NSTARS" in sp:
		grid['nstar'] = eval(sp[1])
	elif "POINTS" in sp:
		grid['nstar'] = eval(sp[1])
		grid['ncells'] = eval(sp[1])

def set_field_map(grid):
	file=open(grid['filename'],'rb')
	file.seek(0,2)
	eof = file.tell()
	offset = grid['data_offset']
	file.seek(offset)

	field_map={}

	if grid.has_key('Nx'): Nx=grid['Nx']

	while offset < eof:

		line=file.readline()
		sp = line.strip().split()
		#print line,sp
			
		field_map[sp[1]] = {}
		field_map[sp[1]]['read_table']=False

		if "SCALARS" in line:
			tmp=file.readline()
			field_map[sp[1]]['read_table']=True
			field_map[sp[1]]['nvar'] = 1
		elif "VECTORS" in line:
			field_map[sp[1]]['nvar'] = 3
		else:
			print 'Error: '+sp[0] + ' is unknown type'
			raise TypeError

		field_map[sp[1]]['offset']=offset
		field_map[sp[1]]['ndata']=field_map[sp[1]]['nvar']*grid['ncells']
		if sp[1] == 'face_centered_B1':
			field_map[sp[1]]['ndata']=(Nx[0]+1)*Nx[1]*Nx[2]
		elif sp[1] == 'face_centered_B2':
			field_map[sp[1]]['ndata']=Nx[0]*(Nx[1]+1)*Nx[2]
		elif sp[1] == 'face_centered_B3':
			field_map[sp[1]]['ndata']=Nx[0]*Nx[1]*(Nx[2]+1)
				
		if sp[2]=='int': dtype='i'
		elif sp[2]=='float': dtype='f'
		elif sp[2]=='double': dtype='d'
		field_map[sp[1]]['dtype']=dtype
		field_map[sp[1]]['dsize']=field_map[sp[1]]['ndata']*struct.calcsize(dtype)
		file.seek(field_map[sp[1]]['dsize'],1)
		offset = file.tell()
		tmp=file.readline()
		if len(tmp)>1: file.seek(offset)
		else: offset = file.tell()

	#grid['field_map'] = field_map
	#grid['data']={}
	return field_map

def read_field(file_pointer,field_map):
	ndata=field_map['ndata']
	dtype=field_map['dtype']
	file_pointer.seek(field_map['offset'])
	file_pointer.readline() # HEADER
	if field_map['read_table']: file_pointer.readline()
	data = file_pointer.read(field_map['dsize'])
	var = np.asarray(struct.unpack('>'+ndata*dtype,data))

	return var


def read(starfile):
	file=open(starfile,'rb')
	star = {}
	star['filename']=starfile
	star['read_field'] = None
	star['read_type'] = None
	while star['read_field'] is None:
		star['data_offset']=file.tell()
		line = file.readline()
		parse_line(line, star)

        time=star['time']
	nstar=star['nstar']
	#print nstar
	fm=set_field_map(star)
	#print fm.keys()
	id=read_field(file,fm['star_particle_id'])
	mass=read_field(file,fm['star_particle_mass'])
	age=read_field(file,fm['star_particle_age'])
	pos=read_field(file,fm['star_particle_position']).reshape(nstar,3)
	vel=read_field(file,fm['star_particle_velocity']).reshape(nstar,3)
	file.close()
	star=[]
	for i in range(nstar):
		star.append({})

	for i in range(nstar):
		star_dict = star[i]
		star_dict['id']=id[i]
		star_dict['mass']=mass[i]
		star_dict['age']=age[i]
		star_dict['v1']=vel[i][0]
		star_dict['v2']=vel[i][1]
		star_dict['v3']=vel[i][2]
		star_dict['x1']=pos[i][0]
		star_dict['x2']=pos[i][1]
		star_dict['x3']=pos[i][2]
		star_dict['time']=time

	return pd.DataFrame(star)

def starparvtk_to_starparhist(spfiles):
    import pandas as pd
    spfiles.sort()
    sphst={}
    for f in spfiles:
        sp=read_starvtk(f)
        if len(sp) != 0:
            idx=sp['mass'] != 0
            sp=sp[idx]
            sp.index=sp.time
            if len(sp) != 0:
                for id in sp.id:
                    if sphst.has_key(id):
                        sphst[id]=pd.DataFrame.append(sphst[id],sp[sp.id == id])
                    else:
                        sphst[id]=pd.DataFrame(sp[sp.id == id])
    sppd=pd.Panel(sphst)
    fsp=f.split('.')
    sppd.to_pickle(fsp[0]+'.starpar.p')
    return sppd

def projection(sp,axis):
    if axis == 0 or axis == 'z':
        spx=sp['x1']
        spy=sp['x2']
        spz=sp['x3']
    elif axis == 1 or axis == 'y':
        spx=sp['x1']
        spy=sp['x3']
        spz=sp['x2']
    elif axis == 2 or axis == 'x':
        spx=sp['x2']
        spy=sp['x3']
        spz=sp['x1']
    return spx,spy,spz

def projection_v(sp,axis):
    if axis == 0 or axis == 'z':
        spx=sp['v1']
        spy=sp['v2']
        spz=sp['v3']
    elif axis == 1 or axis == 'y':
        spx=sp['v1']
        spy=sp['v3']
        spz=sp['v2']
    elif axis == 2 or axis == 'x':
        spx=sp['v2']
        spy=sp['v3']
        spz=sp['v1']
    return spx,spy,spz

def scatter_sp(sp,ax,axis=0,thickness=10,norm_factor=4., \
  type='slice',kpc=True,runaway=True,young=True):
    unit=set_units(muH=1.4271)
    Msun=unit['mass'].to('Msun').value
    Myr=unit['time'].to('Myr').value
    if len(sp) >0:
      runaways=(sp['mass'] == 0.0)
      sp_runaway=sp[runaways]
      sp_normal=sp[-runaways]
      #print len(sp_runaway)
      if len(sp_normal) > 0: 
        spx,spy,spz=projection(sp_normal,axis)
        if kpc:
            spx = spx/1.e3
            spy = spy/1.e3
        if type == 'slice': islice=abs(spz) < thickness
        spm=np.sqrt(sp_normal['mass']*Msun)/norm_factor
        spa=sp_normal['age']*Myr
        if young: iyoung=spa < 40. 
        else: iyoung=spa >= 0.
        #print len(iyoung[0])
        if type == 'slice':
            islab=np.where(islice*iyoung)
            ax.scatter(spx.iloc[islab],spy.iloc[islab],marker='o',\
                s=spm.iloc[islab],c=spa.iloc[islab],\
                vmax=40,vmin=0,cmap=cm.cool_r,alpha=1.0)
        iyoung = np.where(iyoung)
        ax.scatter(spx.iloc[iyoung],spy.iloc[iyoung],marker='o',\
            s=spm.iloc[iyoung],c=spa.iloc[iyoung],\
            vmax=40,vmin=0,cmap=cm.cool_r,alpha=0.5)
      if len(sp_runaway) > 0 and runaway:
        spx,spy,spz=projection(sp_runaway,axis)
        spvx,spvy,spvz=projection_v(sp_runaway,axis)
        if kpc:
            spx = spx/1.e3
            spy = spy/1.e3
        if type == 'slice': 
            islab=np.where(abs(spz) < thickness)
            #ax.scatter(spx.iloc[islab],spy.iloc[islab],marker='.',c='k',alpha=1.0)
            #ax.quiver(spx.iloc[islab],spy.iloc[islab],
            #      spvx.iloc[islab],spvy.iloc[islab],color='k',alpha=1.0)
        ax.scatter(spx,spy,marker='o',c='w',alpha=0.5,s=10.0/norm_factor)
        #ax.quiver(spx,spy,spvx,spvy,color='w',alpha=0.5)
 
def sp_legend(ax0,top=False,norm_factor=4.):
    ext=ax0.images[0].get_extent()

    s1=ax0.scatter(ext[1]*2,ext[3]*2,
      s=np.sqrt(1.e3)/norm_factor,color='k',
      alpha=.5,label=r'$10^3 M_\odot$')
    s2=ax0.scatter(ext[1]*2,ext[3]*2,
      s=np.sqrt(1.e4)/norm_factor,color='k',
      alpha=.5,label=r'$10^4 M_\odot$')
    s3=ax0.scatter(ext[1]*2,ext[3]*2,
      s=np.sqrt(1.e5)/norm_factor,
      color='k',alpha=.5,label=r'$10^5 M_\odot$')
    ax0.set_xlim(ext[0],ext[1])
    ax0.set_ylim(ext[2],ext[3])
    if top:
      legend=ax0.legend((s1,s2,s3),
        (r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), 
        scatterpoints=1,loc=2,ncol=3,fontsize='medium',
        bbox_to_anchor=(0.0, 1.1), frameon=False)
    else:
      legend=ax0.legend((s1,s2,s3),
        (r'$10^3 M_\odot$',r'$10^4 M_\odot$',r'$10^5 M_\odot$'), 
        scatterpoints=1, bbox_to_anchor=(1.01, 1),loc=2,fontsize='medium',
        frameon=False)

    return legend

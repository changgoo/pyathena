from pyathena import *
import glob
import os
import argparse

# matplotlib
import matplotlib
matplotlib.use('agg')
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc

def texteffect():
  try:
    from matplotlib.patheffects import withStroke
    myeffect = withStroke(foreground="w", linewidth=3)
    kwargs = dict(path_effects=[myeffect], fontsize=12)
  except ImportError:
    kwargs = dict(fontsize=12)
  return kwargs

def draw_slice(slice,ax):
  im=ax.imshow(slice.data,origin='lower')
  im.set_extent(slice.bound)
  ax.set_xlabel(slice.axis_labels[0])
  ax.set_ylabel(slice.axis_labels[1])

  return im


def main(**kwargs):
  rc('text', usetex=True)
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  field=kwargs['field']
  file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1

  file=file[start:end:fskip]
  fig = matplotlib.figure.Figure()
  ax1=fig.add_subplot(131)
  ax2=fig.add_subplot(132)
  ax3=fig.add_subplot(133)
  #fig.tight_layout()
  canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)

  grids=None
  for f in file:
    print 'Reading: ',f
    ds = AthenaDataSet(f,grids=grids)
    grids=ds.grids
    for iax in range(3):
      dslice = AthenaSlice(ds,axis=2-iax,field=field,center=[0.5,0.5,0.5])
      ax=fig.axes[iax]
      im=draw_slice(dslice,ax)

      im.set_norm(LogNorm())
      im.set_cmap(plt.get_cmap('jet'))
      im.set_clim(1.e-2,1.e2)

    ax=fig.axes[0]
    t1=ax.text(dslice.bound[0]*0.90,dslice.bound[1]*0.90,\
               r't=%5.1f' % ds.domain['time'],\
               ha="left", va="top", **(texteffect()))
    pngfname=dir+'png/%s.%s.%s.3slices.png' % (ds.id,field,ds.step)
    #fig.tight_layout()
    canvas.print_figure(pngfname,bbox_inches='tight')
    for ax in fig.axes: ax.cla()
    print "Output:",pngfname


if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/scratch/gpfs/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-f','--field',type=str,default='density',
                      help='field')
  parser.add_argument('-r','--range',dest='range',default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))

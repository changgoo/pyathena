import pandas as pd
import pyathena as pa
import cPickle as p
import os
import numpy as np

import rst_handler as rh
import ath_hst

import glob
import argparse

# astropy
import astropy.constants as c
import astropy.units as u


def set_field_list(par):
    shearing_box,cooling,MHD,nscalar,Gamma = get_configure(par)
    zprof_flist=['A','d','v1','v2','v3','M1','M2','M3','Ek1','Ek2','Ek3','P','T']
    if shearing_box: zprof_flist+=['dM2','dEk2']
    zprof_flist+=['Phie','gext','dWext','Phisg','gsg','dWsg','Ber']
    if cooling: zprof_flist+=['cool','heat']
    if MHD: 
        zprof_flist+='B1,B2,B3,PB1,PB2,PB3,vA1,vA2,vA3'.split(',')
        zprof_flist+='dB1,dB2,dB3,dPB1,dPB2,dPB3,dvA1,dvA2,dvA3'.split(',')
        zprof_flist+='S1,S2,S3'.split(',')
    for i in range(nscalar): zprof_flist+=['s%d' % (i+1)]
    zprof_flist+='pA,pd,pvz,pFzd,pFzM1,pFzM2,pFzM3,pFzE1,pFzE2,pFzE3,pFzP'.split(',')
    zprof_flist+='pFzEge,pFzEgsg,pFzEtidal'.split(',')
    if MHD: zprof_flist+='pSzEm1,pSzEm2,pSzvB1,pSzvB2'.split(',')
    for i in range(nscalar): zprof_flist+=['pFzs%d' % (i+1)]
    zprof_flist+='mA,md,mvz,mFzd,mFzM1,mFzM2,mFzM3,mFzE1,mFzE2,mFzE3,mFzP'.split(',')
    zprof_flist+='mFzEge,mFzEgsg,mFzEtidal'.split(',')
    if MHD: zprof_flist+='mSzEm1,mSzEm2,mSzvB1,mSzvB2'.split(',')
    for i in range(nscalar): zprof_flist+=['mFzs%d' % (i+1)]
    if shearing_box:
        zprof_flist+=['RxyL','RxyR']
        if MHD: zprof_flist+=['MxyL','MxyR']
            
    return zprof_flist

def get_configure(par):
    shearing_box=False
    if par['configure']['ShearingBox'] == 'yes': shearing_box=True 
    cooling=False
    if par['configure']['cooling'] == 'ON': cooling=True
    MHD=False
    if par['configure']['gas'] == 'mhd': MHD=True
    nscalar = eval(par['configure']['nscalars'])
    Gamma=par['problem']['gamma']
    return shearing_box,cooling,MHD,nscalar,Gamma

def get_phase(data,phase):
    temp=data['T']
    idx={}
    idx['phase1']=np.where(temp < 184)
    idx['phase2']=np.where((temp >= 184) * (temp <5050))
    idx['phase3']=np.where((temp >= 5050) * (temp <2.e4))
    idx['phase4']=np.where((temp >= 2.e4) * (temp <5.e5))
    idx['phase5']=np.where(temp >= 5.e5)
    newdata={}
    if phase == 'whole':
        for f in data: newdata[f] = np.copy(data[f])
    else:
        for f in data: 
            newdata[f] = np.zeros(data[f].shape)
            newdata[f][idx[phase]]=data[f][idx[phase]]
    return newdata

def get_zprof(data,domain,par,hst):
    Omega=par['problem']['Omega']
    qshear=par['problem']['qshear']
    shearing_box,cooling,MHD,nscalar,Gamma = get_configure(par)
    import pyathena as pa
    x,y,z,=pa.cc_arr(domain)
    Nx=domain['Nx']
    x3d=np.tile(x.reshape(1,1,Nx[0]),(Nx[2],Nx[1],1))
    z3d=np.tile(z.reshape(Nx[2],1,1),(1,Nx[1],Nx[0]))
    if shearing_box: 
        vy0=-qshear*Omega*x3d
        Phit=-qshear*Omega**2*x3d**2
    else:
        vy0=np.zeros(x3d.shape)
    
    if cooling: 
        heat_ratio=np.interp(domain['time'],hst['time'],hst['heat_ratio'])
        unit=pa.set_units(muH=1.4271)
        unitC=unit['density']*unit['velocity']**3/unit['length']
        unitC=unitC.cgs.value
        coolftn=pa.coolftn('./coolftn.p')
    
    dA=domain['dx'][0]*domain['dx'][1]
    dz=domain['dx'][2]
    
    d=np.copy(data['density']).astype('double')
    v1=np.copy(data['velocity'][...,0]).astype('double')
    v2=np.copy(data['velocity'][...,1]).astype('double')
    v3=np.copy(data['velocity'][...,2]).astype('double')
    dv2=v2-vy0
    if MHD:
        B1=np.copy(data['magnetic_field'][...,0]).astype('double')
        B2=np.copy(data['magnetic_field'][...,1]).astype('double')
        B3=np.copy(data['magnetic_field'][...,2]).astype('double')
    P=np.copy(data['pressure']).astype('double')
    Phi=np.copy(data['gravitational_potential']).astype('double')
    T1=np.copy(data['T1'])
    
    if cooling:
        lam=coolftn.get_cool(T1)
        gam=coolftn.get_heat(T1)*heat_ratio

    zprof={}

    zprof['d']=d
    zprof['v1']=v1
    zprof['v2']=v2
    zprof['v3']=v3
    zprof['P']=P
    if MHD:
        zprof['B1']=B1
        zprof['B2']=B2
        zprof['B3']=B3
        dsqrt=np.sqrt(d)
        vdotB=B1*v1+B2*v2+B3*v3
        Emag=0.5*(B1**2+B2**2+B3**2)
    meanB={'1':B1.mean(axis=2).mean(axis=1),'2':B2.mean(axis=2).mean(axis=1),'3':B3.mean(axis=2).mean(axis=1)}
    for vec in ['1','2','3']:
        zprof['M%s' % vec] = zprof['d']*zprof['v%s' % vec]
        zprof['Ek%s' % vec] = 0.5*zprof['d']*zprof['v%s' % vec]**2
        if MHD:
            zprof['PB%s' % vec] = 0.5*zprof['B%s' % vec]**2
            zprof['vA%s' % vec] = zprof['B%s' % vec]/dsqrt
            Bbar=np.tile(meanB[vec].reshape(Nx[2],1,1),(1,Nx[1],Nx[0])).astype('double')
            zprof['dB%s' % vec] = zprof['B%s' % vec] - Bbar
            zprof['dPB%s' % vec] = 0.5*zprof['dB%s' % vec]**2
            zprof['dvA%s' % vec] = zprof['dB%s' % vec]/dsqrt
            zprof['S%s' % vec] = 2.0*Emag*zprof['v%s' % vec] - zprof['B%s' % vec]*vdotB
    if shearing_box:
        zprof['v2']=dv2
        zprof['dM2'] = zprof['d']*zprof['v2']
        zprof['dEk2'] = 0.5*zprof['d']*zprof['v2']**2
#        zprof['Phit'] = Phit
    zprof['T']=coolftn.get_temp(T1)
    if cooling:
        zprof['cool']=d**2*lam/unitC
        zprof['heat']=d*gam/unitC
    for ns in range(nscalar):
        zprof['s%s' % (ns +1)]=data['specific_scalar%s' % ns]*d
        
    import tigress_potential as tp
    param={}
    param['R0']=par['problem']['R0']*c.pc
    param['rho_dm']=par['problem']['rhodm']*c.M_sun/c.pc**3
    param['surf_s']=par['problem']['SurfS']*c.M_sun/c.pc**2
    param['surf_g']=par['problem']['surf']*c.M_sun/c.pc**2
    param['z0']=par['problem']['zstar']*c.pc
    phiext=tp.extpot(param).phiext
    Phie=phiext(z3d*c.pc).to('km**2/s**2').value
    gext=phiext((z3d+dz/2)*c.pc)-phiext((z3d-dz/2)*c.pc)
    zprof['Phie']=Phie
    zprof['gext']=gext.to('km**2/s**2').value/dz
    zprof['dWext']=d*zprof['gext']
    zprof['Phisg']=Phi
    dPhi=np.copy(Phi)
    dPhi[1:-1,:,:]=(Phi[2:,:,:]-np.roll(Phi,2,axis=0)[2:,:,:])/2.0/dz
    dPhi[0,:,:]=(Phi[1,:,:]-Phi[0,:,:])/dz
    dPhi[-1,:,:]=(Phi[-1,:,:]-Phi[-2,:,:])/dz
    zprof['gsg']=dPhi
    zprof['dWsg']=d*zprof['gsg']
    zprof['Ber'] = 0.5*(v1**2+v2**2+v3**2) + Gamma/(Gamma-1)*P/d+Phie+Phi+Phit

    for pm in ['p','m']:
        zprof[pm+'A'] = np.ones(d.shape)*dA
        zprof[pm+'d'] = np.copy(d)
        zprof[pm+'vz'] = np.copy(v3)
        for f in ['d','M1','M2','M3']:
            zprof['%sFz%s' % (pm,f)] = zprof[f]*v3
        for f in ['E1','E2','E3','Ege','Egsg','Etidal']:
            if f in ['E1','E2','E3']: 
                zf='%sk%s' %(f[0],f[1])
                tmp = zprof[zf]*v3
            elif f == 'Ege': tmp = d*Phie*v3
            elif f == 'Egsg': tmp = d*Phi*v3
            elif f == 'Etidal': tmp = d*Phit*v3
            zprof['%sFz%s' % (pm,f)] = tmp
        zprof['%sFzP' % pm] = Gamma/(Gamma-1)*zprof['P']*v3
        if MHD:
            zprof['%sSzEm1' % pm] = 2.0*zprof['PB1']*v3
            zprof['%sSzEm2' % pm] = 2.0*zprof['PB2']*v3
            zprof['%sSzvB1' % pm] = -B3*B1*v1
            zprof['%sSzvB2' % pm] = -B3*B2*v2
        for ns in range(nscalar):
            zprof['%sFzs%s' % (pm,ns+1)]=data['specific_scalar%s' % ns]*d*v3
    if shearing_box:
        zprof['Rxy']=d*v1*dv2
        zprof['Mxy']=-B1*B2
        
    nv3=zprof['v3']<0
    pv3=~nv3
    for k in zprof:
        if k.startswith('p'): zprof[k][nv3]=0
        if k.startswith('m'): zprof[k][pv3]=0

    zprof['A']=np.ones(d.shape)*dA

    return z,zprof

def get_mean(zprof,phase):
    Nx=zprof['d'].shape
    dA=zprof['A'][0,0,0]

    pzprof=get_phase(zprof,phase)
    keys=pzprof.keys()
    for k in keys:
        if k == 'Rxy' or k == 'Mxy':
            stress=pzprof.pop(k)
            stress=stress.sum(axis=1)
            pzprof[k+'L'] = stress[:,0]*Nx[-1]*dA
            pzprof[k+'R'] = stress[:,-1]*Nx[-1]*dA
        elif k == 'A':
            pzprof[k]=pzprof[k].sum(axis=2).sum(axis=1)
        else:
            pzprof[k]=pzprof[k].sum(axis=2).sum(axis=1)*dA

    return pzprof

def get_full_domain(par):
    full_domain={}
    full_domain['left_edge']=np.array([par['domain1']['x1min'],par['domain1']['x2min'],par['domain1']['x3min']]).astype('float')
    full_domain['right_edge']=np.array([par['domain1']['x1max'],par['domain1']['x2max'],par['domain1']['x3max']]).astype('float')
    full_domain['Nx']=np.array([par['domain1']['Nx1'],par['domain1']['Nx2'],par['domain1']['Nx3']])
    full_domain['Lx']=full_domain['right_edge']-full_domain['left_edge']
    full_domain['dx']=full_domain['Lx']/full_domain['Nx']
    return full_domain

def main(**kwargs):

    dir=kwargs['base_directory']+kwargs['directory']
    id=kwargs['id']
    if kwargs['serial']:
        files = glob.glob(dir+id+".????.vtk")
    else:
        files = glob.glob(dir+"id0/"+id+".????.vtk")
    files.sort()

    if kwargs['range'] != '':
        sp=kwargs['range'].split(',')
        start = eval(sp[0])
        end = eval(sp[1])
        fskip = eval(sp[2])
    else:
        start = 0
        end = len(files)
        fskip = 1

    files=files[start:end:fskip]

#    ds0 = pa.AthenaDataSet(files[0])

    if not os.path.isdir(dir+'zprof/'): os.mkdir(dir+'zprof/')

    rst_fnames=glob.glob('%s/rst/%s.????.rst' %(dir,id))+glob.glob('%s/id0/%s.????.rst' %(dir,id))
    rst_fnames.sort()
    par=rh.parse_par(rst_fnames[0])
    hst_fname=glob.glob('%s/hst/%s.hst' % (dir,id))+glob.glob('%s/id0/%s.hst' % (dir,id))
    hst=ath_hst.read_w_pandas(hst_fname[0])

    flist=set_field_list(par)
    full_domain=get_full_domain(par)
    x,y,z,=pa.cc_arr(full_domain)
    zidx=pd.Series(z,name='z')
    plist=['whole','phase1','phase2','phase3','phase4','phase5']
    i=0
    for f in files:
        #zprof_wname=f.replace('vtk','whole.zprof.p')
        #if not os.path.exists(zprof_wname):
            print 'Reading: ',f
            zpdf={}
            for phase in plist:
                zpdf[phase]=pd.DataFrame(columns=flist,index=zidx,dtype='double')
#            ds = pa.AthenaDataSet(f)
            if i==0: 
                ds = pa.AthenaDataSet(f)
                ds0 = ds
                i=i+1
            else:
                ds = pa.AthenaDataSet(f,ds=ds0)
  
            for islab in range(ds.NGrids[2]):
                grids=ds._get_slab_grid(islab+1,verbose=True)
                slab_domain=ds._setup_domain(grids)
                data={}
                for fi in ds.field_list+['T1']:
                    data[fi]=ds.read_all_data(fi,slab=islab+1)
                z_part,zpw_part=get_zprof(data,slab_domain,par,hst)
                for phase in plist:
                    zp_part=get_mean(zpw_part,phase)
                    zidx_part=pd.Series(z_part,name='z')
                    zpdf_part=pd.DataFrame(zp_part,columns=flist,index=zidx_part)
                    zpdf[phase]=zpdf[phase].add(zpdf_part,fill_value=0)
                for g in grids:
                    g['data'].clear()

            for phase in plist:
                zprof_fname=f.replace('vtk','%s.zprof.p' % phase).replace('id0','zprof')
                zpdf[phase].to_pickle(zprof_fname)
                print 'Writing: ', zprof_fname
#            zprof_fname='%s/id0/%s.%s.%s.zprof.p' % (dir,id,ds.step,phase)
#            if os.path.exists(zprof_fname):
#                zprof=p.load(open(zprof_fname,'rb'))
#                zp=zpdf[phase]
#                for f in flist:
#                    new=np.array(zp[f]/zp['A'])
#                    ref=np.array(zprof[f]/zprof['A'])
#                    diff=np.abs(new-ref)
#                    maxidx=diff.argmax()
#                    diff_max=diff.max()/ref[maxidx]
#                    if diff_max > 1.e-2 and ref[maxidx] > 1.e-7: 
#                        print phase,f,diff_max,new[maxidx],ref[maxidx]
#                zpdf[phase].to_pickle('%s/pickles/%s.%s.%s.zprof.p' % (dir,id,ds.step,phase))
#            else:

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-b','--base_directory',type=str,
                        default='/tigress/changgoo/',
                        help='base working directory')
    parser.add_argument('-d','--directory',type=str,default='',
                        help='working directory')
    parser.add_argument('-i','--id',type=str,
                        help='id of dataset')
    parser.add_argument('-s','--serial',action='store_true',help='serial mode')
    parser.add_argument('-r','--range',type=str,default='',
                        help='time range, start:end:skip')
    args = parser.parse_args()
    main(**vars(args))

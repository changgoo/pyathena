#!/usr/bin/python

from init import *
import ath_hst
import glob
from ath_data import *
from optparse import OptionParser

class rprof:
	def __init__(self,data):
		self.data=data
		self.data.set_grid()
		self.setup(self.data.grid)

	def set_unit(self):
		self.lunit=pc
		self.dunit=1.4*mp
		self.vunit=1.e5
		self.tunit=self.lunit/self.vunit/yr/1.e6
		self.Punit=self.dunit*self.vunit**2

	def setup(self,grid):
		self.x1=grid['x1']
		self.x2=grid['x2']
		self.x3=grid['x3']
		self.nx1 = grid['nx1']
		self.nx2 = grid['nx2']
		self.nx3 = grid['nx3']
		self.dx1 = grid['dx1']
		self.dx2 = grid['dx2']
		self.dx3 = grid['dx3']
	
	def read(self):
		self.d = self.data.readvar(self.data.grid,'d')
		vel = self.data.readvar(self.data.grid,'V')
		self.v1 = vel[:,:,:,0]
		self.v2 = vel[:,:,:,1]
		self.v3 = vel[:,:,:,2]
		self.P = self.data.readvar(self.data.grid,'P')

	def cal_rprof(self,xsp,vsp,rin=0.,interp=False):
		x0 = xsp[0]
		y0 = xsp[1]
		z0 = xsp[2]

		vx0 = vsp[0]
		vy0 = vsp[1]
		vz0 = vsp[2]
		x3d = np.tile(self.x1.reshape(1,1,self.nx1),(self.nx3,self.nx2,1))
		y3d = np.tile(self.x2.reshape(1,self.nx2,1),(self.nx3,1,self.nx1))
		z3d = np.tile(self.x3.reshape(self.nx3,1,1),(1,self.nx2,self.nx1))

		r3d = np.sqrt((x3d-x0)**2+(y3d-y0)**2+(z3d-z0)**2)
		if rin == 0:
			rin = r3d.max()
		ind=np.where(r3d < rin)
		d = self.d[ind]
		P = self.P[ind]
		v1 = self.v1[ind]
		v2 = self.v2[ind]
		v3 = self.v3[ind]
		x3d = x3d[ind] - x0
		y3d = y3d[ind] - y0
		z3d = z3d[ind] - z0
		r3d = r3d[ind]
		
		self.vr3d = ((v1-vx0)*x3d+(v2-vy0)*y3d+(v3-vz0)*z3d)/r3d
		self.d3d = d
		self.P3d = P
		dx = self.dx1*0.5
		r1d = np.arange(dx,rin,dx)

		vr1d = np.zeros(len(r1d))
		d1d = np.zeros(len(r1d))
		P1d = np.zeros(len(r1d))
		for i in range(len(r1d)):
			ind = ((r3d >= (r1d[i]-0.5*dx)) & (r3d <= (r1d[i]+0.5*dx)))
			vr1d[i] = np.sum(self.vr3d[ind]*d[ind])/np.sum(d[ind])
			d1d[i] = np.mean(d[ind])
			P1d[i] = np.mean(P[ind])
		self.r3d = r3d.flatten()
		self.r1d = r1d
		self.vr1d = vr1d
		self.d1d = d1d
		self.P1d = P1d

def plot_all(axes,rp):
	x3d=rp.r3d
	x1d=rp.r1d
	ind=x3d.argsort()
	cs = np.sqrt(rp.P3d/rp.d3d)
	cs1d = np.sqrt(rp.P1d/rp.d1d)
	Mach =rp.vr3d/cs
	Mach1d =rp.vr1d/cs1d
	data3d=(rp.d3d.flatten(),rp.P3d.flatten(),rp.vr3d.flatten(),Mach.flatten())
	data1d=(rp.d1d,rp.P1d,rp.vr1d,Mach1d)
	for ax, y, y1d in zip(axes, data3d, data1d):
		ax.plot(x3d[ind],y[ind],ls='',marker='.',alpha=0.2)
		ax.plot(x1d,y1d,ls=':',marker='o')

parser = OptionParser()
parser.add_option('-b','--base-directory', dest='base_dir',
                  help='base directory for data',
                  default='/scr1/cgkim/Research/athena_StarParticle/bin/')
parser.add_option('-d','--directory',dest='dir',
                  help='working directory')
parser.add_option('-i','--id',dest='id',
                  help='id of dataset')
parser.add_option('-o','--out-filename',dest='out_fname',
                  help='output filename')
parser.add_option('-s','--spid',dest='spid',
                  help='id of starparticle',type='int',default=0)
(options, args) = parser.parse_args()

spid = '.%4.4d' % options.spid
vtkfiles=glob.glob(options.base_dir+options.dir+options.id+'.*.vtk')
starfiles=glob.glob(options.base_dir+options.dir+'id0/'+options.id+'.*.starpar.vtk')
starhstfile = options.base_dir+options.dir+'id0/'+options.id+spid+'.star'
starhst = ath_hst.read(starhstfile)
keys=starhst.keys()
keys.sort()
time=starhst[keys[0]]
xsp = [starhst[keys[2]][0],starhst[keys[3]][0],starhst[keys[4]][0]]
vsp = [starhst[keys[5]][0],starhst[keys[6]][0],starhst[keys[7]][0]]
print time[0],xsp, vsp
itime = int(time[0])

if options.out_fname == None: options.out_fname='png/'+options.id+spid+'_rprof.png'

fig, axes=plt.subplots(nrows=2,ncols=2,figsize=(8,8))
axes = axes.flatten()
for i in (itime, itime+1):
	stime = '.%4.4d' % i
	vtkfile = options.base_dir+options.dir+options.id+stime+'.vtk'
	athdata=ath_data(vtkfile)
	rp = rprof(athdata)
	rp.read()
	rp.set_unit()
	rp.cal_rprof(xsp,vsp,rin=5)
	plot_all(axes,rp)

TLP=15.
r1d=np.logspace(-2,1,100)
nHLP=8.86*1.1*kbol*TLP/(1.4*mp)**2/Gconst/r1d**2/pc**2/np.pi/4.0
axes[0].plot(r1d,nHLP)
for ax in axes[:-2]: ax.set_yscale('log')
for ax in axes: ax.set_xscale('log')
for ax in axes: ax.set_xlim(1.e-1,1.e1)
axes[0].set_ylim(1.e-2,1.e5)
plt.tight_layout()
mystyle()
fig.savefig(options.out_fname,dpi=100)

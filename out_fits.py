from pyathena import *
import glob
import os
import argparse
import cPickle as pickle

import astropy.constants as c
import astropy.units as u
from astropy.io import fits

def main(**kwargs):
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  fields=kwargs['fields']
  file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1

  new_mode=False
  if kwargs.has_key('new'): new_mode=kwargs['new']

  file=file[start:end:fskip]

  dunit=1.4*c.m_p/u.cm**3
  lunit=u.pc
  vunit=u.km/u.s

  if not os.path.isdir(dir+'fits/'):
    os.mkdir(dir+'fits/')

  dspickle=dir+'id0/'+id+'.0000.ds.p'

  if os.path.isfile(dspickle) and (not new_mode):
    f=open(dspickle,'rb')
    ds = pickle.load(f)
    grids = ds.grids
  else:
    f=open(dspickle,'wb')
    grids=None
    ds = AthenaDataSet(file[0],grids=grids)
    ds.set_units(dunit,lunit,vunit)
    pickle.dump(ds,f,pickle.HIGHEST_PROTOCOL)
  f.close()

  print ds.domain['field_map'].keys()
  units = ds.units
  for f in file:
    ds = AthenaDataSet(f,grids=grids)
    for type in ('surf','slice'):
      for iax in [2,1,0]:
        slicefits=dir+'fits/'+'%s.%s.%s.%1d.fits' % (ds.id,ds.step,type,iax)
        hdr = fits.Header()
        hdr['axis']=iax
        hdr['time']=ds.domain['time']
        hdu = fits.PrimaryHDU(header=hdr)
	hdu.writeto(slicefits,clobber=True)
        for nf,field in enumerate(fields):
          print 'Field:', field,iax,type
          if type == 'surf': slice=AthenaSurf(ds,axis=iax,field=field)
          elif type == 'slice': slice=AthenaSlice(ds,axis=iax,field=field,center=(0.5,0.5,0.5))
          hdr = fits.Header()
          hdr['field']=field
          fits.append(slicefits,slice.data,hdr)
        print 'Write Fits:',slicefits

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('fields',type=str,nargs='*',
                      default=['density','pressure','magnetic_pressure','plasma_beta'],
                      help='fields')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  parser.add_argument('--mean',action='store_true',default=False,
                      help='toggle to plot mean along an axis')
  parser.add_argument('--new',action='store_true',default=False,
                      help='toggle to store pickle files')
  args = parser.parse_args()
  main(**vars(args))

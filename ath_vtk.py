#!python
"""
  dependency: VTK library with Python Wrapper

  grid = ath_vtk.init_grid(filename)
  grid, d, v1, v2, v3, P = ath_vtk.read_all(filename, dtype='f')
"""

import sys
sys.path.append('/u/cgkim/Sources/VTK/VTK6.0.0/Wrapping/Python')
sys.path.append('/u/cgkim/Sources/VTK/VTK6.0.0/Wrapping/Python/vtk')
sys.path.append('/u/cgkim/Sources/VTK/VTK6.0.0/lib')

import vtk
import numpy as np

def init_grid(filename):

	reader=vtk.vtkDataSetReader()
	reader.SetFileName(filename)
	reader.ReadAllScalarsOff()
	reader.ReadAllVectorsOff()
	reader.Update()

	data=reader.GetOutput()

	grid={}

	head=reader.GetHeader()
	import re
	time=eval(re.split(' ',head)[4])[0]
	grid['time']=time

	dim=data.GetDataDimension()
	grid['ndim']=dim

	dx=data.GetSpacing()
	dx1=dx[0]
	dx2=dx[1]
	dx3=dx[2]
	grid['dx1']=dx1
	grid['dx2']=dx2
	grid['dx3']=dx3

	xlim=data.GetBounds()
	x1min=xlim[0]
	x1max=xlim[1]
	x2min=xlim[2]
	x2max=xlim[3]
	x3min=xlim[4]
	x3max=xlim[5]
	grid['x1min']=x1min
	grid['x2min']=x2min
	grid['x3min']=x3min
	grid['x1max']=x1max
	grid['x2max']=x2max
	grid['x3max']=x3max

	Nx1=(x1max-x1min)/dx1
	Nx2=(x2max-x2min)/dx2
	Nx3=(x3max-x3min)/dx3
	grid['Nx1']=Nx1
	grid['Nx2']=Nx2
	grid['Nx3']=Nx3

	print dx1,dx2,dx3
	print x1min,x1max
	print x2min,x2max
	print x3min,x3max
	print Nx1,Nx2,Nx3

	x1=np.linspace(x1min+0.5*dx1,x1max-0.5*dx1,Nx1)
	x2=np.linspace(x2min+0.5*dx2,x2max-0.5*dx2,Nx2)
	x3=np.linspace(x3min+0.5*dx3,x3max-0.5*dx3,Nx3)
	grid['x1']=x1
	grid['x2']=x2
	grid['x3']=x3

	return grid

def readall(filename,dtype='f'):

	grid = init_grid(filename)

	reader=vtk.vtkDataSetReader()
	reader.SetFileName(filename)
	reader.ReadAllScalarsOn()
	reader.ReadAllVectorsOn()
	reader.Update()

	data=reader.GetOutput()

	den=data.GetCellData().GetArray('density')
	vel=data.GetCellData().GetArray('velocity')
	pres=data.GetCellData().GetArray('pressure')

	if pres == None:
		grid['nad']=0
	else:
		grid['nad']=1

	dim=np.asarray(data.GetDimensions())-1
	darr=np.zeros(den.GetSize(),dtype=dtype)
	varr=np.zeros(vel.GetSize(),dtype=dtype)
	parr=np.zeros(pres.GetSize(),dtype=dtype)

	den.ExportToVoidPointer(darr)
	vel.ExportToVoidPointer(varr)
	pres.ExportToVoidPointer(parr)

	darr.shape=(dim[2],dim[1],dim[0])
	varr.shape=(dim[2],dim[1],dim[0],3)
	parr.shape=(dim[2],dim[1],dim[0])

	v1=varr[:,:,:,0]
	v2=varr[:,:,:,1]
	v3=varr[:,:,:,2]

	return grid, darr, v1, v2, v3, parr
# Should swap dimension

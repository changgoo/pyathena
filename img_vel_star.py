#!/usr/bin/python

from init import *
from ath_data import *
import os
import glob
from optparse import OptionParser


def set_step(grid,data,step):
	data.set_step(step)
	if data.ext =='vtk':
		d=data.readvtk(grid,'density')
		v=data.readvtk(grid,'velocity')
		if v != None:
			v1 = v[:,:,:,0]
			v2 = v[:,:,:,1]
			v3 = v[:,:,:,2]
		else:
			M=data.readvtk(grid,'momentum')
			v1 = M[:,:,:,0]/d
			v2 = M[:,:,:,1]/d
			v3 = M[:,:,:,2]/d
	else:
		data.readall()
		d=data.data[0]
		v1=data.data[1]
		v2=data.data[2]
		v3=data.data[3]

	T=data.readvtk(grid,'pressure')/d*1.4*mp*1.e10/kbol/1.1
	return d,v1,v2,v3,T

def set_2dimg(grid,d,v1,v2,v3,T,symaxis='z',column_density=True,pos=False):

		
	if symaxis=='z':
		nx=grid['nx1']
		ny=grid['nx2']
		nz=grid['nx3']
		dz=grid['dx3']
		vx=v1
		vy=v2-v20
		x=grid['x1']
		y=grid['x2']
		axis=0

		if pos == False:
			nz0=nz/2-1
		else:
			nz0=np.round(pos[2]-grid['x3'].min())
		dxy=d[nz0,:,:]
		v1xy=vx[nz0,:,:]
		v2xy=vy[nz0,:,:]
		Txy=T[nz0,:,:]

	elif symaxis=='y':
		nx=grid['nx1']
		ny=grid['nx3']
		nz=grid['nx2']
		dz=grid['dx2']
		vx=v1
		vy=v3
		x=data.grid['x1']
		y=data.grid['x3']

		if pos == False:
			ny0=nz/2-1
		else:
			ny0=np.round(pos[1]-grid['x2'].min())
		axis=1
		dxy=d[:,ny0,:]
		v1xy=vx[:,ny0,:]
		v2xy=vy[:,ny0,:]
		Txy=T[:,ny0,:]

	extent=(x.min(),x.max(),y.min(),y.max())

	if column_density:
		dxy=d.sum(axis=axis)
		v1xy=(d*vx).sum(axis=axis)/dxy
		v2xy=(d*vy).sum(axis=axis)/dxy
		Txy=(d*T).sum(axis=axis)/dxy
		dxy=dxy*dz*munit
		if symaxis=='z': 
			print dxy.mean(),vx.max(),vy.max(),T.min()
	#v1xy=vx.mean(axis=axis)
	#v2xy=vy.mean(axis=axis)

	dxy.reshape(ny,nx)
	v1xy.reshape(ny,nx)
	v2xy.reshape(ny,nx)
	Txy.reshape(ny,nx)

	return dxy,v1xy,v2xy,Txy,x,y,extent

def plot_starpar(ax,data,step,symaxis='z'):
	step = '.%04d.' % (data.step)
	starfile=data.dir+data.id+step+'starpar.vtk'
	if not os.path.isfile(starfile): starfile=data.dir+'id0/'+data.id+step+'starpar.vtk'
	if not os.path.isfile(starfile): return
	nstar, id, mass, pos, vel=readvtk_star(starfile)
	mass = mass*munit/100.
	if nstar !=0:
		pos.shape = (nstar, 3)
		vel.shape = (nstar, 3)
		if symaxis=='z':
			x=pos[:,0]
			y=pos[:,1]
			vx=vel[:,0]
			vy=vel[:,1]+qshear*Omega*x
		elif symaxis=='y':
			x=pos[:,0]
			y=pos[:,2]
			vx=vel[:,0]
			vy=vel[:,2]
		ax.scatter(x,y,marker='o',color='m',s=mass,edgecolor='black',alpha=0.5)
		q=ax.quiver(x,y,vx,vy,
                    pivot='tail',color='r',scale=15,
	            headlength=5,headwidth=3,headaxislength=5,clip_on=False)
		if symaxis=='z':
			qk = ax.quiverkey(q, 0.1, 1.15, 1, r'$1 km/s$',
                   	    labelpos='E', color='r',fontproperties={'weight': 'bold'})

	return

parser = OptionParser()
parser.add_option('-b','--base_directory',dest='base_dir',
                  default='/scr1/cgkim/Research/athena_StarParticle/bin/',
                  help='base working directory')
parser.add_option('-d','--directory',dest='dir',
                  default='',
                  help='working directory')
parser.add_option('-i','--id',dest='id',
                  help='id of dataset')
parser.add_option('-o','--out-directory',dest='out_dir',
                  help='working directory')
parser.add_option('-r','--range',dest='range',
                  help='range of steps. start,end')
parser.add_option('-v','--vrange',dest='vrange',
                  help='range of data. min,max')
(options, args) = parser.parse_args()

if options.out_dir == None: options.out_dir=options.id

file=glob.glob(options.base_dir+options.dir+options.id+'.????.vtk')
file.sort()
data=ath_data(file[0])
nfile=len(file)

munit=1.4*mp*pc*pc*pc/msun
Q=0.7
nJ=4
qshear=1.0
Omega=28.e-3
cs=np.sqrt(2.0*(2.0-qshear))/np.pi/Q/nJ

if options.range == None: 
	start = 0
	end = nfile
	fskip = 1
else:
	start = eval(options.range.split(',')[0])
	end = eval(options.range.split(',')[1])
	fskip = eval(options.range.split(',')[2])

if options.vrange == None: 
	vmin = -1
	vmax = 2
else:
	vmin = eval(options.vrange.split(',')[0])
	vmax = eval(options.vrange.split(',')[1])


data.set_grid()
#r3d,x3d,y3d,z3d = data.rprof()
#v20=-qshear*Omega*x3d
v20=0.0
grid=data.grid
x1=data.grid['x1']
x2=data.grid['x2']
x3=data.grid['x3']
Lx=x1.max()-x1.min()
Ly=x2.max()-x2.min()
Lz=x3.max()-x3.min()
ratio=(Lx+Lz)/Lx

symaxis='z'


skip=x1.size/64
steps=range(start,end,fskip)

if not os.path.isdir('png/'+options.out_dir): os.mkdir('png/'+options.out_dir)
for step in steps:
    d,v1,v2,v3,T=set_step(grid,data,step)
    time=data.readtime(grid)
    for column in (True, False):

	if column:
		vrange=[-2,2]
	else:
		vrange=[vmin,vmax]
	nrow=2
	ncol=2
	fig=plt.figure(figsize=(5*ncol,5*ratio))
	axgrid = axes_grid.ImageGrid(fig,111,
		 nrows_ncols = (nrow,ncol),
		 label_mode = "L",
		 direction='row',
		 share_all = False,
		 cbar_location='right',
		 cbar_mode='each',
		 cbar_size='3%',
		 axes_pad=0.25,)

	Nax=axgrid.ngrids

# for midplane slice
	x0=0.
	y0=0.
	z0=0.
# for slice along massive star particle
#	step = '.%04d.' % (data.step)
#	starfile=data.dir+data.id+step+'starpar.vtk'
#	if not os.path.isfile(starfile): starfile=data.dir+'id0/'+data.id+step+'starpar.vtk'
#	nstar, id, mass, pos, vel=readvtk_star(starfile)
#	if nstar !=0:
#		ind=mass.argmax()
#		pos.shape = (nstar, 3)
#		vel.shape = (nstar, 3)
#		x0=pos[ind,0]
#		y0=pos[ind,1]
#		z0=pos[ind,2]
#	z0=0.

	for i,symaxis in zip((0,2),('z','y')):
		ax1=axgrid[i]
		ax2=axgrid[i+1]
		dxy, v1xy, v2xy, Txy, x, y, extent = \
                  set_2dimg(grid,d,v1,v2,v3,T,symaxis=symaxis,column_density=column,pos=(x0,y0,z0))
		vxy=np.sqrt(v1xy**2+v2xy**2)
		vxymax=vxy.max()
		if vxymax > 100: vscale=2000
		else: vscale=200
		imd=ax1.imshow(np.log10(dxy),extent=extent,origin='lower',
       		  interpolation='nearest',cmap=cm.jet,vmin=vrange[0],vmax=vrange[1])
		q=ax1.quiver(x[::skip],y[::skip],
		  v1xy[::skip,::skip],v2xy[::skip,::skip],
		  pivot='tail',color='black',scale=vscale,alpha=0.5,
		  headlength=6,headwidth=4,headaxislength=6,clip_on=False)
		ax1.set_xlim(extent[0], extent[1])
		ax1.set_ylim(extent[2], extent[3])
		if i == 0: 
			ax1.set_xlabel('x')
			ax1.set_ylabel('y')
		else: 
			ax1.set_xlabel('x')
			ax1.set_ylabel('z')

		imT=ax2.imshow(np.log10(Txy),extent=extent,origin='lower',
       		  interpolation='nearest',cmap=cm.spectral,vmin=1,vmax=8)
		if not column:
			if i == 0: ax2.axhline(y0,ls=':')
			if i == 2: ax2.axhline(z0,ls=':')
		ax2.set_xlim(extent[0], extent[1])
		ax2.set_ylim(extent[2], extent[3])

		plot_starpar(ax1,data,step,symaxis=symaxis)
		plot_starpar(ax2,data,step,symaxis=symaxis)
		cbd=ax1.cax.colorbar(imd)
		cbT=ax2.cax.colorbar(imT)

		if(i==0):
			if vxymax > 100: vkey=100. 
			else: vkey=10.
			qk = ax1.quiverkey(q, 0.1, 1.05, vkey, (r'$%3d km/s$' % vkey),
       	   	    	  labelpos='E', fontproperties={'weight': 'bold'})
			ax1.set_title('%5.2f' % (time))#*Omega/2./np.pi))

			ax2.scatter(extent[0]+(extent[1]-extent[0])*0.0,extent[3]*1.08,
			    marker='o',color='m',s=1,edgecolor='black',alpha=0.5,clip_on=False)
			ax2.text(extent[0]+(extent[1]-extent[0])*0.05,extent[3]*1.08,
			    r'$10^2 M_\odot$',verticalalignment='center')
			ax2.scatter(extent[0]+(extent[1]-extent[0])*0.4,extent[3]*1.08,
			    marker='o',color='m',s=10,edgecolor='black',alpha=0.5,clip_on=False)
			ax2.text(extent[0]+(extent[1]-extent[0])*0.45,extent[3]*1.08,
			    r'$10^3 M_\odot$', verticalalignment='center')
			ax2.scatter(extent[0]+(extent[1]-extent[0])*0.8,extent[3]*1.08,
			    marker='o',color='m', s=100,edgecolor='black',alpha=0.5,clip_on=False)
			ax2.text(extent[0]+(extent[1]-extent[0])*0.85,extent[3]*1.08,
			    r'$10^4 M_\odot$', verticalalignment='center')

		if(i==2):
			if column:
				fig.savefig('png/'+options.out_dir+'/'+data.base+'surf.png',dpi=100)
			else:
				fig.savefig('png/'+options.out_dir+'/'+data.base+'slice.png',dpi=100)
			fig.clf()

# for surface density

#	nrow=2
#	ncol=2
#	fig=plt.figure(figsize=(6*ncol,6*ratio))
#	axgrid = axes_grid.ImageGrid(fig,111,
#		 nrows_ncols = (nrow,ncol),
#		 label_mode = "L",
#		 direction='row',
#		 share_all = False,
#		 cbar_location='right',
#		 cbar_mode='each',
#		 cbar_size='3%',
#		 axes_pad=0.25,)
#
#	Nax=axgrid.ngrids


#	for i,symaxis in zip(range(2),('z','y')):
#		ax=axgrid[(i)%(Nax)]
#		dxy, v1xy, v2xy, Txy, x, y, extent = set_2dimg(grid,d,v1,v2,v3,T,symaxis=symaxis)
#		vxy=np.sqrt(v1xy**2+v2xy**2)
#		im=ax.imshow(np.log10(dxy),extent=extent,origin='lower',
#           	interpolation='nearest',cmap=cm.coolwarm,vmin=-2,vmax=2)
#		ax.set_xlim(extent[0], extent[1])
#		ax.set_ylim(extent[2], extent[3])
#		cb=ax.cax.colorbar(im,ticks=np.arange(vmin,vmax+1,1))
#		cb.ax.set_label(r'$\Sigma$[M$_\odot$pc$^{-2}$]')
#		ax.cax.toggle_label(True)
#		q=ax.quiver(x[::skip],y[::skip],
#                    v1xy[::skip,::skip],v2xy[::skip,::skip],
#                    pivot='tail',color='green',scale=100,
#	            headlength=6,headwidth=4,headaxislength=6,clip_on=False)
#		plot_starpar(ax,data,step,symaxis=symaxis)
#		if (i)%(Nax) == (Nax-1): 
#			ax.set_xlabel('x')
#			ax.set_ylabel('z')
#			fig.savefig('png/'+options.out_dir+'/'+data.base+'surf.png',dpi=100)
#			fig.clf()
#		else: 
#			ax.set_xlabel('x')
#			ax.set_ylabel('y')
#			qk = ax.quiverkey(q, 0.1, 1.05, 10.0, r'$10 km/s$',
#          	    	  labelpos='E', fontproperties={'weight': 'bold'})
#			ax.set_title('%5.2f' % (time))#*Omega/2./np.pi))
#			ax.scatter(extent[1]*0.2,extent[3]*1.05,marker='o',color='m',
#                           s=1,edgecolor='black',alpha=0.5,clip_on=False)
#			ax.text(extent[1]*0.25,extent[3]*1.05,r'$10^2 M_\odot$',
#                           verticalalignment='center')
#			ax.scatter(extent[1]*0.5,extent[3]*1.05,marker='o',color='m',
#                           s=10,edgecolor='black',alpha=0.5,clip_on=False)
#			ax.text(extent[1]*0.55,extent[3]*1.05,r'$10^3 M_\odot$',
#                           verticalalignment='center')
#			ax.scatter(extent[1]*0.8,extent[3]*1.05,marker='o',color='m',
#                           s=100,edgecolor='black',alpha=0.5,clip_on=False)
#			ax.text(extent[1]*0.85,extent[3]*1.05,r'$10^4 M_\odot$',
#                           verticalalignment='center')

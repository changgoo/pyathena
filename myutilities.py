import numpy as np
import matplotlib.pyplot as plt

def texteffect():
    try:
      from matplotlib.patheffects import withStroke
      myeffect = withStroke(foreground="w", linewidth=3)
      kwargs = dict(path_effects=[myeffect])
    except ImportError:
      kwargs = dict()
    return kwargs


def legend_decorate(legend):
    light_grey = np.array([float(248)/float(255)]*3)
    rect = legend.get_frame()
    rect.set_linewidth(0.0)
    rect.set_facecolor(light_grey)

def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.
    
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
        
    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)
    
    see also: 
    
    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
 
    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y
 
def smooth_gauss(list,degree=10):

    window=degree*2-1
    weight=np.array([1.0]*window)
    weightGauss=[]

    for i in range(window):

         i=i-degree+1
         frac=i/float(window)
         gauss=1/(np.exp((4*(frac))**2))
         weightGauss.append(gauss)

    weight=np.array(weightGauss)*weight
    smoothed=[0.0]*(len(list)-window)

    for i in range(len(smoothed)):
         smoothed[i]=sum(np.array(list[i:i+window])*weight)/sum(weight)

    return smoothed

def logok(data):

    min=data.min()
    max=data.max()
    if max <=0 or (abs(max) < abs(3.*min)): return False
    else: return True

def figure_preference():
    plt.rc('text', usetex=True)
    plt.rc('font',family='serif')
    plt.rc('lines',lw=2)
    plt.rc('font',size=16)
    plt.rc('xtick',labelsize=14)
    plt.rc('ytick',labelsize=14)


